@extends('users.layout.master',['title'=>'Agence de transformation digitale'])

@section('main')
  
  {{-- CLIENTS --}}
  <section id="clients" class="clients">
    <div class="container" data-aos="zoom-in">
      <div class="clients-slider swiper">
        <div class="swiper-wrapper align-items-center">
          <div class="swiper-slide"><img src="assets/img/clients/client-1.png" class="img-fluid" alt=""></div>
          <div class="swiper-slide"><img src="assets/img/clients/client-2.png" class="img-fluid" alt=""></div>
          <div class="swiper-slide"><img src="assets/img/clients/client-3.png" class="img-fluid" alt=""></div>
          <div class="swiper-slide"><img src="assets/img/clients/client-4.png" class="img-fluid" alt=""></div>
          <div class="swiper-slide"><img src="assets/img/clients/client-5.png" class="img-fluid" alt=""></div>
          <div class="swiper-slide"><img src="assets/img/clients/client-6.png" class="img-fluid" alt=""></div>
          <div class="swiper-slide"><img src="assets/img/clients/client-7.png" class="img-fluid" alt=""></div>
          <div class="swiper-slide"><img src="assets/img/clients/client-8.png" class="img-fluid" alt=""></div>
        </div>
        <div class="swiper-pagination"></div>
      </div>
    </div>
  </section> 

  <section id="about" class="about section-bg">
    <div class="container" data-aos="fade-up">

      <div class="row no-gutters">
        <div class="content col-xl-5 d-flex align-items-stretch">
          <div class="content">
            <h3>Notre vision pour votre digitalisation</h3>
            <p>
              Grace au digital, vous pouvez simplifier et reduire de maniere considerable votre charge de travail en dematerialisant l'ensemble de vos services tout en gardant et en multipliant votre efficacite. Notre agence forte de son experience met a votre disposition une expertise vous assurant une digitalisation reussi.  
            </p>
            {{--<a href="" class="about-btn"><span>About us</span> <i class="bx bx-chevron-right"></i></a>--}}
          </div>
        </div>
        <div class="col-xl-7 d-flex align-items-stretch">
          <div class="icon-boxes d-flex flex-column justify-content-center">
            <div class="row">
              <div class="col-md-6 icon-box" data-aos="fade-up" data-aos-delay="100">
                <i class="bx bx-receipt"></i>
                <h4>Efficacite et Rapidite</h4>
                <p>La digitalisation procure a vos equipes efficacite, rapidite et la limitation considerable des erreurs.</p>
              </div>
              <div class="col-md-6 icon-box" data-aos="fade-up" data-aos-delay="200">
                <i class="bx bx-cube-alt"></i>
                <h4>Expenssion rapide</h4>
                <p>Le digitale vous permet de vous etendre, de toucher de potentiel client et de fideliser vos client acquis.</p>
              </div>
              <div class="col-md-6 icon-box" data-aos="fade-up" data-aos-delay="300">
                <i class="bx bx-images"></i>
                <h4>Croissance</h4>
                <p>Grace a une efficacite, une rapidite des services et une expenssion le digital vous assure une croissance economique.</p>
              </div>
              <div class="col-md-6 icon-box" data-aos="fade-up" data-aos-delay="400">
                <i class="bx bx-shield"></i>
                <h4>Leadership</h4>
                <p>Grace a une digitalisation maitrisee vous pouvez assoir votre position de leader dans votre secteur d'activite.</p>
              </div>
            </div>
          </div> 
        </div>
      </div>

    </div>
  </section> 

 {{-- stats --}}
  <section id="counts" class="counts">
    <div class="container" data-aos="fade-up">

      <div class="row">

        <div class="col-lg-3 col-md-6">
          <div class="count-box">
            <i class="bi bi-emoji-smile"></i>
            <span data-purecounter-start="0" data-purecounter-end="100" data-purecounter-duration="1" class="purecounter"></span>
            <p>Happy Clients</p>
          </div>
        </div>

        <div class="col-lg-3 col-md-6 mt-5 mt-md-0">
          <div class="count-box">
            <i class="bi bi-journal-richtext"></i>
            <span data-purecounter-start="0" data-purecounter-end="200" data-purecounter-duration="1" class="purecounter"></span>
            <p>Projects</p>
          </div>
        </div>

        <div class="col-lg-3 col-md-6 mt-5 mt-lg-0">
          <div class="count-box">
            <i class="bi bi-headset"></i>
            <span data-purecounter-start="0" data-purecounter-end="700" data-purecounter-duration="1" class="purecounter"></span>
            <p>Etudiants formes</p>
          </div>
        </div>

        <div class="col-lg-3 col-md-6 mt-5 mt-lg-0">
          <div class="count-box">
            <i class="bi bi-people"></i>
            <span data-purecounter-start="0" data-purecounter-end="25" data-purecounter-duration="1" class="purecounter"></span>
            <p>Hard Workers</p>
          </div>
        </div>

      </div>

    </div>
  </section> 

  {{-- ======= Tabs Section ======= --}}
  <section id="tabs" class="tabs">
    <div class="container" data-aos="fade-up">

      <ul class="nav nav-tabs row d-flex">
        <li class="nav-item col-3">
          <a class="nav-link active show" data-bs-toggle="tab" data-bs-target="#tab-1">
            <i class="ri-gps-line"></i>
            <h4 class="d-none d-lg-block">Etude approfondie du secteur</h4>
          </a>
        </li>
        <li class="nav-item col-3">
          <a class="nav-link" data-bs-toggle="tab" data-bs-target="#tab-2">
            <i class="ri-body-scan-line"></i>
            <h4 class="d-none d-lg-block">Traitement et Conception</h4>
          </a>
        </li>
        <li class="nav-item col-3">
          <a class="nav-link" data-bs-toggle="tab" data-bs-target="#tab-3">
            <i class="ri-sun-line"></i>
            <h4 class="d-none d-lg-block">Evaluation clientelle</h4>
          </a>
        </li>
        <li class="nav-item col-3">
          <a class="nav-link" data-bs-toggle="tab" data-bs-target="#tab-4">
            <i class="ri-store-line"></i>
            <h4 class="d-none d-lg-block">Des experts professionnels</h4>
          </a>
        </li>
      </ul>

      <div class="tab-content">
        <div class="tab-pane active show" id="tab-1">
          <div class="row">
            <div class="col-lg-6 order-2 order-lg-1 mt-3 mt-lg-0" data-aos="fade-up" data-aos-delay="100">
              <h3>Chez {{config('eurazcom.name')}} nous mettons le client au centre de nos priorites et de notre politique de developpement.</h3>
              <p class="fst-italic">
                Grace a un personnel competent, qualifie et professionnel, nous restons a l'ecoute des besoins de nos clients pour toute demande ou sollicitation. Chez {{config('eurazcom.name')}}
              </p>
              <ul>
                <li><i class="ri-check-double-line"></i> Nous pretons une tres grande attention a l'ecoute et de la prise en compte des besoins du client.</li>
                <li><i class="ri-check-double-line"></i>Nous traitons chaque demande avec professionnalisme pour une grande satisfaction de nos clients.</li>
                <li><i class="ri-check-double-line"></i>Nous nous adaptons aux difficultes et aux doleances de nos clients car le plus important pour nous est votre satisfaction.</li>
              </ul>
              <p>
                Notre agence c'est donne pour mission d'assister chacun de ces clients dans leur processus de transformation digitale, en traitant chaque projet dans le respect des regles qui a bati notre reputation et avec un sens de l'ethique qui est la notre, tout en mettant egalement un accent sur le respect de votre vie privee.
              </p>
            </div>
            <div class="col-lg-6 order-1 order-lg-2 text-center" data-aos="fade-up" data-aos-delay="200">
              <img src="assets/img/tabs-1.jpg" alt="" class="img-fluid">
            </div>
          </div>
        </div>
        <div class="tab-pane" id="tab-2">
          <div class="row">
            <div class="col-lg-6 order-2 order-lg-1 mt-3 mt-lg-0">
              <h3>Neque exercitationem debitis soluta quos debitis quo mollitia officia est</h3>
              <p>
                Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
                velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
                culpa qui officia deserunt mollit anim id est laborum
              </p>
              <p class="fst-italic">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
                magna aliqua.
              </p>
              <ul>
                <li><i class="ri-check-double-line"></i> Ullamco laboris nisi ut aliquip ex ea commodo consequat.</li>
                <li><i class="ri-check-double-line"></i> Duis aute irure dolor in reprehenderit in voluptate velit.</li>
                <li><i class="ri-check-double-line"></i> Provident mollitia neque rerum asperiores dolores quos qui a. Ipsum neque dolor voluptate nisi sed.</li>
                <li><i class="ri-check-double-line"></i> Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate trideta storacalaperda mastiro dolore eu fugiat nulla pariatur.</li>
              </ul>
            </div>
            <div class="col-lg-6 order-1 order-lg-2 text-center">
              <img src="assets/img/tabs-2.jpg" alt="" class="img-fluid">
            </div>
          </div>
        </div>
        <div class="tab-pane" id="tab-3">
          <div class="row">
            <div class="col-lg-6 order-2 order-lg-1 mt-3 mt-lg-0">
              <h3>Voluptatibus commodi ut accusamus ea repudiandae ut autem dolor ut assumenda</h3>
              <p>
                Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
                velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
                culpa qui officia deserunt mollit anim id est laborum
              </p>
              <ul>
                <li><i class="ri-check-double-line"></i> Ullamco laboris nisi ut aliquip ex ea commodo consequat.</li>
                <li><i class="ri-check-double-line"></i> Duis aute irure dolor in reprehenderit in voluptate velit.</li>
                <li><i class="ri-check-double-line"></i> Provident mollitia neque rerum asperiores dolores quos qui a. Ipsum neque dolor voluptate nisi sed.</li>
              </ul>
              <p class="fst-italic">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
                magna aliqua.
              </p>
            </div>
            <div class="col-lg-6 order-1 order-lg-2 text-center">
              <img src="assets/img/tabs-3.jpg" alt="" class="img-fluid">
            </div>
          </div>
        </div>
        <div class="tab-pane" id="tab-4">
          <div class="row">
            <div class="col-lg-6 order-2 order-lg-1 mt-3 mt-lg-0">
              <h3>Omnis fugiat ea explicabo sunt dolorum asperiores sequi inventore rerum</h3>
              <p>
                Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
                velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
                culpa qui officia deserunt mollit anim id est laborum
              </p>
              <p class="fst-italic">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
                magna aliqua.
              </p>
              <ul>
                <li><i class="ri-check-double-line"></i> Ullamco laboris nisi ut aliquip ex ea commodo consequat.</li>
                <li><i class="ri-check-double-line"></i> Duis aute irure dolor in reprehenderit in voluptate velit.</li>
                <li><i class="ri-check-double-line"></i> Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate trideta storacalaperda mastiro dolore eu fugiat nulla pariatur.</li>
              </ul>
            </div>
            <div class="col-lg-6 order-1 order-lg-2 text-center">
              <img src="assets/img/tabs-4.jpg" alt="" class="img-fluid">
            </div>
          </div>
        </div>
      </div>

    </div>
  </section><!-- End Tabs Section -->

  {{-- ======= Services Section ======= --}}
  <section id="services" class="services section-bg ">
    <div class="container" data-aos="fade-up">

      <div class="section-title">
        <h2>Nos services</h2>
        <p>Depuis sa creation et la mise sur le marche de nos premiers produits, notre agence a sur se diversifier dans divers secteurs du numerique et du digital afin de repondre efficacement aux problematique de nos clients, et leurs assurer une reponse efficace et une offre de de qualite en fonction de leurs besoins et centre d'interet.</p>
      </div>

      <div class="row">
        <div class="col-md-6">
          <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
            {{--<i class="bi bi-briefcase"></i>--}}
            <i class="bi bi-window-fullscreen"></i>
            <h4><a href="#">Conception des sites et applications web</a></h4>
            <p>Notre agence est specialise dans la conception des sites Vitrine, Landing page, E-commerce, WordPress, Dropshipping, etc... et des sites web sur mesure.</p>
          </div>
        </div>
        <div class="col-md-6 mt-4 mt-md-0">
          <div class="icon-box" data-aos="fade-up" data-aos-delay="200">
            <i class="bi bi-card-checklist"></i>
            <h4><a href="#">Creation de logiciels specialises</a></h4>
            <p>Notre agence est specialise dans la conception des logiciels de gestion de stock, Dashboard, Gestion des ventes, Facturation, etc... et des logiciels sur mesure.</p>
          </div>
        </div>
        <div class="col-md-6 mt-4 mt-md-0">
          <div class="icon-box" data-aos="fade-up" data-aos-delay="300">
            <i class="bi bi-bar-chart"></i>
            <h4><a href="#">Communication et Marketing digital</a></h4>
            <p>Nous accompagnons les entreprises dans l'elabortion des plans de communication(crise, lancement produits) et des strategies marketing.</p>
          </div>
        </div>
        <div class="col-md-6 mt-4 mt-md-0">
          <div class="icon-box" data-aos="fade-up" data-aos-delay="400">
            <i class="bi bi-binoculars"></i>
            <h4><a href="#">Publicite et Annonces</a></h4>
            <p>Vous souhaitez faire connaitre votre marque ou vos produits, nous concevons et diffusons pour vous vos spots et videos publicitaires cibles de hautes qualite.</p>
          </div>
        </div>
        <div class="col-md-6 mt-4 mt-md-0">
          <div class="icon-box" data-aos="fade-up" data-aos-delay="500">
            <i class="bi bi-brightness-high"></i>
            <h4><a href="#">Design graphique</a></h4>
            <p>Carte de visite professionnelle, Magazine, Logo, Banderolles publicitaires, Amballage produits, Conception de marquette de projet</p>
          </div>
        </div>
        <div class="col-md-6 mt-4 mt-md-0">
          <div class="icon-box" data-aos="fade-up" data-aos-delay="600">
            <i class="bi bi-calendar4-week"></i>
            <h4><a href="#">Formation</a></h4>
            <p>Developpement des sites web et de logiciels, Community manager, Graphisme(Suite adobe),etc</p>
          </div>
        </div>
      </div>

    </div>
  </section> 

  {{-- ======= Portfolio Section ======= --}}
  @include('partials.portfolio')

  {{-- ======= Testimonials Section ======= --}}
  @include('partials.testimonials')
  

  {{-- ======= Pricing Section ======= --}}
  <section id="pricing" class="pricing section-bg">
    <div class="container" data-aos="fade-up">

      <div class="section-title">
        <h2>Avez-vous un projet qui necessite notre expertise?</h2>
        <p>Vous avez un projet qui necessite une expertise poussee et professionnelle, faites confiance a notre agence pour un maximum de securite, et de satisfaction, nous tout respectons vos contraintes(temps, budget,et autres.). </p>
      </div>
      <div class="text-center">
        <a href="{{route('eurazcom.project.home')}}" class="btn btn-primary fw-bold">Demander un devis</a>
      </div>

    </div>
  </section> 

  {{-- ======= Frequently Asked Questions Section ======= --}}
   @include('partials.faq')

  <!-- ======= Team Section ======= -->
  <section id="team" class="team section-bg">
    <div class="container" data-aos="fade-up">

      <div class="section-title">
        <h2>Notre Equipe</h2>
        <p>Majoritairement constitue de jeunes ingenieurs qualifies, professionels dote d'une expertise averee, nous efforcons chaque jour de donner le sourire et de realiser le reve de centaine de clients ou qu'il se trouve dans le monde.</p>
      </div>

      <div class="row">

        <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
          <div class="member" data-aos="fade-up" data-aos-delay="100">
            <div class="member-img">
              <img src="assets/img/team/team-1.jpg" class="img-fluid" alt="">
              <div class="social">
                <a href=""><i class="bi bi-twitter"></i></a>
                <a href=""><i class="bi bi-facebook"></i></a>
                <a href=""><i class="bi bi-instagram"></i></a>
                <a href=""><i class="bi bi-linkedin"></i></a>
              </div>
            </div>
            <div class="member-info">
              <h4>Walter White</h4>
              <span>Chief Executive Officer</span>
            </div>
          </div>
        </div>

        <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
          <div class="member" data-aos="fade-up" data-aos-delay="200">
            <div class="member-img">
              <img src="assets/img/team/team-2.jpg" class="img-fluid" alt="">
              <div class="social">
                <a href=""><i class="bi bi-twitter"></i></a>
                <a href=""><i class="bi bi-facebook"></i></a>
                <a href=""><i class="bi bi-instagram"></i></a>
                <a href=""><i class="bi bi-linkedin"></i></a>
              </div>
            </div>
            <div class="member-info">
              <h4>Sarah Jhonson</h4>
              <span>Product Manager</span>
            </div>
          </div>
        </div>

        <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
          <div class="member" data-aos="fade-up" data-aos-delay="300">
            <div class="member-img">
              <img src="assets/img/team/team-3.jpg" class="img-fluid" alt="">
              <div class="social">
                <a href=""><i class="bi bi-twitter"></i></a>
                <a href=""><i class="bi bi-facebook"></i></a>
                <a href=""><i class="bi bi-instagram"></i></a>
                <a href=""><i class="bi bi-linkedin"></i></a>
              </div>
            </div>
            <div class="member-info">
              <h4>William Anderson</h4>
              <span>CTO</span>
            </div>
          </div>
        </div>

        <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
          <div class="member" data-aos="fade-up" data-aos-delay="400">
            <div class="member-img">
              <img src="assets/img/team/team-4.jpg" class="img-fluid" alt="">
              <div class="social">
                <a href=""><i class="bi bi-twitter"></i></a>
                <a href=""><i class="bi bi-facebook"></i></a>
                <a href=""><i class="bi bi-instagram"></i></a>
                <a href=""><i class="bi bi-linkedin"></i></a>
              </div>
            </div>
            <div class="member-info">
              <h4>Amanda Jepson</h4>
              <span>Accountant</span>
            </div>
          </div>
        </div>

      </div>

    </div>
  </section><!-- End Team Section -->

 

@stop