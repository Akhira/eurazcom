<section id="testimonials" class="testimonials">
  <div class="container" data-aos="fade-up">
    @if(!Route::is('eurazcom.testimonials'))
      <div class="section-title">
        <h2>Ce qu'ils pensent de nous</h2>
        <p>Tres content de notre travail pour eux ils ont tenus a temoigner leur reconnaissance envers notre agence ainsi que l'ensemble de notre equipe, et d'exprimer leur senntiment apres la livraison et la prise en main de leur projet.</p>
      </div>
    @endif

    <div class="testimonials-slider swiper" data-aos="fade-up" data-aos-delay="100">
      <div class="swiper-wrapper">

        @foreach(testimonials() as $testimonial)
          <div class="swiper-slide">
            <div class="testimonial-wrap">
              <div class="testimonial-item">
                <img src="{{asset($testimonial->image)}}" class="testimonial-img" alt="">
                <h3 class="text-capitalize">{{$testimonial->name}}</h3>
                <h4>{{$testimonial->profession}}</h4>
                <p>
                  <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                  {{$testimonial->content}}
                  <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                </p>
              </div>
            </div>
          </div>
        @endforeach

      </div>
      <div class="swiper-pagination"></div>
    </div>

  </div>
</section>