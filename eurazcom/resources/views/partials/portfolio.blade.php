{{-- realizations section --}}
<section class="realizations" id="realizations">
   	<h5 class="text-center fw-bold fs-5 text-uppercase mb-3">
   		Nos realisations
   	</h5>
   	<div class="text-center fw-bold text-uppercase text-muted" style="font-size: .9rem;">Plus de 200 projets realises pour nos clients dans plus de 22 pays
   	</div>
   	<div class="realizations-slider swiper">
   		<div class="swiper-wrapper">
   			@foreach(realizations() as $realization)
		   		<div class="swiper-slide">
	   				<div class="realization-wrap">
		              <div class="card realization-item border-0">
	              		<img src="{{asset($realization->image)}}" class="card-img-top">	
		              </div>
		          	</div>
		   		</div>
	   		@endforeach
   		</div>
   	</div>
   	<div class="text-center mt-3">
   		<a href="{{route('eurazcom.portfolio')}}" class="btn btn-outline-primary fw-bold text-uppercase" style="font-size: .9rem;">
			voir toutes nos realisations
		</a>
   	</div>
</section>