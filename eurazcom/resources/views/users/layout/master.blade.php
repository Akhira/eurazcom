<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>{{dynamicTitle($title??'')}}</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.17/css/intlTelInput.css" integrity="sha512-gxWow8Mo6q6pLa1XH/CcH8JyiSDEtiwJV78E+D+QP0EVasFs8wKXq16G8CLD4CJ2SnonHr4Lm/yY2fSI2+cbmw==" crossorigin="anonymous" referrerpolicy="no-referrer"/>

  <link href="{{asset('assets/vendor/aos/aos.css')}}" rel="stylesheet">
  <link href="{{asset('assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{asset('assets/vendor/bootstrap-icons/bootstrap-icons.css')}}" rel="stylesheet">
  <link href="{{asset('assets/vendor/boxicons/css/boxicons.min.css')}}" rel="stylesheet">
  <link href="{{asset('assets/vendor/glightbox/css/glightbox.min.css')}}" rel="stylesheet">
  <link href="{{asset('assets/vendor/remixicon/remixicon.css')}}" rel="stylesheet">
  <link href="{{asset('assets/vendor/swiper/swiper-bundle.min.css')}}" rel="stylesheet">
  <link rel="stylesheet" href="{{asset('assets/plugins/fontawesome-free/css/all.min.css')}}">

  @yield('type-section')
  @yield('registerStyle')
  @yield('blog-style')
  @yield('about-style')
  @yield('services-style')

  <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">
</head>

<body>

  <section id="topbar" class="d-flex align-items-center">
    <div class="container d-flex justify-content-center justify-content-md-between">
      <div class="contact-info d-flex align-items-center">
        <i class="bi bi-envelope d-flex align-items-center"><a href="mailto:contact@eurazcom.com">{{config('site.email')}}</a></i>
        <i class="bi bi-phone d-flex align-items-center ms-4"><span>{{config('site.phone')}}</span></i>
      </div>
      <div class="social-links d-none d-md-flex align-items-center">
        <a href="{{config('site.tw')}}" class="twitter"><i class="bi bi-twitter"></i></a>
        <a href="{{config('site.fb')}}" class="facebook"><i class="bi bi-facebook"></i></a>
        <a href="{{config('site.yt')}}" class="instagram"><i class="bi bi-youtube"></i></a>
        <a href="{{config('site.ld')}}" class="linkedin"><i class="bi bi-linkedin"></i></a>
        <a href="{{config('site.inst')}}" class="linkedin"><i class="bi bi-instagram"></i></a>
        <a href="{{config('site.tw')}}" class="linkedin"><i class="bi bi-flag"></i></a>
      </div>
    </div>
  </section>
  <header id="header" class="d-flex align-items-center">
    <div class="container d-flex align-items-center justify-content-between">
      <!-- <h1 class="logo"><a href="index.html">BizLand<span>.</span></a></h1> -->
      <h1 class="logo"><a href="{{route('welcome')}}">{{config('app.name')}}</a></h1>
      <!-- Uncomment below if you prefer to use an image logo -->
      <!-- <a href="index.html" class="logo"><img src="assets/img/logo.png" alt=""></a>-->
      <nav id="navbar" class="navbar">
        <ul>
          <li><a class="nav-link scrollto {{activeRoute('welcome')}}" href="{{route('welcome')}}">Home</a></li>
          <li><a class="nav-link scrollto {{activeRoute('eurazcom.about')}}" href="{{route('eurazcom.about')}}">About</a></li>
          <li class="dropdown">
            <a class="nav-link scrollto" href=""><span>Services</span><i class="bi bi-chevron-down"></i></a>
            <ul>
              <li><a href="{{route('eurazcom.service-1')}}">Creation site / application web</a></li>
              <li><a href="{{route('eurazcom.service-2')}}">Creation de logiciel specialises</a></li>
              <li><a href="{{route('eurazcom.service-3')}}">Integration de solutions ERP & ODOOs</a></li>
              <li><a href="{{route('eurazcom.service-4')}}">Production des identites visuelles</a></li>
              <li><a href="{{route('eurazcom.service-5')}}">Production & Diffusion Publicitaires</a></li>
              <li><a href="{{route('eurazcom.service-6')}}">Communication & Marketing</a></li>
              <li><a href="{{route('eurazcom.service-7')}}">Consulting Numerique</a></li>
              <li><a href="{{route('eurazcom.service-8')}}">Formations</a></li> 
             </ul>
          </li>
          <li>
            <a class="nav-link scrollto  {{activeRoute('eurazcom.portfolio')}}" 
              href="{{route('eurazcom.portfolio')}}">Portfolio
            </a>
          </li>
          <li><a class="nav-link scrollto {{activeRoute('eurazcom.solutions')}}" 
              href="{{route('eurazcom.solutions')}}">Solutions</a></li>
          <li><a href="{{route('eurazcom.publications')}}" class="nav-link {{activeRoute('eurazcom.publications')}}">Blog</a></li>
          <li class="dropdown"><a href=""><span>Tools</span> <i class="bi bi-chevron-down"></i></a>
            <ul>
              <li><a href="{{route('eurazcom.academy')}}" target="_blank">Eurazcom Academy</a></li>
              <li><a href="{{route('eurazcom.project.home')}}">Demander un devis</a></li>
              <li><a href="{{route('eurazcom.appointment.create')}}">Demander un RDV</a></li>
              <li><a href="{{route('eurazcom.partner.home')}}">Devenir Partenaires</a></li>
              <li><a href="{{route('eurazcom.testimonials')}}">Temoignages</a></li>
             </ul>
          </li>
          <li><a class="nav-link scrollto {{activeRoute('eurazcom.contact.create')}}" href="{{route('eurazcom.contact.create')}}">Contact</a></li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav> 
      @guest
        <a href="{{route('login')}}" class="fw-bold ms-4  btn btn-primary btn-sm">Portail client</a>
      @else
         <a href="{{route('eurazcom.profil',userAuth()->lastname.' '.userAuth()->name)}}" class="nav-link scrollto ms-5" >
            <img src="{{asset(userAuth()->avatar=='default.jpg'?'assets/img/'.userAuth()->avatar : userAuth()->avatar)}}" 
            width="30" class="img-fluid rounded-circle">
          </a>
      @endguest
    </div>
  </header>
 
   {{-- Banner of the home page --}}
  @if(Route::is('welcome'))
    <section id="hero" class="d-flex align-items-center">

      <div class="container" data-aos="zoom-out" data-aos-delay="100">
        <div class="row">
          <div class="col-xl-6">
            <h1>{{config('site.name')}} est une agence digitale au coeur de vos preocupations.</h1>
            <h2>
              Nous sommes le partenaire qu'il vous manquait pour la reussite de la digitalisation de vos services avec professionnalisme et efficacite et 100% de satisfaction.
            </h2>
            <a href="{{route('eurazcom.project.home')}}" class="btn btn-primary fw-bold mt-4">
              <i class="bi bi-card-list"></i> Demandez un devis des maintenant
            </a>
          </div>
        </div>
      </div>
    </section>
  @endif

   <main class="id">
     @yield('main')
   </main>

  <footer id="footer">

    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-3 col-md-6 footer-contact">
            <h3>{{config('site.name')}}<span>.</span></h3>
            <p>
              {{config('site.address')}}
              {{config('site.city')}}<br> BP {{config('site.pobox')}}<br>
              {{config('site.country')}} <br><br>
              <strong>Phone:</strong> {{config('site.phone')}}<br>
              <strong>Email:</strong> {{config('site.email')}}<br>
            </p>
          </div>

          <div class="col-lg-2 col-md-6 footer-links">
            <h4>Liens utiles</h4>
            <ul>
              @foreach(documents() as $document)
                <li>
                  <i class="bx bx-chevron-right"></i> 
                  <a href="{{$document->path}}">{{$document->name}}</a>
                </li>
              @endforeach
            </ul>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Our Services</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Adverting</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Web Development</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Communication</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Marketing</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Graphic Design</a></li>
            </ul>
          </div>

          <div class="col-lg-4 col-md-6 footer-newsletter">
            <h4>Rejoignez notre Newsletter</h4>
            <p>Pour etre au courant de l'evolution du numerique et de nos solutions de derniere genertion</p>
            <form action="{{route('eurazcom.newsletter')}}" method="POST">
              @csrf
              <div class="input-group">
                <input type="text" name="email" value="{{old('email')}}" class="form-control" style="box-shadow: none;">
                @error('email')<small class="text-danger fw-bold">{{$message}}</small>@enderror
                <button type="submit" class="btn btn-danger">Souscrire</button>
              </div>
            </form>
          </div>

        </div>
      </div>
    </div>

    <div class="container d-md-flex py-4">

      <div class="me-md-auto text-center text-md-start">
        <div class="copyright">
          &copy; Copyright <strong><span>{{Config('site.name')}} 2018 - {{date('Y')}}</span></strong> All Rights Reserved
        </div>
        <div class="credits">
           Designed by <a href="https://emmanya.com/">Emma Nya</a>
        </div>
      </div>
      <div class="social-links text-center text-md-end pt-3 pt-md-0">
        <a href="{{config('site.tw')}}" class="twitter"><i class="bx bxl-twitter"></i></a>
        <a href="{{config('site.fb')}}" class="facebook"><i class="bx bxl-facebook"></i></a>
        <a href="{{config('site.inst')}}" class="instagram"><i class="bx bxl-instagram"></i></a>
        <a href="{{config('site.yt')}}" class="google-plus"><i class="bx bxl-youtube"></i></a>
        <a href="{{config('site.lb')}}" class="linkedin"><i class="bx bxl-linkedin"></i></a>
      </div>
    </div>
  </footer> 

  <a href="#" class="back-to-top d-flex align-items-center rounded-circle justify-content-center">
    <i class="bi bi-arrow-up-short"></i>
  </a>

  <script src="https://code.jquery.com/jquery-3.6.4.js" integrity="sha256-a9jBBRygX1Bh5lt8GZjXDzyOB+bWve9EiO7tROUtj/E=" crossorigin="anonymous"></script>
  <!-- Vendor JS Files -->
  <script src="{{asset('assets/vendor/purecounter/purecounter_vanilla.js')}}"></script>
  <script src="{{asset('assets/vendor/aos/aos.js')}}"></script>
  <script src="{{asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{asset('assets/vendor/glightbox/js/glightbox.min.js')}}"></script>
  <script src="{{asset('assets/vendor/isotope-layout/isotope.pkgd.min.js')}}"></script>
  <script src="{{asset('assets/vendor/swiper/swiper-bundle.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('assets/js/phone.js')}}">    </script>
  <script src="{{asset('assets/js/main.js')}}"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.9.2/umd/popper.min.js" integrity="sha512-2rNj2KJ+D8s1ceNasitex6z4HWyOnEYLVC3FigGOmyQCZc2eBXKgOxQmo3oKLHyfcj53uz4QMsRCWNbLd32Q1g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <script src="{{asset('assets/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/parsley.js/2.9.2/parsley.min.js" integrity="sha512-eyHL1atYNycXNXZMDndxrDhNAegH2BDWt1TmkXJPoGf1WLlNYt08CSjkqF5lnCRmdm3IrkHid8s2jOUY4NIZVQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.17/js/intlTelInput-jquery.js" integrity="sha512-vSlSctDa0IV+XrJeS/WvsFHA4Wtf3mKgA3BeiTVq5EjqefeWC3gOhrdUiVt4Lc3Planrr7MnB3pdEQLHSj54rQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.17/js/intlTelInput.js" integrity="sha512-K6UpMqSC0wEa85pwDemaP1rsVXh7gTEfeJDimEtwi/gcGE05cVNnzKr19feb4sJnCrCmxRBFVBoCl4F8/iFIJg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
 
  @yield('mulitpleForm')

 </body>

</html>





