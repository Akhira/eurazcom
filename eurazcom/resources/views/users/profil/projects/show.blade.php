<!DOCTYPE html>
<html>
<head>
	<title>{{$project->title}} - {{config('eurazcom.name')}}</title>
	<link href="{{asset('assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
	 <link rel="stylesheet" href="{{asset('assets/plugins/fontawesome-free/css/all.min.css')}}">
</head>
<body class="bg-darks">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-3 ps-5 pt-5">
				<p>Project Name : <strong> {{$project->title}}</strong></p>
				<p>Project Sector : <strong> {{$project->activity}} </strong></p>
				<p>Type of project: <strong> {{$project->type}} </strong></p>
				<p>Budget: <strong> {{$project->final_budget}} </strong></p>
			</div>
			<div class="col-lg-6 shadow" style="height: 100vh; overflow-y: scroll;">
				contenus pdf
			</div>
			<div class="col-lg-3 p-5">
			 <div class="row mb-3">
			 	<p class="mb-3">Status : <span class="text-danger fw-bold">{{$project->status}} </span></p>
  				<button type="button" class="btn btn-info btn-sm fs-5  fw-bold mb-3">
 					<i class="fa fa-download"></i> Telecharger
 				</button> 

 				
 				@if($project->project == 0)
 					<h6>Pour accepter et transmettre ce projet a notre pour un debut de realisation entrez votre addresse email.</h6>
	 				<form method="POST" action="{{route('eurazcom.project.approve',$project->code)}}">
	 					@csrf
	 					{{method_field('PATCH')}}
	 					<input type="text" name="email" class="form-control" value="{{old('email')}}" placeholder="Email addresse">
	 					<div class="text-center">
	 						<button type="submit" class="btn btn-primary btn-sm mt-3 fw-bold">Envoyer le projet</button>
	 					</div>
	 				</form>
  				@endif
  			</div>
			</div>
		</div>
	</div>
 
	<form class="d-none" id='approve-{{$project->code}}' method="POST" action="{{route('eurazcom.project.approve',$project->code)}}">
		@csrf
		{{method_field("PATCH")}}
	</form>
</body>
</html>