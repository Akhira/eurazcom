@extends('users.profil.projects.master')

@section('quotations')
 <div class="col-lg-10">
	<table class="table">
		<thead>
			<tr>
				<th>#</th>
				<th>Name</th>
				<th>Activity sector</th>
				<th>Categorie</th>
				<th>Status</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			@forelse($projects as $project)
    			<tr>
    				<td>
					 <button type="button" class="border-0 bg-light"
					 onclick=" 
					 	event.preventDefault();
					 	document.getElementById('mark-{{$project->code}}').submit();
					 ">
					 @if($project->mark==0)<i class="fas fa-star"></i>@else <i class="fas fa-folder"></i> @endif
					 </button>
	    				</td>
    				<td>{{$project->title}}</td>
    				<td>{{$project->activity}}</td>
    				<td>{{$project->type}}</td>
    				<td class="text-danger">
    					{{$project->status}}
    				</td>
    				<td>
						<a href="{{route('eurazcom.project.show',$project->code)}}" target="__blank">
							<i class="fa fa-eye text-dark right"></i>
						</a>    					
    				</td>
    			</tr>
    			<form class="d-none" id="mark-{{$project->code}}" method="POST" 
    				action="{{route('eurazcom.project.mark',$project->code)}}">
    					@csrf
    					{{method_field('PATCH')}}
    			</form>
    		@empty
    		@endforelse
		</tbody>
	</table>
</div>
@stop