@extends('users.profil.layout.master')

@section('profil')
<div class="container-fluid mt-3">
	<div class="row"> 
		<div class="col-lg-2 project-sidebar mt-0">
			<nav>
				<ul>
					@if(Route::is('eurazcom.quotations.current') ||Route::is('eurazcom.quotations.treat') 	
																 ||Route::is('eurazcom.quotations.follow') ||Route::is('eurazcom.profil.quotations') ||Route::is('eurazcom.quotations.waitValidation'))
					<li><a href="{{route('eurazcom.quotations.current')}}" class="text-muted fw-bold">Attente traitement</a></li>
					<li><a href="{{route('eurazcom.quotations.treat')}}" class="text-muted fw-bold">Encours traitement</a></li>
					<li><a href="{{route('eurazcom.quotations.follow')}}" class="text-muted fw-bold">Suivis</a></li>
					<li>
						<a href="{{route('eurazcom.quotations.waitValidation')}}" class="text-muted fw-bold">
							Attente validation
						</a>
					</li>
					@else
 					<li><a href="{{route('eurazcom.projects.current')}}" class="text-muted fw-bold">Encours Realisation</a></li>
					<li><a href="{{route('eurazcom.projects.follow')}}" class="text-muted fw-bold">Suivis</a></li>
					<li><a href="{{route('eurazcom.projects.treat')}}" class="text-muted fw-bold">Livres</a></li>
					@endif
 				</ul>
			</nav>
		</div>
		@yield('quotations')
	</div>
</div>
@stop