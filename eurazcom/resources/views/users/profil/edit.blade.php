@extends('users.profil.layout.master')

@section('profil')
  <div class="container-fluid mt-3">
    <div class="row">
	      <div class="col-lg-6">
	        <div class="card card-primary card-outline">
	          <div class="card-header">
	            <h5 class="m-0">Informations personnelles</h5>
	          </div>
	          <div class="card-body">
	             <form method="post" action="{{route('eurazcom.profil.updateUserInfos')}}">
	             	@csrf
	          		{{method_field('PATCH')}}
	             	<div class="row">
	             		<div class="col-lg-6">
	             			<div class="form-group mb-3">
	             				<input type="text" name="name" value="{{userAuth()->name}}" placeholder="Enter name" class="form-control">
	             				@error('name')<small class="text-danger">{{$message}}</small>@enderror
	             			</div>
	             			<div class="form-group mb-3">
	             				<input type="text" name="email" value="{{userAuth()->email}}" placeholder="Enter email" class="form-control">
	             				@error('email')<small class="text-danger">{{$message}}</small>@enderror
	             			</div>
	             			<div class="form-group mb-3">
	             				<input type="text" name="city" value="{{userAuth()->city}}" placeholder="Enter city" class="form-control">
	             				@error('city')<small class="text-danger">{{$message}}</small>@enderror
	             			</div>
	             			<div class="form-group mb-3">
	             				<input type="text" name="address" value="{{userAuth()->address}}" placeholder="Enter address" class="form-control">
	             				@error('address')<small class="text-danger">{{$message}}</small>@enderror
	             			</div>
	             		</div>
	             		<div class="col-lg-6">
	             			<div class="form-group mb-3">
	             				<input type="text" name="lastname" value="{{userAuth()->lastname}}" placeholder="Enter lastname" class="form-control">
	             				@error('lastname')<small class="text-danger">{{$message}}</small>@enderror
	             			</div>
	             			<div class="form-group mb-3">
	             				<input type="text" name="phone" value="{{userAuth()->phone}}" placeholder="Enter phone" class="form-control">
	             				@error('phone')<small class="text-danger">{{$message}}</small>@enderror
	             			</div>
	             			<div class="form-group mb-3">
	             				<select name="country" class="form-select">
                                      @foreach(allCountries() as $key=>$country)
                                        <option value="{{$country}}" 
                                          {{geolocate()->country == $country? 'selected':''}}>
                                          {{$country}}
                                        </option>
                                      @endforeach
                                </select>
	             			</div>
	             			<div class="form-group mb-3">
	             				<input type="text" name="status" value="{{userAuth()->status}}" disabled class="form-control">
	             			</div>
	             		</div>
	             	</div>
	             	<div class="text-center mt-3">
	             		<button type="submit" class="btn btn-primary btn-sm fw-bold">Modifier infos</button>
	             	</div>
	             </form>
	           </div>
	        </div>
		   </div>

		  <div class="col-lg-6">
		  	<div class="card card-primary card-outline">
		      <div class="card-header">
		        <h5 class="m-0">Update image</h5>
		      </div>
		      <div class="card-body">
		        <div class="row">
		          <div class="col-lg-6 text-center">
		             <img src="{{asset(userAuth()->avatar=='default.jpg'?'assets/img/'.userAuth()->avatar : userAuth()->avatar)}}" width="210" height="210" class="img-fluid rounded-circle">
 		          </div>
		          <div class="col-lg-6">
		          	<form method="post" action="{{route('eurazcom.profil.updateUserAvatar')}}" enctype="Multipart/form-data">
		          		@csrf
		          		{{method_field('PATCH')}}
			             <div class="form-group text-end">
			             	<input type="file" name="avatar" name="{{userAuth()->avatar}}" class="form-control">
			             	@error('avatar')<small class="text-danger">{{$message}}</small>@enderror
			             	<button type="submit" class="btn btn-primary btn-sm fw-bold mt-3">Update avatar</button>
			             </div>
		            </form>
		          </div>
		        </div>
		       </div>
		    </div>
		  </div>
	</div>
</div>
@stop