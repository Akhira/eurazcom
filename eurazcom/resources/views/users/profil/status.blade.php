@extends('users.profil.layout.master')

@section('profil')
<div class="container-fluid mt-3">
	<form method="post" action="{{route('eurazcom.profil.updateStatus')}}" enctype="Multipart/form-data">
		@csrf
		{{method_field('PATCH')}}
		<div class="col-lg-7 offset-3">
			<div class="col-lg-12">
			  	 <div class="form-section">
                          <h5 class="mb-3 mt-0 fw-bold">Quel est votre status?</h5>
                          <div class="col-lg-12 d-flex justify-content-center">
                              <div class="form-check me-4">
                                <input class="form-check-input" type="radio" name="status" value="{{'Particular'}}" id="particular" {{userAuth()->status === 'Particular' ? 'checked':''}}
                                {{old('status')==='Particular'?'checked':''}}>
                                <label class="form-check-label" for="particular">
                                  Particular
                                </label>
                              </div>
                               
                              <div class="form-check me-4">
                                <input class="form-check-input" type="radio" value="{{'Company'}}" name="status" id="company" {{userAuth()->status === "Company" ? 'checked':''}}
                                {{old('status')==='Company'?'checked':''}}>
                                <label class="form-check-label" for="company">
                                  Entreprise enregistre
                                </label>
                              </div>
                              <div class="form-check me-4">
                                <input class="form-check-input" type="radio" value="{{'Association'}}" name="status" id="association" {{userAuth()->status === "Association" ? 'checked':''}}
                                {{old('status')==='Association'?'checked':''}}>
                                <label class="form-check-label" for="association">
                                  Association
                                </label>
                              </div>
                              <div class="form-check me-4">
                                <input class="form-check-input" type="radio" value="{{'Organization'}}" name="status" id="organization"{{userAuth()->status === "Organization" ? 'checked':''}}
                                {{old('status')==='Organization'?'checked':''}}>
                                <label class="form-check-label" for="organization">
                                  Organisation
                                </label>
                              </div>
                          </div>
                 </div>
			</div>
			<div class="col-lg-12 mt-4">
				<div class="row">
	             		<div class="col-lg-6">
	             			<div class="form-group mb-3">
	             				<input type="text" name="orgname" value="{{userAuth()->organization_name}}" placeholder="Enter entreprise name" class="form-control">
	             				@error('orgname')<small class="text-danger">{{$message}}</small>@enderror
	             			</div>
	             			<div class="form-group mb-3">
	             				<input type="text" name="orgemail" value="{{userAuth()->organization_email}}" placeholder="Enter email" class="form-control">
	             				@error('orgemail')<small class="text-danger">{{$message}}</small>@enderror
	             			</div>
	             			<div class="form-group mb-3">
	             				<input type="text" name="orgsector" value="{{userAuth()->organization_sector}}" placeholder="Enter sector entreprise" class="form-control">
	             				@error('orgsector')<small class="text-danger">{{$message}}</small>@enderror
	             			</div>
 	             		</div>
	             		<div class="col-lg-6">
	             			<div class="form-group mb-3">
	             				<input type="text" name="orgrole" value="{{userAuth()->users_role}}" placeholder="Enter user role" class="form-control">
	             				@error('orgrole')<small class="text-danger">{{$message}}</small>@enderror
	             			</div>
	             			<div class="form-group mb-3">
	             				<input type="text" name="orgphone" value="{{userAuth()->organization_phone}}" placeholder="Enter phone entreprise" class="form-control">
	             				@error('orgphone')<small class="text-danger">{{$message}}</small>@enderror
	             			</div>
	             			<div class="form-group mb-3">
	             				<input type="text" name="address" value="{{userAuth()->organization_address}}" placeholder="Enter address" class="form-control">
	             				@error('address')<small class="text-danger">{{$message}}</small>@enderror
	             			</div>
	             		</div>
	            </div>
	            <div class="form-group mt-4">
					<label for="form-label">Logo de votre structure</label>
					<input type="file" name="logo" value="{{userAuth()->organization_logo}}" class="form-control">
					@error('logo')<small class="text-danger">{{$message}}</small>@enderror
				 </div> 
	            <div class="text-center mt-4">
	            	<button type="submit" class="btn btn-primary btn-sm fw-bold">Update status</button>
	            </div>
			</div>
		</div>
	</form>
</div>
@stop