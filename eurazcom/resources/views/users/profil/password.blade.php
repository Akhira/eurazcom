@extends('users.profil.layout.master')

@section('profil')
<div class="container-fluid mt-3">
 	<h6 class="col-lg-7 offset-3 fw-bold">Changer votre mot de passe</h6>
	<form method="post" action="{{route('eurazcom.profil.updatePassword')}}" enctype="Multipart/form-data">
		@csrf
		{{method_field('PATCH')}}
		<div class="col-lg-7 offset-3">
 			<div class="col-lg-12 mt-4">
				<div class="row">
             		<div class="col-lg-6">
             			<div class="form-group mb-3">
             				<input type="password" name="password" value="{{old('password')}}" placeholder="Enter your current password" class="form-control">
             				@error('password')<small class="text-danger">{{$message}}</small>@enderror
             			</div>
             			<div class="form-group mb-3">
             				<input type="password" name="new_password" value="{{old('new_password')}}" placeholder="Enter new password" class="form-control">
             				@error('new_password')<small class="text-danger">{{$message}}</small>@enderror
             			</div>
             			<div class="form-group mb-3">
             				<input type="password" name="c_new_password" value="{{old('c_new_password')}}" placeholder="Confirm new password" class="form-control">
             				@error('c_new_password')<small class="text-danger">{{$message}}</small>@enderror
             			</div>
	             	</div>
 	            </div>
	            <div class="col-lg-6 text-center mt-4">
	            	<button type="submit" class="btn btn-primary btn-sm fw-bold">Update password</button>
	            </div>
			</div>
		</div>
	</form>
</div>
@stop