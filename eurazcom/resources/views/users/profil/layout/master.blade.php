@extends('users.layout.master',['title'=>userAuth()->lastname.' '.userAuth()->name])

@section('main')
	<section id="clients" class="clients">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-2 profil-sidebar">
					<div class="text-center mb-2" style="border-bottom: 1px solid grey;">
						<img src="{{asset(userAuth()->avatar=='default.jpg'?'assets/img/'.userAuth()->avatar : userAuth()->avatar)}}" width="130" class="img-fluid rounded-circle">
						<div class="fw-bold text-muted">{{stringSize(userAuth()->lastname,userAuth()->name,11)}}</div>
						<div class="fw-bold text-muted">{{userAuth()->email}}</div>
					</div>
					<nav>
						<ul>
							<li>
								<a href="{{route('eurazcom.profil.about')}}" 
								class="text-muted {{activeRoute('eurazcom.profil.about')}}">
								<i class="fas fa-user"></i> About</a>
							</li>
 							<hr class="divider"></li>							
							<li><a href="{{route('eurazcom.profil.quotations')}}" class="text-muted">
								<i class="far fa-folder"></i> Mes Devis</a>
							</li>
							<li>
								<a href="{{route('eurazcom.profil.projects')}}" class="text-muted">
								<i class="far fa-folder"></i> Mes Projets</a>
							</li>
							<li><hr class="divider"></li>
							<li>
								<a href="{{route('eurazcom.profil.edit')}}" class="text-muted">
								<i class="fas fa-pencil-alt"></i> Editer profil</a>
							</li>
							<li>
								<a href="{{route('eurazcom.profil.status')}}" class="text-muted">
								<i class="fas fa-user"></i> Status client</a>
							</li>
							<li>
								<a href="{{route('eurazcom.profil.password')}}" class="text-muted">
								<i class="fas fa-user"></i> Mot de passe</a>
							</li>
							<li><hr class="divider"></li>	
 							<li>
 								 <a class="dropdown-item text-muted" href="{{ route('logout',app()->getLocale()) }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('deconnexion') }}
                                    </a>

		                        <form id="logout-form" action="{{ route('logout',app()->getLocale()) }}" method="POST" class="d-none">
                                        @csrf
		                        </form>
 							</li>
 							<li>
 								<a class="dropdown-item text-muted" href="{{route('eurazcom.profil.closeAccount')}}">
                                        Fermer compte
                                </a>
  							</li>
						</ul>
  					</nav>
				</div>
				<div class="col-lg-10 profil-content">
					<header style="border-bottom: 1px solid grey;" class="mt-3">
						<div class="col-lg-6 mx-auto mb-2"> 
							<form method="POST" action="">
								@csrf
								<div class="input-group">
									<input type="search" name="search" value="" class="form-control" placeholder="Enter title or code of project" style="box-shadow: none;">
									<button class="btn btn-primary"><i class="fa fa-search"></i></button>
								</div>
				 			</form>
						</div>
						<div>
							
						</div>
					</header>
					<div class="col-lg-12">
						@yield('profil')
					</div>
				</div>
			</div>
		</div>
	</section>
@stop