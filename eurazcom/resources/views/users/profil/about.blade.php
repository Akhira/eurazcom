@extends('users.profil.layout.master')

@section('profil')
<div class="container-fluid mt-3">
    <div class="row">
	      <div class="col-lg-6">
	        <div class="card card-primary card-outline">
	          <div class="card-header">
	            <h5 class="m-0">Status : <strong class="text-success">{{userAuth()->status}}</strong></h5>
	          </div>
	          <div class="card-body">
	            <div class="row">
	              <div class="col-lg-6">
	                <img src="{{asset('assets/img/default.jpg')}}" width="200" height="200" class="rounded-circle img-fluid">
	                <div class="bg-dark mt-1 col-lg-8 text-center"><strong>Representatnt avatar</strong></div>
	              </div>
	              <div class="col-lg-6">
	                <div class="p-1">Name: 
	                	<strong class="text-success font-weight-bold">{{userAuth()->name." ".userAuth()->lastname}}</strong>
	                </div>
	                <div class="p-1">Email: 
	                	<strong class="text-success font-weight-bold">{{userAuth()->email}}</strong>
		            </div>
	                <div class="p-1">Phone: 
	                	<strong class="text-success font-weight-bold">{{userAuth()->phone}}</strong></div>
	                <div class="p-1">Location: 
	                	<strong class="text-success font-weight-bold">{{userAuth()->address}}</strong>
		            </div>
	                <div class="p-1">Residence: 
	                	<strong class="text-success font-weight-bold">{{userAuth()->city."-".userAuth()->country}}</strong>
		            </div>
	                <div class="p-1">Entr Role: 
	                	<strong class="text-success font-weight-bold">{{userAuth()->users_role}}</strong>
		            </div>
	                <div class="p-1">Total project: <strong class="text-success font-weight-bold">{{"20"}}</strong></div>
	              </div>
	            </div>
	           </div>
	        </div>
		   </div>

		  <div class="col-lg-6">
		  	<div class="card card-primary card-outline">
		      <div class="card-header">
		        <h5 class="m-0">Entreprise infos</h5>
		      </div>
		      <div class="card-body">
		        <div class="row">
		          <div class="col-lg-4 text-center">
		             <img src="{{asset('assets/img/default.jpg')}}" width="200" height="200" class="rounded-circle img-fluid">
		             <div class="bg-dark mt-1 col-lg-8 mx-auto text-center"><strong>Entreprise Logo</strong></div>
		          </div>
		          <div class="col-lg-8">
		            <div class="p-1">Entr/Assoc/Org name: 
		              <strong class="text-success font-weight-bold">{{userAuth()->organization_name}}</strong>
		          	</div>
		            <div class="p-1">Entr Phone: 
		              <strong class="text-success font-weight-bold">{{userAuth()->organization_phone}}</strong>
		          	</div>
		            <div class="p-1">Entr Email: 
		              <strong class="text-success font-weight-bold">{{userAuth()->organization_email}}</strong>
		          	</div>
		            <div class="p-1">Entr Address: 
		              <strong class="text-success font-weight-bold">{{userAuth()->organization_address}}</strong>
		          	</div>
		          </div>
		        </div>
		       </div>
		    </div>
		  </div>
	</div>
</div>
@stop