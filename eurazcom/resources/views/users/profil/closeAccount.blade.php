@extends('users.profil.layout.master')

@section('profil')
<div class="container-fluid mt-3">
	<form method="post" action="{{route('eurazcom.profil.storeCloseAccount')}}">
		@csrf
		{{method_field('PATCH')}}
		 <div class="col-lg-6 offset-3">
		 	<div class="form-group">
		 		<label class="form-label">SVP donnez-nous les raisons de votre fermeture de compte</label>
		 		<textarea class="form-control" name="reason" rows="5">{{old('reason')}}
		 		</textarea>
		 	</div>
		 	<div class="mt-3 text-center">
		 		<button class="btn btn-primary btn-sm fw-bold">Fermer mon compte</button>
		 	</div>
		 </div>
	</form>
</div>
@stop