<article class=" mb-2">
  <div class="card">
    <a href="{{route('eurazcom.publication.show',[$article->id,$article->slug])}}" class="text-dark">
      <img src="{{asset($similar->image)}}" class="card-img-top" style="max-height: 150px;">
    </a>
    <div class="card-body">
      <a href="{{route('eurazcom.publication.show',[$article->id,$article->slug])}}" class="text-dark">
        <h6 class="card-title"><strong>{{truncateWords($similar->title,35)}}</strong></h6>
      </a>
      <p class="card-text text-secondary">Emma - il y'a {{carbone_date($article->date_publication??'')}}</p>
      <p class="card-text" style="margin-top: -15px;">{{truncateWords($similar->presentation,70)}}</p>
    </div>
  </div>
</article>