@extends('users.layout.master',['title'=>'Notre banque de realisations'])

@section('main')
<section id="portfolio" class="portfolio">
    <div class="container">
      <div class="row">
        <div class="col-12 col-lg-12 text-center d-flex justify-content-center fw-bold">
          Des centaines de clients nous font confiance de par le monde 
        </div>
      </div>
    </div>
    <div class="containers mt-3">
        <div class="row">
          @forelse($realizations as $realization)
          <div class="col-lg-3">
            <div class="card border-0">
              <div class="card-body">
                <a href="{{route('eurazcom.portfolio.show',$realization->id)}}"> 
                  <img src="{{asset($realization->image)}}  " class="img-fluid" style="width: 100%;">
                </a>
                <div style="margin-top: -10px;">
                  <span class="badge bg-success  ms-3 fw-bold text-capitalize" style="font-size: .85rem;">
                    {{$realization->category}}
                  </span>
                  <div class="ms-3 fw-bold text-capitalize">
                    {{$realization->title}}
                  </div>
                </div>
              </div>
              
            </div>
          </div>
          @empty
          @endforelse 
        </div>
      </div>
</section>
@stop