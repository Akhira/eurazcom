@extends('users.layout.master',['title'=>$realization->title])

@section('main')
  <section class="breadcrumbs pt-0">
    <div class="container">
      <ol>
        <li><a href="{{route('eurazcom.portfolio')}}">Home</a></li>
        <li>Realization Details</li>
      </ol>
      <h2>Realisation  de {{$realization->title}} pour le compte de la societe {{$realization->client}}</h2>
    </div>
  </section> 
  <section id="portfolio-details" class="portfolio-details">
    <div class="container">
      <div class="row gy-4">
        <div class="col-lg-8">
          <div class="portfolio-details-slider swiper">
            <div class="swiper-wrapper align-items-center">
              <div class="swiper-slide">
                <div class="card border-0">
                  <div class="card-body">
                    <img src="{{asset($realization->image)}}" alt="{{$realization->title}}">
                  </div>
                </div>
              </div>
            </div>
            <div class="swiper-pagination"></div>
          </div>
        </div>
        <div class="col-lg-4">
          <div class="portfolio-info">
            <h3>Project information</h3>
            <ul>
              <li><strong>Category</strong>: {{$realization->category}}</li>
              <li><strong>Client</strong>: {{$realization->client}}</li>
              <li><strong>Project date</strong>: {{$realization->date}}</li>
              <li><strong>Country </strong>: {{$realization->country??'Undefined'}}</li>

              @if(!empty($realization->url))
              <li><strong>Project URL</strong>: 
                <a href="{{$realization->url}}" target="__blank">{{substr($realization->url,7,20)}}</a>
              @endif
            </li>
            <li><strong>Demo</strong>: <a href=""> Regarder la video</a></li>
            </ul>
          </div>
          <div class="portfolio-description">
            <h2>Description de la realization</h2>
            <p>
              {{$realization->description}}
            </p>
          </div>
        </div>
      </div>
     </div>
  </section> 
@stop