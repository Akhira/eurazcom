@extends('users.layout.master',['title'=>'Contacter notre agence'])

@section('main')
  <section id="contact" class="contact">
    <div class="container">
      <div class="section-title mt-4">
        <h2>Contact {{config('eurazcom.name')}}</h2>
        <p>Notre equipe de call center reste a votre disposition 24h/24 de Lundi a Samedi pour tous vos probleme ce peu importe le moyen que vous utilisez pour nous contacte. En cas de soucis laissez nous un message et nous vous repondons dans les plus bref delais. Coordialement le departement de communication de {{config('eurazcom.name')}}</p>
      </div>

      <div class="row">

        <div class="col-lg-6">

          <div class="row">
            <div class="col-md-12">
              <div class="info-box">
                <i class="bx bx-map"></i>
                <h3>Our Address</h3>
                <p>A108 Adam Street, New York, NY 535022</p>
              </div>
            </div>
            <div class="col-md-6">
              <div class="info-box mt-4">
                <i class="bx bx-envelope"></i>
                <h3>Email Us</h3>
                <p>info@example.com<br>contact@example.com</p>
              </div>
            </div>
            <div class="col-md-6">
              <div class="info-box mt-4">
                <i class="bx bx-phone-call"></i>
                <h3>Call Us</h3>
                <p>+1 5589 55488 55<br>+1 6678 254445 41</p>
              </div>
            </div>
          </div>

        </div>

        <div class="col-lg-6">
          <form  method="POST" action="{{route('eurazcom.contact.send')}}"  role="form" class="php-email-form">
          	@csrf
            <div class="row">
              <div class="col form-group">
                <input type="text" name="name" class="form-control" value="{{old('name')}}" id="name" placeholder="Your Name">
                @error('name')<div class="text-danger">{{$message}}</div>@enderror
              </div>
              <div class="col form-group">
                <input type="email" class="form-control" name="email" value="{{old('email')}}" id="email" placeholder="Your Email">
                @error('email')<div class="text-danger">{{$message}}</div>@enderror
              </div>
            </div>
            <div class="form-group">
              <input type="text" class="form-control" name="subject" id="subject" value="{{old('subject')}}" placeholder="Subject">
              @error('subject')<div class="text-danger">{{$message}}</div>@enderror
            </div>
            <div class="form-group">
              <textarea class="form-control" name="message" rows="5" placeholder="Message">{{old('message')}}</textarea>
              @error('message')<div class="text-danger">{{$message}}</div>@enderror
            </div>
            <div class="text-center">
            	<button type="submit" class="btn btn-primary btn-sm fw-bold">Send Message</button>
            </div>
          </form>
        </div>

      </div>

    </div>
  </section>
 @stop