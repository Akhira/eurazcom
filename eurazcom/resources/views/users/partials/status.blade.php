<div class="form-section mb-3">
      <h5 class="mb-3 mt-0 fw-bold">Quel est votre status?</h5>
      <div class="col-lg-12 d-flex justify-content-center">
          <div class="form-check me-4">
            <input class="form-check-input" type="radio" name="status" value="{{'Particular'}}" id="particular" checked {{old('status')==='Particular'?'checked':''}}>
            <label class="form-check-label" for="particular">
              Particular
            </label>
          </div>
          <div class="form-check me-4">
            <input class="form-check-input" type="radio" value="{{'Company'}}" name="status" id="company"{{old('status')==='Company'?'checked':''}}>
            <label class="form-check-label" for="company">
              Entreprise Enregistree
            </label>
          </div>
          <div class="form-check me-4">
            <input class="form-check-input" type="radio" value="{{'Association'}}" name="status" id="association"{{old('status')==='Association'?'checked':''}}>
            <label class="form-check-label" for="association">
              Association
            </label>
          </div>
          <div class="form-check me-4">
            <input class="form-check-input" type="radio" value="{{'Organization'}}" name="status" id="organization"{{old('status')==='Organization'?'checked':''}}>
            <label class="form-check-label" for="organization">
              Organisation
            </label>
          </div>
      </div>
      @include("users.partials.companyInfos")
 </div>