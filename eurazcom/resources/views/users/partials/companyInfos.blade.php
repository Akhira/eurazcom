<div class="col-lg-12  shadow p-2 mt-3 mb-4 form-organization organization association company">
  <h5 class="fs-5 fw-bold">Information de votre organisation</h5>
    <div class="row">
      <div class="col-md-6">
          <div class="form-group mb-2">
              <input type="text" name="orgname" value="{{old('orgname')}}" placeholder="organisation name" class="form-control">
          </div>
      </div>
      <div class="col-md-6">
          <div class="form-group mb-2">
              <input type="text" name="role" value="{{old('role')}}" placeholder="Your Role in organisation" class="form-control">
          </div>
      </div>
    </div>
</div>