@extends('users.publications.home')

@section('pubications')
<div class="col-lg-10 mt-3">
  <div class="row">
    @forelse($articles as $article)
      <div class="col-12 col-md-6 col-lg-3 col-xl-3 mb-3">
          <div class="card border-0 shadow">
            <a href="{{route('eurazcom.publication.show',[$article->id,$article->slug])}}" class="text-dark">
              <img src="{{asset($article->image)}}" class="card-img-top" >
            </a>
            <div class="card-body" style="height: 150px;">
                <h5 class="fw-bold card-title mb-3 fs-6">
                  <a href="{{route('eurazcom.publication.show',[$article->id,$article->slug])}}" class="text-dark">
                     {{truncatewords($article->title,60)}}
                  </a>
                </h5>
                <div class="card-text text-muted mb-2">
                  Emma - il y'a {{carbone_date($article->date_publication)}}
                </div>
                <div class="card-text p-2 text-muted mb-2 d-flex justify-content-between">
                  <span><i class="fa fa-heart"></i> {{$article->likes}}</span>
                  <span><i class="fa fa-comment-alt"></i> {{$article->comments->count()}}</span>
                  <span><i class="fa fa-eye"></i> {{$article->views}}</span>
                  <span><i class="fa fa-share"></i> {{$article->shares}}</span>
                </div>
            </div>
          </div>
      </div>
    @empty
      <div class="container text-center pt-5">
        <img src="{{asset('assets/img/empty.png')}}" class="img-fluid" >
        <p>Aucun article publie</p>
      </div>
    @endforelse
  </div>
</div>
<div class="col-11 mx-auto col-lg-2">
  <div class="col-lg-12">
     <div class="card border-0">
      <div class="card-body">
        <img src="{{asset('assets/img/banner.jpeg')}}" class="img-fluid rounded" style="max-width: 200px;">
      </div>
    </div>
  </div>
  <div class="col-lg-12">
     <div class="card border-0">
      <div class="card-body">
        <img src="{{asset('assets/img/banner.jpeg')}}" class="img-fluid rounded" style="max-width: 200px;">
      </div>
    </div>
  </div>
</div>
@stop