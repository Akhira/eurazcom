@extends('users.publications.home')

@section('pubications')
 <div class="col-lg-8">
 	<div class="col-12 col-md-11 col-lg-10 col-xl-10 mx-auto">

 		<div class="card border-0">
 			<img src="{{asset($article->image)}}" class="catd-img-top" style="max-height: 350px;">
 			<div class="card-body">
 				<h5 class="card-title"><strong>{{$article->title}}</strong></h5>
 				<div class="card-text text-muted mb-2">Emma - il y'a {{carbone_date($article->date_publication)}}</div>
                <div class="card-text p-2 text-muted mb-2 d-flex justify-content-between">
                  <button class="btn btn-primary-outline text-primary" 
                  onclick="document.getElementById('likes-form-{{$article->id}}').submit();">
                    <i class="fa fa-heart"></i> {{$article->likes}}
                  </button>
                  <a href="#" class="text-primary"><i class="fa fa-share"></i> {{$article->shares}}</a>
                  <span><i class="fa fa-comment-alt"></i> {{$article->comments->count()}}</span>
                  <span><i class="fa fa-eye"></i> {{$article->views}}</span>
                </div>
                <p class="card-text">{{$article->presentation}}</p>
                <div class="mt-5">
                	{{$article->content}}
                </div>
 			</div>
 		</div>

 		<div class="col-12 col-md-12 col-xl-12 col-lg-12 mt-2">
 			<h2 class="fw-bold mb-3">Comments</h2>
      <div class="row">
        @forelse($article->comments as $comment)	
     			<div class="card border-0 shadow mb-3">
     				<div class="card-header row border-0">
     					<div class="col-lg-1">
                @if($comment->image === "default.jpg")
    	 					  <img src="{{asset('assets/img/default.jpg')}}" class="card-img-top rounded-circle" style="max-width: 70px;">
                @else
                  <img src="{{asset($comment->image)}}" class="card-img-top rounded-circle" style="max-width: 70px;">
                @endif
     					</div>
     					<div class="col-lg-11">
     						<p class="card-text">
                    <strong>{{$comment->name}}</strong> <br> 
                    {{$comment->country}} - il y'a {{carbone_date($comment->created_at)}}
                </p>
      					</div>
     				</div>
     				<div class="card-body">{{$comment->content}}</div>
     			</div>
        @empty
         <p class="fw-bold text-muted fs-4 text-center p-5">0 commentaires</p>
        @endforelse
      </div>
      <div class="shadow p-2">  
    		<form method="POST" action="{{route('eurazcom.publication.comment',$article->id)}}" enctype="Multipart/form-data">
   				@csrf
          {{--
            <input type="hidden" name="article" value="{{$article->id}}">

           --}}
          <div class="form-group mb-2">
            <input type="text" name="name" value="{{old('name')}}" class="form-control" placeholder="Votre nom">
            @error('name')<div class="text-danger">{{$message}}</div>@enderror
          </div>
          <div class="form-group mb-2">
            <input type="file" name="image" value="{{old('image')}}" class="form-control">
            {{-- @error('image')<div class="text-danger">{{$message}}</div>@enderror--}}
          </div>
   				<div class="form-group">
   					<textarea class="form-control" name="content" placeholder="Votre commentaire">
              {{old('content')}}
            </textarea>
            @error('content')<div class="text-danger">{{$message}}</div>@enderror
   				</div>
   				<div class="text-center">
  	 				<button class="mt-2 btn btn-primary btn-sm p-2 rounded-4 fw-bold" type="submit">Publier</button>
   				</div>
   			</form>
      </div>
 		</div>
 	</div>
 </div>
 <div class="col-11  col-lg-4">
   <h5 class="fw-bold fs-4">Articles similaires</h5>
   @foreach($articleCategories as $similar)
     <article class="mb-2 shadow p-0">
       <div class="row">
         <div class="col-lg-5">
           <div class="card">
             <a href="{{route('eurazcom.publication.show',[$article->id,$article->slug])}}" class="text-dark">
                <img src="{{asset($similar->image)}}" class="card-img-top" style="max-height: 150px;">
             </a>
           </div>
         </div>
         <div class="col-lg-7" style="margin-left: -25px;">
           <div class="card border-0">
             <div class="card-body" style="max-height: 100px;">
               <a href="{{route('eurazcom.publication.show',[$article->id,$article->slug])}}" class="text-dark">
                  <h6 class="card-title" style="margin-top: -15px;"><strong>{{truncateWords($similar->title,25)}}</strong></h6>
               </a>
               <p class="card-text text-secondary">Emma - il y'a {{carbone_date($article->date_publication)}}</p>
               <p class="card-text" style="margin-top: -15px;">{{truncateWords($similar->presentation,50)}}</p>
             </div>
           </div>
         </div>
       </div>
     </article>
   @endforeach
</div>
@stop

<form class="d-none" method="POST" action="{{route('eurazcom.publication.like',$article->id)}}" id="likes-form-{{$article->id}}">
  @csrf
  {{method_field("PATCH")}}
  <input type="hidden" name="articles" value="{{$article->id}}">
  <input type="hidden" name="address" value="{{userMacAddress()}}">
</form>