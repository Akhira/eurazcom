@extends('users.publications.home')

@section('pubications')
<div class="col-lg-12 mt-3">
  <div class="container"> 
    <div class="col-lg-8 mx-auto"> 
      @forelse($articles as $article)
        <div class="row shadow">
            <div class="col-lg-4 p-2">
              <img src="{{asset($article->image)}}" class="img-fluid runded" style="max-width: 100%;" >
            </div>
            <div class="col-lg-8 p-3">
              <div class="fw-bold fs-5 mb-1">
                <strong>{{trucatewords($article->title,50)}}</strong>
              </div>
              <div class="text-muted fw-bold">
                <div class="mb-1">
                  <span><i class="fa fa-heart"></i>&nbsp; 0</span>
                  <span class="ms-3"><i class="fa fa-comment"></i>&nbsp; 0</span>
                  <span class="ms-3"><i class="fa fa-eye"></i>&nbsp; 0</span>
                </div>
              </div>
              <div class="">
                {{trucatewords($article->presentation,200)}}
                </div>
            </div>
        </div>  
      @empty
        <div class="container text-center pt-5">
          <img src="{{asset('assets/img/empty.png')}}" class="img-fluid" >
          <p>Aucun article trouve</p>
        </div>
      @endforelse
    </div>
  </div>
</div>
@stop