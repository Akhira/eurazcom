@extends('users.layout.master',['title'=>'Nos publications'])
 
@section('main')
 <section class="publications" id="publications">
    <div class="col-lg-11 mx-auto mb-3">
      <div class="row">
        <div class="col-3 col-lg-3">
          <div class="row">
             <div class="col-lg-6 text-center">{{articles()}}&nbsp;Article(s)</div>
             <div class="col-lg-6 text-center">
              <form action="{{route('eurazcom.publications.categoriesFilter')}}">
                <div class="form-group">
                   <select class="form-control" name="filter" onchange="this.form.submit();" style="box-shadow: none;">
                     <option selected disabled>categorie</option>
                     @foreach(categories() as $categorie)
                      <option value="{{$categorie->id}}" {{filter($categorie->id)}}>{{$categorie->name}}</option>
                     @endforeach
                   </select>
                </div>
              </form>
             </div>
          </div>
        </div>
        <div class="col-8 col-lg-6">
          <form method="POST" action="{{route('eurazcom.publication.search')}}" autocomplete="on">
            @csrf
            <div class="input-group">
              <input type="search" name="search" value="{{request()->search??''}}" class="form-control" placeholder="Enter the title of article" style="box-shadow: none;">
              <button type="submit" class="btn btn-primary btn-sm">
                <i class="fa fa-search fw-bold"></i>
              </button>
            </div>
          </form>
        </div>
        <div class="col-12  col-lg-3">
          <div class="row">
            <div class="col-6 col-lg-6">
              <form action="{{route('eurazcom.publications.filter')}}"> 
                <div class="input-group">
                  <span><i class="fas fa-filter"></i></span>
                  <select name="filter" class="form-control border-0" onchange="this.form.submit();" style="box-shadow: none;">
                    <option value="all"{{filter('all')}}>All Publications</option>
                    <option value="date" {{filter('date')}}>Plus anciens</option>
                    <option value="most like" {{filter('most like')}}>Plus aimes</option>
                    <option value="popular" {{filter('popular')}}>Populaires</option>
                    <option value="most share" {{filter('most share')}}>Plus partages</option>
                  </select>
                </div>
              </form>
            </div>
            <div class="col-6 col-lg-6">
              <a href="{{route('eurazcom.publications')}}" class="btn btn-primary btn-sm fw-bold">
                <i class="fa fa-folder"></i> All Publications
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-11 mx-auto">
      <div class="row">
        @yield('pubications')
      </div>
    </div>
 </section>
@stop