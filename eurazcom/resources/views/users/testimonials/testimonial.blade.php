@extends('users.layout.master',['title'=>'Agence de transformation digitale'])

@section('main')
<section>
	<div class="container">
 	<h1 class="text-center">Faites votre temoignage ici</h1>
 	<p class="text-center">
 		Nous vous remiercions d'avance pour la reconnance de notre travail, et pour le temoignage vous nous gratifiez en ce moment.
 	</p>

 	<div class="col-lg-8 p-3 mx-auto shadow">
 		<form method="POST" action="{{route('eurazcom.testimonial.store')}}" enctype="Multipart/form-data">
 			@csrf
			<div class="row mb-3">
				<div class="col-lg-6">
		 			<div class="form-group">
							<input type="text" name="name" value="{{old('name')}}" placeholder="Votre nom" class="form-control">
							@error('name')<div class="text-danger">{{$message}}</div>@enderror
		 			</div>
				</div>
				<div class="col-lg-6">
		 			<div class="form-group">
							<input type="text" name="profession" value="{{old('profession')}}" placeholder="Votre profession" class="form-control">
							@error('profession')<div class="text-danger">{{$message}}</div>@enderror
		 			</div>
				</div>
			</div>
			<div class="form-group mb-3">
				<label class="form-label" for="image">votre photo de profil</label>
				<input type="file" name="image" value="{{old('image')}}" class="form-control" id="image">
				@error('image')<div class="text-danger">{{$message}}</div>@enderror
 			</div>
 			<div class="form-group mb-3">
 				<textarea name="content" class="form-control" placeholder="Votre temoignage">{{old('content')}}</textarea>
				@error('content')<div class="text-danger">{{$message}}</div>@enderror
			</div>
			<div class="text-center">
				<button type="submit" class="btn btn-primary fw-bold rounded-5">Envoyer</button>
			</div>
 		</form>
 	</div>
	</div>

</section>

@include('partials.testimonials')
@stop