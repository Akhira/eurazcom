@extends('users.layout.master',["title"=>"Submit Project"])
@section('type-section')
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/multiple.css')}}">
 @endsection
@section("main")
<div class="container">
	<section class="type-section col-md-10 mx-auto" style="margin-top: -20px;">
		<div class="content-type col-md-10 mx-auto bg-dangers p-3">
			<form class="register-form" method="POST" action="{{route('eurazcom.appointment.store')}}" enctype="">
				@csrf
				<div class="form-section">
					<h3 class="text-center mb-3 mt-5 fw-bold">Informations concernant la demande</h3>
					<div class="row mb-3">
						<div class="col-md-4"> 
							<div class="form-group">
								<label class="form-label">Nom complet</label>
								<input type="text" name="name" value="{{old('name')}}" class="form-control">
								@error('name')<small class="text-danger">{{$message}}</small>@enderror
							</div>
						</div>
						<div class="col-md-4"> 
							<div class="form-group">
								<label class="form-label">Telephone (Whatsapps si possible)</label>
								<input type="text" name="phone" value="{{old('phone')}}" id="phone" class="form-control">
                                <span id="error-msg" class="hide text-danger"></span>
                                @error('phone')<small class="text-danger">{{$message}}</small>@enderror
							</div>
						</div>
						<div class="col-md-4"> 
							<div class="form-group">
								<label class="form-label">Address email</label>
								<input type="email" name="email" value="{{old('email')}}"  class="form-control">
								@error('email')<small class="text-danger">{{$message}}</small>@enderror
							</div>
						</div>
					</div>
					<div class="row mb-3">
						<div class="col-md-4"> 
							<div class="form-group">
								<label class="form-label">Quel type de RDV souhaitez-vous?</label>
								<select name="type" class="form-select">
									<option selected disabled>Selectionner le type</option>
									<option value="Presentiel">Dans nos locaux</option>
									<option value="Telephonique">Telephonique</option>
									<option value="Viso-conference">Par visio-conference</option>
								</select>
								@error('type')<small class="text-danger">{{$message}}</small>@enderror
							</div>
						</div>
						<div class="col-md-4"> 
							<div class="form-group">
								<label class="form-label">Date</label>
								<input type="date" name="date" value="{{old('date')}}" class="form-control">
								@error('date')<small class="text-danger">{{$message}}</small>@enderror
							</div>
						</div>
						<div class="col-md-4"> 
							<div class="form-group">
								<label class="form-label">Quelle heure?</label>
 								<input type="text" name="hours" value="{{old('hours')}}" class="form-control" placeholder="11 heure">
 								@error('hours')<small class="text-danger">{{$message}}</small>@enderror
							</div>
						</div>
					</div>
					<div class="row mb-3">
						<div class="col-md-12"> 
							<div class="form-group">
								<label class="form-label">Qui souhaitez-vous rencontrer ?</label>
								<input type="text" name="target" value="{{old('target')}}" class="form-control" placeholder="Le directeur general">
								@error('target')<small class="text-danger">{{$message}}</small>@enderror
							</div>
						</div>
					</div>
					<div class="row mb-3">
						<div class="col-md-12"> 
							<div class="form-group">
								<label class="form-label">Quel est l'objet de votre demande?</label>
								<textarea name="object" class="form-control">
								{{old('object')}}</textarea>
								@error('object')<small class="text-danger">{{$message}}</small>@enderror 
							</div>
						</div>
					</div>
				</div>

				<div class="form-navigation col-md-9 mx-auto mt-5 mb-5">
					<button type="submit" class="btn btn-primary fw-bold  btn-sm">Envoyer votre demande</button>
				</div>
			</form>
			
		</div>
	</section>
</div>
@stop

@section('registerJs')
  <script type="text/javascript" src="{{asset('assets/js/register.js')}}"></script>
  <script type="text/javascript" src="{{asset('assets/js/project.js')}}"></script>
@stop