@extends('users.layout.master',['title'=>'Demande de devis'])

@section('main')
 <section class="container mt-5" >
 	<div class="container-fluid p-4">
 		<div class="row"> 
	 		<div class="col-lg-9 col-md-12 col-sm-12">
	 			<h1 class="text-uppercase fs-4">comment demander un devis</h1>
	 			<div class="col-lg-12 mt-3 mb-2">
	 				<h6 class="fw-bold text-capitalize">1) la reflexion</h6>
	 				<p>
	 					La reuissite d'un projet ne depends pas seulment de la bonne competence de l'agence mais egalement de l'idee de depart du projet. C'est la raison pour laquelle une bonne organisation de vos idees de projet est primordiale pour nous afin que nous puissions mieux cerner votre besoins et de vous produire un resultat de qualite a la hauteur de vos attentes et de notre reputation.
	 					<a href="#">Decouvrez comment organiser les idees d'un projet !!!</a>
	 				</p>
	 			</div>
	 			<div class="col-lg-12 mt-3 mb-2">
	 				<h6 class="fw-bold text-capitalize">2) la soummission</h6>
	 				<p>
	 					Une fois l'etape de la reflexion terminee, il ne vous reste plus qu'a nous soummetre votre idee de projet, afin de faciliter la soumission nous mettons a votre disposition des documents pret a l'utilisation ce en fonction du type de votre projet pour une meilleur presentation de votre projet.
	 					Telecharger le document de presentation de votre projet dans la section de droite de votre ecran et remplissez-la soigneusement, puis transformer la en PDF puis revenez sur le site et cliiquez sur 
	 					<span class="badge bg-warning">Soumettre votre projet</span> et il ne vous reste plus qu'a suivre les etapes qui vous sont proposees.
	 				</p>
	 			</div>
	 			<div class="col-lg-12 mt-3 mb-2">
	 				<h6 class="fw-bold text-capitalize">3)le suivis du projet</h6>
	 				<p>
	 					Une fois le projet soumis, vous pouvez suivre l'evolution du traitement de votre demande partant de l'etape de demande de devis jusqu'a la livraison en passant par le suivi de l'evolution du projet. pour cela connectez-vous a votre compte, puis allez dans la rubrique mes demandes, selectionnez le projet. <br>
	 					<a href="#">Decouvrez comment suivre un projet</a>
	 				</p>
	 			</div>
	   
	 		</div>
	 		<div class="col-lg-3">
	 			<h4 class="fw-bold fs-5">Nos fiches projet</h4>
	 			<p>En fonction de projet telechargez une de nos fiches ci-dessous</p>
	 			<div class="mb-2">
	 				<i class="bi bi-code-slash"></i>
	 				<a href="#" class="ms-2">Projet de solution numerique </a>
	 			</div>
	 			<div class="mb-2">
	 				<i class="bi bi-pencil-fill"></i>
	 				<a href="#" class="ms-2">Projet de design graphique</a>
	 			</div>
	 			<div class="mb-2">
	 				<i class="bi bi-phone-vibrate"></i>
	 				<a href="#" class="ms-2">Projet de communication/Marketing  </a>
	 			</div>	
	 			<div class="col-lg-12 text-center mt-3">
	 				<a href="{{route('eurazcom.project.create')}}" class="btn btn-primary fw-bold">Soummettre votre projet</a>
	 			</div> 			
 	 		</div>
	 	</div>
 	</div>
 </section>
@stop