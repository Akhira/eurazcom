@extends('users.layout.master',["title"=>"Create project"])
@section('type-section')
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/multiple.css')}}">
 @endsection
@section("main")
<div class="container mt-0" style="margin-bottom: 150px;">
    <div class="row justify-content-center " style="margin-top: 90px;">
        <div class="col-md-12">
           <div class="col-md-6 mx-auto shadow rounded p-4 ">
               
              <div class="mt-3">
                  <form method="POST" action="{{route('eurazcom.project.store')}}" class="register-form" enctype="Multipart/form-data">
                    @csrf
                      <div class="form-section">
	                      	<h5 class="fw-bold fs-5">Vos informations</h5>
	                      	<div class="row">
	                      		<div class="col-lg-6">
	                      			<div class="form-group mb-2">
	                      				<input type="text" name="name" value="{{old('name')}}" placeholder="Your name" class="form-control mb-2" required>
	                      			</div>
	                      			<div class="form-group">
	                      				<input type="text" name="email" value="{{old('email')}}" placeholder="Your email" class="form-control mb-2" required>
	                      			</div>
	                      			<div class="form-group mb-2">
	                      				<input type="text" name="address" value="{{old('address')}}" placeholder="Your address" class="form-control" required>
	                      			</div>
 	                      		</div>
	                      		<div class="col-lg-6">
	                      			<div class="form-group">
	                      				<input type="text" name="lastname" value="{{old('lastname')}}" placeholder="Your familly name" class="form-control mb-2" required>
	                      			</div>

 	                      			<div class="form-group">
	                      				<input type="text" name="phone" value="{{old('phone')}}" id="phone" placeholder="Your phone" class="form-control mb-2" required>
	                      			</div>
	                      			 
	                      			<div class="form-group">
	 	 								<select name="country" class="form-select">
											@foreach(allCountries() as $key=>$country)
		                                      <option value="{{$country}}" 
		                                        {{geolocate()->iso_code == $key? 'selected':''}}>
		                                        {{$country}}
		                                      </option>
		                                    @endforeach
										</select>
									</div>
	                      		</div>
	                      	</div>
	                      	<div class="form-group">
	              				<input type="text" name="city" value="{{old('city')}}" id="phone" placeholder="Your city" class="form-control mb-2" required>
                  			</div>
                      </div> 

                      @include('users.partials.status') 

                      <div class="form-section">
	                      	<h5 class="fw-bold fs-5">Informations concernant votre projet</h5>
	                      	<div class="fs-6">
	                          Vous pouvez telecharger nos <a href="">models</a> de cahier de presentation de projet afin de respecter nos normes en matiere de structuration de projet et de gagner en temps.
	                        </div>
 	                        <div class="form-group mt-4 mb-2">
	                          <select class="form-control" name="type" required>
	                            <option selected disabled>Selectionner une categorie pour votre projet</option>
	                            <option value="APPDEV">Developpement</option>
	                            <option value="BLD">Branding & Logo Design</option>
	                            <option value="MCD">Communication & Marketing</option>
	                            <option value="CDP">Conception & Diffusion publicitaire</option>
	                          </select>
	                        </div>
	                      	<div class="row">
	                      		<div class="col-lg-6">
	                      			<div class="form-group mb-2">
	                      				<input type="text" name="title" value="{{old('title')}}" placeholder="Project Name" class="form-control mb-2" required>
	                      			</div>
 	                      		</div>
	                      		<div class="col-lg-6">
	                      			<div class="form-group">
	                      				<input type="text" name="sector" value="{{old('sector')}}" placeholder="Project activitu sector" class="form-control mb-2" required>
	                      			</div>
  	                      		</div>
	                      	</div>
	                      	<div class="form-group">
	                          <label class="form-label">Telecharger le cahier de charge</label>
	                             <input type="file" name="chargeBook" value="{{old('chargeBook')}}" class="form-control" required>
	                        </div>
                      </div>

                      <div class="form-navigation mt-3">
                          <button type="button" 
                                  class="previous btn btn-danger btn-sm fw-bold float-left">
                                Precedent
                          </button>
                          <button type="button" 
                                  class="next btn btn-success btn-sm fw-bold float-right">
                                Suivant
                          </button>
                          <button type="submit" 
                                  class="float-right btn btn-primary btn-sm fw-bold">
                                Devenir client
                          </button>
                      </div>
                  </form>
              </div>
           </div>
        </div>
    </div>
</div>
@stop

@section('mulitpleForm')
  <script type="text/javascript" src="{{asset('assets/js/mulitpleForm.js')}}"></script>
  <script type="text/javascript" src="{{asset('assets/js/project.js')}}"></script>
@stop