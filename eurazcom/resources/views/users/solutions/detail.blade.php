@extends('users.layout.master',['title'=>$solution->title])

@section('main')
  <section class="breadcrumbs pt-0">
    <div class="container">
      <ol>
        <li><a href="{{route('eurazcom.solutions')}}">Home</a></li>
        <li>Solution Details</li>
      </ol>
      <h2>La solution {{$solution->title}} pour la gestion de votre(s) {{$solution->category}}</h2>
    </div>
  </section> 
  <section id="portfolio-details" class="portfolio-details">
    <div class="container">
      <div class="row gy-4">
        <div class="col-lg-8">
          <div class="portfolio-details-slider swiper">
            <div class="swiper-wrapper align-items-center">
              <div class="swiper-slide">
                <div class="card border-0">
                  <div class="card-body">
                    <img src="{{asset($solution->image)}}" alt="{{$solution->title}}">
                  </div>
                </div>
              </div>
            </div>
            <div class="swiper-pagination"></div>
          </div>
        </div>
        <div class="col-lg-4">
          <div class="portfolio-info">
            <h3>Project information</h3>
            <ul>
              <li><strong>Category</strong>: {{$solution->category}}</li>
               <li><strong>Project URL</strong>: 
              @if(!empty($solution->url))
                <a href="{{$solution->url}}" target="__blank">{{substr($solution->url,7,20)}}</a>
              @else
                <i>Undefined</i>
              @endif
            </li>
            <li><strong>Demo</strong>: <a href=""> Regarder la video</a></li>
            </ul>
          </div>
          <div class="portfolio-description">
            <h2>Description de la solution</h2>
            <p>
              {{$solution->description}}
            </p>
          </div>
        </div>
      </div>
     </div>
  </section> 
@stop