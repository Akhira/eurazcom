@extends('users.layout.master',['title'=>'Solutions'])

@section('main')
<section id="portfolio" class="portfolio">
    <div class="container">
      Vous avez un projet en tete et vous savez ce que vous souhaitez realiser, mais vous ne savez comment vous-y prendre pour realiser cela, pas de probleme vous pouvez vous procurer des projets deja existant et pret a l'emploi que nous vous proposons en guise de solution pour vos besoins.
    </div>
     <div class="containers mt-3">
        <div class="row">
          @forelse($solutions as $solution)
          <div class="col-lg-3">
            <div class="card border-0">
              <div class="card-body">
                <a href="{{route('eurazcom.solution.show',$solution->id)}}"> 
                  <img src="{{asset($solution->image)}}  " class="img-fluid" style="width: 100%;">
                </a>
                <div style="margin-top: -10px;">
                  <span class="badge bg-success  ms-3 fw-bold text-capitalize" style="font-size: .85rem;">
                    {{$solution->category}}
                  </span>
                  <div class="ms-3 fw-bold text-capitalize">
                    {{$solution->title}}
                  </div>
                </div>
              </div>
              
            </div>
          </div>
          @empty
          @endforelse 
        </div>
      </div>
</section>
@stop