<!DOCTYPE html>
<html>
<head>
    <title>{{dynamicTitle($title??'')}}</title>

      <link href="{{asset('assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
      <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.17/css/intlTelInput.css" integrity="sha512-gxWow8Mo6q6pLa1XH/CcH8JyiSDEtiwJV78E+D+QP0EVasFs8wKXq16G8CLD4CJ2SnonHr4Lm/yY2fSI2+cbmw==" crossorigin="anonymous" referrerpolicy="no-referrer"/>
     @yield('registerStyle')  
</head>
<body>
<div class="container-fluid">
    <header id="header" class="fixed-top mb-5 d-flex align-items-center">
        <div class="container d-flex align-items-center">
          <h1 class="logo me-auto"><a href="{{route('welcome')}}">{{config('app.name')}}</a></h1>
          @if(Route::is('register'))
              <a href="{{route('login')}}" class="get-started-btn scrollto">Sign In</a>
          @else
             <a href="{{route('register')}}" class="get-started-btn scrollto">Sign Up</a>
          @endif
        </div>
  </header>

  <main class="py-4">
      @yield('content')
  </main>
</div>

<script src="https://code.jquery.com/jquery-3.6.4.js" integrity="sha256-a9jBBRygX1Bh5lt8GZjXDzyOB+bWve9EiO7tROUtj/E=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.9.2/umd/popper.min.js" integrity="sha512-2rNj2KJ+D8s1ceNasTIex6z4HWyOnEYLVC3FigGOmyQCZc2eBXKgOxQmo3oKLHyfcj53uz4QMsRCWNbLd32Q1g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="{{asset('assets/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/parsley.js/2.9.2/parsley.min.js" integrity="sha512-eyHL1atYNycXNXZMDndxrDhNAegH2BDWt1TmkXJPoGf1WLlNYt08CSjkqF5lnCRmdm3IrkHid8s2jOUY4NIZVQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.17/js/intlTelInput-jquery.js" integrity="sha512-vSlSctDa0IV+XrJeS/WvsFHA4Wtf3mKgA3BeiTVq5EjqefeWC3gOhrdUiVt4Lc3Planrr7MnB3pdEQLHSj54rQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.17/js/intlTelInput.js" integrity="sha512-K6UpMqSC0wEa85pwDemaP1rsVXh7gTEfeJDimEtwi/gcGE05cVNnzKr19feb4sJnCrCmxRBFVBoCl4F8/iFIJg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script type="text/javascript" src="{{asset('assets/js/phone.js')}}">    </script>
@yield('registerJs')
</body>
</html>