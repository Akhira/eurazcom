@extends('partners.layout.master',['title'=>'Soumission projet'])

@section('registerStyle')
  <link rel="stylesheet" type="text/css" href="{{asset('assets/css/multiple.css')}}">
@stop

@section('partner')
<div class="container-fluid">
	<div class="container">
		<div class="col-sm-10 offset-2">
 			<form method="POST" action="{{route('eurazcom.partner.project-store')}}" autocomplete="off" 
 			class="register-form" enctype="Multipart/form-data">
				@csrf
				<input type="hidden" name="partner" value="{{$partner->code}}">
				<div class="form-section">
					<h5 class="fw-bold mb-4">Information du client</h5>	
					<div class="row"> 
						<div class="col-sm-6">
							<div class="form-group mb-3">
								<input type="text" name="clientName" value="{{old('clientName')}}" placeholder="Nom du client" class="form-control" required>
							</div>
							<div class="form-group mb-3">
								<input type="text" name="clientPOBOX" value="{{old('clientPOBOX')}}" placeholder="Code postal client" class="form-control" required>
							</div>
							<div class="form-group">
								<input type="text" name="clientCity" value="{{old('clientCity')}}" placeholder="Ville du client" class="form-control" required>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group mb-3">
								<input type="text" name="clientEmail" value="{{old('clientEmail')}}" placeholder="Email du client" class="form-control" required>
							</div>
							<div class="form-group mb-3">
								<input type="text" name="clientPhone" id="phone" value="{{old('clientPhone')}}" placeholder="Telephone du client" class="form-control" required>
							</div>
							<div class="form-group">
								<input type="text" name="clientCountry" value="{{old('clientCountry')}}" placeholder="Pays du client" class="form-control" required>
							</div>
						</div>
					</div>
				</div>
				<div class="form-section">
					<h5 class="fw-bold mb-4">Information sur le projet</h5>
					<div class="row"> 
						<div class="col-sm-6">
							<div class="form-group mb-3">
								<input type="text" name="projectTitle" value="{{old('projectTitle')}}" placeholder="Dites-nous en quoi consiste le projet" class="form-control" required>
							</div>
							
							<div class="form-group">
								<input type="number" name="budget" value="{{old('budget')}}" placeholder="Budget Propose" class="form-control" required>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group mb-3">
								<select name="categorie" class="form-control" required>
									<option selected disabled>Selectinner une categorie</option>
									<option class="Application web">Application web</option>
									<option class="Logiciel">Logiciel</option>
									<option class="Branding & Logo Design">Branding & Logo Design</option>
									<option class="Conception Publicitaire">Conception Publicitaire</option>
									<option class="Diffusion Publicitaire">Diffusion Publicitaire</option>
								</select>
 							</div>
							<div class="form-group mb-3">
								<select name="currency" class="form-control" required>
									<option selected disabled>Selectinner une devise</option>
									<option class="USD">USD</option>
									<option class="EUR">EUR</option>
									<option class="XFA">XFA</option>
 								</select>
							</div>
 						</div>
					</div>
					<div class="form-group">
						<label class="form-label">Document descriptif du projet</label>
						<input type="file" name="doc" value="{{old('doc')}}"class="form-control" required>
					</div>
				</div>
				<div class="form-navigation mt-3">
                  <button type="button" 
                          class="previous btn btn-danger btn-sm fw-bold float-left">
                        Precedent
                  </button>
                  <button type="button" 
                          class="next btn btn-success btn-sm fw-bold float-right">
                        Suivant
                  </button>
                  <button type="submit" 
                          class="float-right btn btn-primary btn-sm fw-bold">
                        Devenir client
                  </button>
	            </div>
 			</form>
		</div>
	</div>
</div> 
@stop

@section('registerJs')
<script type="text/javascript" src="{{asset('assets/js/register.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/project.js')}}"></script>
@stop