@extends('partners.layout.master',['title'=>'Devenir partenaire Eurazcom'])

@section('partner')
 <div class="container-fluid">
	<h5 class="fw-bold">Notre programme de partenariat : en quoi consiste t'elle?</h5>
	<div class="pt-2 mb-5">
		{{config('eurazcom.name')}} Partneship est un programme de notre agence de transformation digitale et de solutions numeriques s'adressant a toute personne desirant devenir partenaire d'affaire de l'agence. Notre programme couvre principalement <span class="text-primary fw-bold">Les apporteurs d'affaire et les annonceurs</span>. <br>
		<nav>
			<ol>
				<li class="fw-bold">Les apporteurs d'affaires</li>
				<p>
					Les apporteurs d'affaires sont nos partenaire qui ont pour objectifs de prospecter et de gagner des parts de marcher pour l'agence et de toucher en retour une commission sur le montant total de l'affaire conclu a hauteur de 30%;
				</p>
				<p>
					Notre programme est ouvert  a tous les pays ceci dans notre objectif d'internationnalisation de notre agence et de nos services.
				</p>
				<h6>Souhaitez-vous integrer notre programme ?</h6>
				<a href="{{route('eurazcom.partner.create')}}">Devenez un partenaire {{config('eurazcom.name')}}</a>
			</ol>
		</nav>
	</div>
	<div class="row border-1">
		<div class="col text-center">
			<h5 class="fw-bold fs-6">Partenaires total</h5>
			125489
		</div>
		<div class="col text-center">
			<h5 class="fw-bold fs-6">Apporteurs d'affaires</h5>
			125489
		</div>
		<div class="col text-center">
			<h5 class="fw-bold fs-6">Distributeurs de solutions</h5>
			125489
		</div>
	</div>
	<div class="row mt-5">
		<div class="col">
			<a href="#">Consulter les missions d'un partenaire</a> <br>
		</div>
		<div class="col">
			 <a href="#">Consulter les conditions pour devenir partenaire</a>
		</div>
	</div>
 </div>
@stop