@extends('partners.layout.master',['title'=>'Authentication Partner'])

@section('partner')
	<div class="p-0">
		@if(Session::get('warning'))
		  <div class="alert alert-warning mb-2">{{Session::get('warning')}}</div>
		@endif
		<h4 class="text-center fs-3 col-lg-6 mx-auto"><i class="bi bi-person-circle"></i></h4>
		<h6 class="col-lg-6 mx-auto mb-3 fw-bold text-center">Verificcation de l'identite partenaire</h6>
  		<form method="POST" action="{{route('eurazcom.partner.authentication')}}" class="col-lg-6 mx-auto" autocomplete="off">
 			@csrf
 			<div class="form-group mb-3">
 				<input type="text" name="code" value="{{old('code')}}" placeholder="Code partenaire" class="form-control">
 				@error('code')<small class="text-danger fw-bold">{{$message}}</small>@enderror
 			</div>
  			<div class="form-group mb-3 text-center">
 				<a href="{{route('eurazcom.partner.forgotCode')}}">J'ai oublie mon code partenaire</a>
 			</div>
 			<div class="text-center mb-5">
 				<button type="submit" class="btn btn-primary btn-sm fw-bold">Verification</button>
 			</div>
  		</form>
 	</div>
@stop