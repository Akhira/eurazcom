@extends('users.layout.master')

@section('main')
<section class="partners" id="partners">
	<div class="container">
		<div class="row">
			<div class="col-sm-5 mb-3">
				<img src="{{asset('assets/img/partner.png')}}" alt="partners-icon"> <br>
				<div class="col-12">
					@if(Route::is('eurazcom.partner.connexion'))
						<h6 class="mb-3">Si vous n'etes pas encore partenaire ?</h6>
						<a href="{{route('eurazcom.partner.create')}}">Devenez notre apporteur d'affaires</a>
		 			@else
		 				<h6 class="mb-3">Etes-vous deja un de nos partenaire?</h6>
						<a href="{{route('eurazcom.partner.connexion')}}">Soummetez un projet negocie</a>
	 				@endif
	 			</div>
			</div>
			<div class="col-sm-7">
				@yield('partner')
			</div>
		</div>
	</div>
</section>
@stop