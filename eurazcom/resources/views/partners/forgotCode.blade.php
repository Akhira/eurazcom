@extends('partners.layout.master',['title'=>'Forgot code partner'])
 
@section('partner')
	<div class="p-0">
		<h4 class="text-center fs-3 col-lg-6 mx-auto"><i class="bi bi-person-circle"></i></h4>
		<h6 class="col-lg-6 mx-auto mb-3 fw-bold text-center">Entrer les information permetant de retrouver votre code partenaire</h6>
  		<form method="POST" action="{{route('eurazcom.partner.forgotCodeStore')}}" class="col-lg-10 mx-auto" autocomplete="off">
 			@csrf
 			<div class="form-section">
	 			<div class="row">
	 				<div class="col-sm-6">
	 					<div class="form-group mb-4">
			 				<input type="text" name="name" value="{{old('name')}}" placeholder="Votre nom" class="form-control">
				 			@error('name')<small class="text-danger fw-bold">{{$message}}</small>@enderror
			 			</div>
			 			<div class="form-group mb-3">
			 				<input type="text" name="lastname" value="{{old('lastname')}}" placeholder="Nom de famille" class="form-control">
				 			@error('lastname')<small class="text-danger fw-bold">{{$message}}</small>@enderror
			 			</div>
	 				</div>
	 				<div class="col-sm-6">
	 					<div class="form-group mb-4">
			 				<input type="text" name="email" value="{{old('email')}}" placeholder="Email" class="form-control">
				 			@error('email')<small class="text-danger fw-bold">{{$message}}</small>@enderror
			 			</div>
			 			<div class="form-group mb-3">
			 				<input type="text" name="cni" value="{{old('cni')}}" placeholder="Code de la CNI" class="form-control">
				 			@error('cni')<small class="text-danger fw-bold">{{$message}}</small>@enderror
			 			</div>
	 				</div>
	 			</div>
	 		</div>
  			<div class="form-navigation text-center mt-3">
 				<button type="submit" class="float-right btn btn-primary btn-sm fw-bold">Envoyer ma requete</button>
 			</div>
  		</form>
 	</div>
@stop

 