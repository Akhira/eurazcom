@extends('partners.layout.master',['title'=>'Enter new password'])

@section('partner')
	<div class="p-5">
		<h4 class="text-center fs-3 col-lg-6 mx-auto"><i class="bi bi-person-circle"></i></h4>
		<h6 class="col-lg-6 mx-auto mb-3 fw-bold text-center">Changer de mot de passe </h6>
 		<form method="POST" action="{{route('partner.updatePassword')}}" class="col-lg-6 mx-auto">
 			@csrf
 			<div class="form-group mb-3">
 				<input type="password" name="password" value="{{old('password')}}" placeholder="New password" class="form-control">
 				@error('password')<small class="text-danger">{{$message}}</small>@enderror
 			</div>
 			<div class="form-group mb-3">
 				<input type="password" name="c_password" value="{{old('c_password')}}" placeholder="Confirm new password" class="form-control">
 				@error('c_password')<small class="text-danger">{{$message}}</small>@enderror
 			</div>

 			<input type="hidden" name="code" value="{{old($verification_code)}}">
 
 			<div class="text-center mb-5">
 				<button type="submit" class="btn btn-primary btn-sm fw-bold">Changer</button>
 			</div>

 		</form>
 	</div>
@stop