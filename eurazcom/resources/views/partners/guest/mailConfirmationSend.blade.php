@extends('partners.layout.master',['title'=>'Mail confirmation send'])

@section('partner')
	<div class="p-5 text-center">
 			<h3 class="mb-3 fw-bold text-center">
 				<i class="bi bi-envelope-check text-success"></i>
 			</h3>
			Un lien de changement de mot de passe vous a ete envoye par mail. <br> <br>

			Consulter le et activez le lien afin de debuter la modification effective de votre mot de passe. Si vous ne retrouvez pas cela dans votre boite email, consulter vos spams ou cliquez sur le bouton ci-dessous pour recevoir de nouveau le lien. <br> <br>

			<form method="POST" action="{{route('eurazcom.	partner.verify')}}">
				@csrf
				<input type="hidden" name="email" value="{{old('email')}}">
				<input type="hidden" name="code" value="{{old('code')}}">

				<button type="submit" class="btn btn-primary btn-sm fw-bold">Envoyer de nouveau le lien</button>
			</form>
 	</div>
@stop