@extends('partners.layout.master',['title'=>'Mot de passe oublie'])

@section('partner')
	<div class="p-5">
		<h4 class="text-center fs-3 col-lg-6 mx-auto"><i class="bi bi-person-circle"></i></h4>
		<h6 class="col-lg-6 mx-auto mb-3 fw-bold text-center">Changer de mot de passe </h6>
 		<form method="POST" action="{{route('eurazcom.partner.verify')}}" class="col-lg-6 mx-auto">
 			@csrf
 			<div class="form-group mb-3">
 				<input type="text" name="email" value="{{old('email')}}" placeholder="Email address" class="form-control">
 				@error('email')<small class="text-danger">{{$message}}</small>@enderror
 			</div>
 			<div class="form-group mb-3">
 				<input type="text" name="code" value="{{old('code')}}" placeholder="Code partner" class="form-control">
 				@error('code')<small class="text-danger">{{$message}}</small>@enderror
 			</div>
 
 			<div class="text-center mb-5">
 				<button type="submit" class="btn btn-primary btn-sm fw-bold">Envoyer</button>
 			</div>

 		</form>
 	</div>
@stop