@extends('partners.layout.master',['title'=>'Connexion partenaire Eurazcom'])

@section('partner')
	<div class="p-5">
		<h4 class="text-center fs-3 col-lg-6 mx-auto"><i class="bi bi-person-circle"></i></h4>
		<h6 class="col-lg-6 mx-auto mb-3 fw-bold text-center">Mon espace partenaire</h6>
 		<form method="POST" action="{{route('eurazcom.partner.sing-in')}}" class="col-lg-6 mx-auto">
 			@csrf
 			<div class="form-group mb-3">
 				<input type="text" name="identifiantPartners" value="{{old('identifiantPartners')}}" placeholder="Email address" class="form-control">
 			</div>
 			<div class="form-group mb-3">
 				<input type="password" name="password" value="{{old('password')}}" placeholder="Mot de passe" class="form-control">
 			</div>
 			<div class="form-group mb-3 text-center">
 				<a href="{{route('eurazcom.partner.passwordForm')}}">J'ai oublie mon mot de passe</a>
 			</div>
 			<div class="text-center mb-5">
 				<button type="submit" class="btn btn-primary btn-sm fw-bold">Connexion</button>
 			</div>
 			<div class="form-group mb-3 text-center">
 				<a href="{{route('eurazcom.partner.passwordForm')}}">J'ai oublie mon mot de passe</a>
 			</div>
 		</form>
 	</div>
@stop