@extends('partners.layout.master',['title'=>'Devenir partenaire Eurazcom'])
@section('type-section')
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/multiple.css')}}">
@endsection
@section('partner')
 <div class="container-fluid">
 	<form class="register-form" method="POST" action="{{route('eurazcom.partner.store')}}" enctype="Multipart/form-data">
 		@csrf
 		<div class="form-section">
 			<h3 class="fs-5 mb-5">Quel type de partenariat souhaitez-vous ?</h3>
 			<div class="row">
 				<div class="col">
 					<label class="form-label" for="apf">
 						<img src="{{asset('assets/img/partner.png')}}">
 						<legend class="fs-6 fw-bold">Apporteur d'Affaire</legend>
 					</label>
  					<input type="radio" name="type" class="form-check" id="apf" checked>
  				</div>
 				<div class="col">
 					<label class="form-label" for="dist">
 						<img src="{{asset('assets/img/partner.png')}}">
 						<legend class="fs-6 fw-bold">Distributeur de solution</legend>
 					</label>
 					<input type="radio" name="type" class="form-check" id="dist">
 				</div>
 			</div>
 		</div>

 		<div class="form-section">
 			<h3 class="fs-5 mb-5 fw-bold">Informations D'identification personnelle</h3>
 			<div class="row">
 				<div class="col">
 					<div class="form-group mb-3">
 						<input type="text" name="name" value="{{old('name')}}" class="form-control" placeholder="Name" required>
 					</div>
 					<div class="form-group mb-3">
 						<input type="text" name="email" value="{{old('email')}}" class="form-control" placeholder="Email address" required>
 					</div>
 					<div class="form-group mb-3">
 						<input type="text" name="city" value="{{old('city')}}" class="form-control" placeholder="Your city" required>
 					</div>
 				</div>
 				<div class="col">
 					<div class="form-group mb-3">
 						<input type="text" name="lastname" value="{{old('lastname')}}" class="form-control" placeholder="Last Name" required>
 					</div>
 					<div class="form-group mb-3">
 						<input type="text" name="phone" value="{{old('phone')}}" class="form-control" id="phone" required>
 						<span id="error-msg" class="hide text-danger"></span>
 					</div>
 					<div class="form-group mb-3">
 						<select name="country" class="form-select">
							@foreach(allCountries() as $key=>$country)
                              <option value="{{$country}}" 
                                {{geolocate()->iso_code == $key? 'selected':''}}>
                                {{$country}}
                              </option>
                            @endforeach
						</select>
 					</div>
 				</div>
 			</div>
 		</div>

 		<div class="form-section">
 			<h3 class="fs-5 mb-5 fw-bold">Informations De Confirmation de votre identite</h3>
 			<div class="row">
 				<div class="col">
 					<div class="form-group mb-4">
 						<input type="text" name="address" value="{{old('address')}}" class="form-control" placeholder="Your address" required>
 					</div>
 					<div class="form-group mb-4">
 						<label class="form-label fs-6">Photo CNI/Passport</label>
 						<input type="file" name="cni_img" value="{{old('cni_img')}}" class="form-control" required>
 					</div>
 				</div>
 				<div class="col">
 					<div class="form-group mb-4">
 						<input type="number" name="cni_number" value="{{old('cni_number')}}" class="form-control" placeholder="Numero de la CNI" required>
 					</div>
 					<div class="form-group mb-4">
 						<label class="form-label fs-6">Photo CNI/Passport</label>
 						<input type="file" name="cni_img2" value="{{old('cni_img2')}}" class="form-control" required>
 					</div>
 				</div>
 			</div>
 		</div>

 		<div class="form-section">
 			<h3 class="fs-5 mb-5 fw-bold">Photo de profil</h3>
 			 <label class="form-label">Photo de profil</label>
       <input type="file" name="avatar" class="form-control" value="{{old('avatar')}}">
 		</div>

 		<div class="form-navigation  mt-5">
			<button type="button" class="previous btn btn-danger btn-sm fw-bold  float-left">Precedent</button>
			<button type="button" class="next btn btn-info btn-sm fw-bold  float-right">Suivant</button>
			<button type="submit" class="btn btn-primary fw-bold  btn-sm">Envoyer votre demande</button>
		</div>
 	</form>
 </div>
@stop

@section('registerJs')
  <script type="text/javascript" src="{{asset('assets/js/register.js')}}"></script>
@stop