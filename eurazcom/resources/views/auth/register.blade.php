@extends('layouts.app',['title'=>'Create client account'])
@section('registerStyle')
  <link rel="stylesheet" type="text/css" href="{{asset('assets/css/multiple.css')}}">
@stop
@section('content')
<div class="container">
    <div class="row justify-content-centers " style="margin-top: 90px;">
        <div class="col-md-12">
           <div class="col-12 col-md-12 col-lg-6 col-xl-6 mx-auto shadow rounded p-4 ">
               @if(Session::get("warning"))
               <div class="alert alert-warning">{{Session::get("warning")}}</div>
               @endif
              <div class="mt-3">
                  <form method="POST" action="{{route('register')}}" class="register-form" enctype="Multipart/form-data">
                    @csrf
                      <div class="form-section">
                        <h5 class="fs-5  fw-bold">Informations concernant votre projet</h5>
                        <div class="fs-6">
                          Pour pourvoir devenir notre client, vous devez au prealable avoir deposee au moins un projet, lors de la create de votre portail client.
                        </div>
                         <div class="form-group mt-4">
                          <select class="form-control" name="type" required>
                            <option selected disabled>Selectionner une categorie pour votre projet</option>
                            <option value="APPDEV">Developpement</option>
                            <option value="BLD">Branding & Logo Design</option>
                            <option value="MCD">Communication & Marketing</option>
                            <option value="CDP">Conception & Diffusion publicitaire</option>
                          </select>
                        </div>

                        <div class="form-group">
                          <div class="row mt-3">
                            <div class="col-12 col-md-12 col-xl-6 col-lg-6">
                              <input type="text" name="projectName" value="{{old('projectName')}}" class="form-control mb-2" placeholder="project Name" required>
                            </div>
                            <div class="col-lg-6">
                              <input type="text" name="sector" value="{{old('sector')}}" class="form-control mb-2" placeholder="activity type of project" required>
                            </div>
                          </div>
                        </div>

                        <div class="form-group">
                          <label class="form-label">Telecharger le cahier de charge</label>
                             <input type="file" name="chargeBook" value="{{old('chargeBook')}}" class="form-control" required>
                        </div>
                      </div>

                      <div class="form-section">
                          <h5 class="fs-5 fs-5 fw-bold">Vos informations</h5>
                          <div class="row mt-3">
                            <div class="col-md-6">
                                <div class="form-group mb-2">
                                    <input type="text" name="name" value="{{old('name')}}" placeholder="Your first name" class="form-control" required>
                                </div>
                                <div class="form-group mb-2">
                                    <input type="text" name="email" value="{{old('email')}}" placeholder="Email address" class="form-control" required>
                                </div>
                                <div class="form-group mb-2">
                                    <input type="text" name="city" value="{{old('city')}}" placeholder="city" class="form-control"  required>
                                </div>
                                
                            </div>
                            <div class="col-md-6">
                                <div class="form-group mb-2">
                                    <input type="text" name="lastname" value="{{old('lastname')}}" placeholder="Your lastname" class="form-control" required>
                                </div>
                                <div class="form-group mb-2">
                                    <input type="text" name="phone" value="{{old('phone')}}" id="phone" class="form-control" required>
                                     <span id="error-msg" class="hide text-danger"></span>
                                </div>
                                <div class="form-group mb-2">
                                    <select name="country" class="form-select">
                                      @foreach(allCountries() as $key=>$country)
                                        <option value="{{$country}}" 
                                          {{geolocate()->iso_code == $key? 'selected':''}}>
                                          {{$country}}
                                        </option>
                                      @endforeach
                                    </select>
                                </div>
                            </div>
                          </div>
                      </div>
                      
                       <div class="form-section">
                          <h5 class="mb-3 mt-0 fw-bold">Quel est votre status?</h5>
                          <div class="col-lg-12 d-flex justify-content-center">
                              <div class="form-check me-4">
                                <input class="form-check-input" type="radio" name="status" value="{{'Particular'}}" id="particular" checked {{old('status')==='Particular'?'checked':''}}>
                                <label class="form-check-label" for="particular">
                                  Particular
                                </label>
                              </div>
                              <div class="form-check me-4">
                                <input class="form-check-input" type="radio" value="{{'Company'}}" name="status" id="company"{{old('status')==='Company'?'checked':''}}>
                                <label class="form-check-label" for="company">
                                  Entreprise Enregistree
                                </label>
                              </div>
                              <div class="form-check me-4">
                                <input class="form-check-input" type="radio" value="{{'Association'}}" name="status" id="association"{{old('status')==='Association'?'checked':''}}>
                                <label class="form-check-label" for="association">
                                  Association
                                </label>
                              </div>
                              <div class="form-check me-4">
                                <input class="form-check-input" type="radio" value="{{'Organization'}}" name="status" id="organization"{{old('status')==='Organization'?'checked':''}}>
                                <label class="form-check-label" for="organization">
                                  Organisation
                                </label>
                              </div>
                          </div>
                          @include("users.partials.companyInfos")
                       </div>

                       <div class="form-section">
                         <div class="row">
                           <div class="col-lg-6">
                             <div class="form-group mb-2">
                                    <input type="password" name="password" value="{{old('password')}}" placeholder="Your password" class="form-control" required>
                              </div>
                           </div>
                           <div class="col-lg-6">
                             <div class="form-group mb-2">
                                <input type="password" name="c_password" value="{{old('c_password')}}" placeholder="Confirm password" class="form-control" required>
                              </div>
                           </div>
                         </div>
                         <div class="col-lg-12 mt-2 mb-3">
                           En cliquant sur <span class="fw-bold">Devenir client</span>, vous acceptez nos 
                           <a href="">conditions d'utilisation</a>, de meme que notre <a href="">politique d'utilisation des donnees</a>, ainsi que notre <a href=""> charte clientelle</a>.
                         </div>
                       </div>

                      <div class="form-navigation mt-3">
                          <button type="button" 
                                  class="previous btn btn-danger btn-sm fw-bold float-left">
                                Precedent
                          </button>
                          <button type="button" 
                                  class="next btn btn-success btn-sm fw-bold float-right">
                                Suivant
                          </button>
                          <button type="submit" 
                                  class="float-right btn btn-primary btn-sm fw-bold">
                                Devenir client
                          </button>
                      </div>
                  </form>
              </div>
           </div>
        </div>
    </div>
</div>
@endsection

@section('mulitpleForm')
<script type="text/javascript" src="{{asset('assets/js/mulitpleForm.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/project.js')}}"></script>
@stop


