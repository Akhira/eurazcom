@extends('layouts.app',['title'=>'Connect your self'])

@section('content')
 <div class="col-12 col-md-11 col-lg-4 col-xl-4 mx-auto bg-dangers shadow rounded p-3" style="margin-top: 120px;">
  <div class="text-center"><i class="bi bi-person-circle"></i></div>
   <h2 class="fs-5 fw-bold text-center mb-3">Accedez a votre espace client</h2>
   <div class="mb-3 text-center">
      Je ne dispose pas encore de <a href="{{route('register')}}" class="text-primary">compte client</a>
   </div>

   <div class="mt-3">
     <form method="POST" action="">
        @csrf
          <div class="row">
              <div class="col-md-12">
                  <div class="form-group mb-2">
                      <input type="text" name="email" value="" placeholder="Email address" class="form-control">
                  </div>
                  <div class="form-group mb-2">
                      <input type="password" name="password" value="" placeholder="Password" class="form-control">
                  </div>
              </div>
          </div>
          <div class="row mt-2 mb-4">
               <div class="col-md-6">
                    <div class="form-check">
                        <input type="checkbox" name="remember-me" style="margin-left: -20px;" id="remember-me" class="agree-term"  />
                        <label for="remember-me" class="label-agree-term">
                            <span><span></span></span>Remember me
                        </label>
                    </div>
               </div>
               <div class="col-md-6">
                   <a href="#">Forgot password</a>
               </div>
          </div>
          <div class="col-md-6 mx-auto text-center">
              <button type="submit" class="btn btn-primary btn-sm fw-bold">Connexion</button>
          </div>
     </form>
   </div>
 </div>
@endsection
