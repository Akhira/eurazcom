@extends('users.layout.master',['title'=>'Agence de conception de sites & applications web'])

@section('services-style')
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/services.css')}}">
@stop

@section('main')
<section class="services-banner" id="services-banner" 
	style="background: url('{{asset('assets/img/hero-bg.jpg')}}') center no-repeat;">
	<div class="container position-relative ">
		<h1 class="text-center mt-5">Agencce de conception de sites & applications web</h1>
		<div class="banner-description col-xl-8 mx-auto mt-4">
			Vous avez un projet de creation ou de conception d'un site ou d'une application web ? 
			notre agence est partenaire propice pour a realisation de votre projet, notre grande experience acquise au fil du temps nous a permis depuis notre lancement jusqu'aujourdhui de nous frotter a un certains nombre de systmes
			tout aussi complexe les uns que les autres, mais grace a notre equipe en charge de la conception des sites 
			et appications web, nous avons sur relever chaque defis qui nous etait soumis par nos clients de part le monde.

		</div>
	</div>
</section>

<section id="services-presentation" class="services-presentation">
	<div class="container">
		<h6 class="text-center text-uppercase text-muted fw-bold fs-6">Pourquoi se doter d'un site web ?</h6>
		<div class="mt-4 col-xl-8 mx-auto	">
		  Aujourd'hui, plus que jamais, il est primordial de posséder un site Internet. Que ce soit simplement pour que les gens puissent entrer en communication avec vous ou pour agrandir ou fideliser votre reseax clientèle, une page web est le meilleur outil pour atteindre vos objectifs. Contrairement aux autres médias, le Web est accessible de partout à travers le monde, en tout temps et par tout le monde.
          Nous sommes une agences internationale de développement de sites Web de commerce électronique , avec une approche intelligente et systématique pour répondre à vos besoins commerciaux. En gardant à l’esprit votre marché cible, vos produits, vos budgets et vos spécifications, de la conception à la maintenance, nous fournissons les meilleures solutions pour stimuler votre entreprise à un prix abordable.
		</div>
	</div>
</section>

<section id="services-about" class="services-about">
	<div class="container">
		<div class="row">
			<div class="col-xl-6">
				<h6 class="text-uppercase text-muted fw-bold fs-6">pourquoi nous ?</h6>
				<div class="">
					Notre agence forte de sa riche experience acquise au fil du temps a sur convaincre de centaine de client dans le monde, 
				  Notre agence specialise dans la transformation digitale des entreprises et des particuliers est une agence internationale qui vous propose pour sa clientelle une large gamme de prestation en matiere de creation de sites et applications web.
				</div>
			</div>
			<div class="col-xl-6">
				<div class="card border-0">
					<div class="card-body" style="">
						<img src="{{asset('assets/img/dev.jpeg')}}" class="img-fluid  w-100 rounded" height="">
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section id="services-prestations" class="services-prestations">
	<div class="container">
		<h6 class="text-center text-uppercase text-muted fw-bold fs-6">Nos prestations en la matiere</h6>
		<div class="mt-4 col-xl-8 mx-auto">
		  Notre agence specialise dans la transformation digitale des entreprises et des particuliers est une agence internationale qui vous propose pour sa clientelle une large gamme de prestation en matiere de creation de sites et applications web.
		</div>
		<div class="row mt-5">
			<div class="col-xl-3">
				<div class="card">
					<div class="card-header text-center fw-bold fs-6 text-uppercase bg-primary text-light">
						site web vitrine
					</div>
					<div class="card-body">
						<img src="{{asset('assets/img/pc.png')}}" class="img-fluid w-100">
					</div>
					<div class="card-footer">
						Nous concevons pour vous des sites web vitrine, afin de vous permettre d'accroitre la visibilite 
						de votre marque, ainsi que de presenter l'ensemble de vos services.
					</div>
				</div>
 			</div>
 			<div class="col-xl-3">
				<div class="card">
					<div class="card-header text-center fw-bold fs-6 text-uppercase bg-primary text-light">
						landing page
					</div>
					<div class="card-body">
						<img src="{{asset('assets/img/pc.png')}}" class="img-fluid w-100">
					</div>
					<div class="card-footer">
						Nous concevons selon vos besoins des sites landing page, afin de vous permettre d'accroitre la visibilite de votre marque, ainsi que de presenter l'ensemble de vos services.
					</div>
				</div>
 			</div>
 			<div class="col-xl-3">
				<div class="card">
					<div class="card-header text-center fw-bold fs-6 text-uppercase bg-primary text-light">
						e-commerce
					</div>
					<div class="card-body">
						<img src="{{asset('assets/img/pc.png')}}" class="img-fluid w-100">
					</div>
					<div class="card-footer">
						Nous concevons des plateformes de commerces electroniques de toute taille, ainsi que des markets place pour la vente de vos produits et services ce sans vous depacer.
					</div>
				</div>
 			</div>
 			<div class="col-xl-3">
				<div class="card">
					<div class="card-header text-center fw-bold fs-6 text-uppercase bg-primary text-light">
						dropshipping
					</div>
					<div class="card-body">
						<img src="{{asset('assets/img/pc.png')}}" class="img-fluid w-100">
					</div>
					<div class="card-footer">
						Notre agence est specialise dans la creation de plateformes de dropshipping wooCommerce, Prestashop ou encore Shopify, pour la realisation de vos objectifs.
					</div>
				</div>
 			</div>
		</div>
		<div class="row mt-3">
			<div class="col-xl-3">
				<div class="card">
					<div class="card-header text-center fw-bold fs-6 text-uppercase bg-primary text-light">
						sites wordpress
					</div>
					<div class="card-body">
						<img src="{{asset('assets/img/pc.png')}}" class="img-fluid w-100">
					</div>
					<div class="card-footer">
						Notre agence a fait de WordPress son CMS(Content Management System) prefere pour la creation de plateformes web rapides et robustes, pour nos nombreux clients .
					</div>
				</div>
 			</div>
 			<div class="col-xl-3">
				<div class="card">
					<div class="card-header text-center fw-bold fs-6 text-uppercase bg-primary text-light">
						Maintenance de sites web
					</div>
					<div class="card-body">
						<img src="{{asset('assets/img/pc.png')}}" class="img-fluid w-100">
					</div>
					<div class="card-footer">
						Nous assurons une maintenance preventive et curative de vos sites et applications web dans ce domaine, vous permettant ainsi d'etre toujour operationnel.
					</div>
				</div>
 			</div>
 			<div class="col-xl-3">
				<div class="card">
					<div class="card-header text-center fw-bold fs-6 text-uppercase bg-primary text-light">
						site web sur mesure
					</div>
					<div class="card-body">
						<img src="{{asset('assets/img/pc.png')}}" class="img-fluid w-100">
					</div>
					<div class="card-footer">
						Vous avez besoin d'une plateforme web adaptee a votre vision et a votre activite, notre agence vous permet de vous doter d'une plateforme sur mesure, robuste et puissante.
					</div>
				</div>
 			</div>
 			<div class="col-xl-3">
				<div class="card">
					<div class="card-header text-center fw-bold fs-6 text-uppercase bg-primary text-light">
						formations
					</div>
					<div class="card-body">
						<img src="{{asset('assets/img/pc.png')}}" class="img-fluid w-100">
					</div>
					<div class="card-footer">
						Plus qu'une agence nous vous permettons d'acquerir des competences solide en creation et maintenance des plateformes web, ce dans les langages les plus populaires.
					</div>
				</div>
 			</div>
		</div>
	</div>
</section>

<section id="mission" class="mission">
   	<div class="container">
   		<div class="row">
   			<div class="col-xl-6 mt-3">
   				<h5 class="fw-bold fs-6 text-muted text-uppercase">
   					Une agence, une expertise
   				</h5>
   				<h2 class="fw-bold fs-3 text-uppercase">
   					Une agence avec une expertise au service du digital
   				</h2>
   				<div class="accordion accordion-flush" id="accordionFlushExample">
				  <div class="accordion-item">
				    <h2 class="accordion-header" id="flush-headingOne">
				      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
				        Accordion Item #1
				      </button>
				    </h2>
				    <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
				      <div class="accordion-body">Placeholder content for this accordion, which is intended to demonstrate the <code>.accordion-flush</code> class. This is the first item's accordion body.</div>
				    </div>
				  </div>
				  <div class="accordion-item">
				    <h2 class="accordion-header" id="flush-headingOne">
				      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
				        Accordion Item #1
				      </button>
				    </h2>
				    <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
				      <div class="accordion-body">Placeholder content for this accordion, which is intended to demonstrate the <code>.accordion-flush</code> class. This is the first item's accordion body.</div>
				    </div>
				  </div>
				  <div class="accordion-item">
				    <h2 class="accordion-header" id="flush-headingTwo">
				      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
				        Accordion Item #2
				      </button>
				    </h2>
				    <div id="flush-collapseTwo" class="accordion-collapse collapse" aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushExample">
				      <div class="accordion-body">Placeholder content for this accordion, which is intended to demonstrate the <code>.accordion-flush</code> class. This is the second item's accordion body. Let's imagine this being filled with some actual content.</div>
				    </div>
				  </div>
				  <div class="accordion-item">
				    <h2 class="accordion-header" id="flush-headingThree">
				      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
				        Accordion Item #3
				      </button>
				    </h2>
				    <div id="flush-collapseThree" class="accordion-collapse collapse" aria-labelledby="flush-headingThree" data-bs-parent="#accordionFlushExample">
				      <div class="accordion-body">Placeholder content for this accordion, which is intended to demonstrate the <code>.accordion-flush</code> class. This is the third item's accordion body. Nothing more exciting happening here in terms of content, but just filling up the space to make it look, at least at first glance, a bit more representative of how this would look in a real-world application.</div>
				    </div>
				  </div>
				  <div class="accordion-item">
				    <h2 class="accordion-header" id="flush-headingThree">
				      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
				        Accordion Item #3
				      </button>
				    </h2>
				    <div id="flush-collapseThree" class="accordion-collapse collapse" aria-labelledby="flush-headingThree" data-bs-parent="#accordionFlushExample">
				      <div class="accordion-body">Placeholder content for this accordion, which is intended to demonstrate the <code>.accordion-flush</code> class. This is the third item's accordion body. Nothing more exciting happening here in terms of content, but just filling up the space to make it look, at least at first glance, a bit more representative of how this would look in a real-world application.</div>
				    </div>
				  </div>
				</div>
   			</div>
   			<div class="col-xl-6 ">
   				 <div class="card border-0">
   				 	<div class="card-body system-img">
   				 		<img src="{{asset('assets/img/joy.jpeg')}}" class="img-fluid w-100">
   				 	</div>
   				 </div>
    		</div>
   		</div>
   	</div>
</section>

<section id="ask-quotation" class="ask-quotation">
	   	<div class="bg-dark mt-0 p-3">
			<div class="container">
				<div class="row">
					<div class="col-xl-7">
						<h1 class="text-light fw-bold fs-4 pt-3 text-uppercase">
						Avez-vous un projet?</h1>
						<p class="mt-4 text-light fw-bold col-xl-9 pb-3" style="border-bottom: 5px solid #FFF;">
						Un document concu pour faciliter la comprehension de nos differents services et vous detailler de nos references en images et en terme de projets.
						</p>
					</div>
					<div class="col-xl-4 d-flex justify-content-center align-items-center">
						<a href="" class="btn btn-primary btn-sm text-uppercase p-2 fw-bold" style="font-size: .90rem;">
						telecharger notre document
						</a>
					</div>
				</div>
			</div>
		</div>
</section>

<section id="mission" class="mission">
   	<div class="container">
   		<div class="row">
   			<div class="col-xl-6 ">
   				 <div class="card border-0">
   				 	<div class="card-body system-img">
   				 		<img src="{{asset('assets/img/joy.jpeg')}}" class="img-fluid w-100">
   				 	</div>
   				 </div>
    		</div>
   			<div class="col-xl-6 mt-3">
   				<h5 class="fw-bold fs-6 text-muted text-uppercase">
   					Une agence, une expertise
   				</h5>
   				<h2 class="fw-bold fs-3 text-uppercase">
   					Une agence avec une expertise au service du digital
   				</h2>
   				<div class="accordion accordion-flush" id="accordionFlushExample">
				  <div class="accordion-item">
				    <h2 class="accordion-header" id="flush-headingOne">
				      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
				        Accordion Item #1
				      </button>
				    </h2>
				    <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
				      <div class="accordion-body">Placeholder content for this accordion, which is intended to demonstrate the <code>.accordion-flush</code> class. This is the first item's accordion body.</div>
				    </div>
				  </div>
				  <div class="accordion-item">
				    <h2 class="accordion-header" id="flush-headingOne">
				      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
				        Accordion Item #1
				      </button>
				    </h2>
				    <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
				      <div class="accordion-body">Placeholder content for this accordion, which is intended to demonstrate the <code>.accordion-flush</code> class. This is the first item's accordion body.</div>
				    </div>
				  </div>
				  <div class="accordion-item">
				    <h2 class="accordion-header" id="flush-headingTwo">
				      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
				        Accordion Item #2
				      </button>
				    </h2>
				    <div id="flush-collapseTwo" class="accordion-collapse collapse" aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushExample">
				      <div class="accordion-body">Placeholder content for this accordion, which is intended to demonstrate the <code>.accordion-flush</code> class. This is the second item's accordion body. Let's imagine this being filled with some actual content.</div>
				    </div>
				  </div>
				  <div class="accordion-item">
				    <h2 class="accordion-header" id="flush-headingThree">
				      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
				        Accordion Item #3
				      </button>
				    </h2>
				    <div id="flush-collapseThree" class="accordion-collapse collapse" aria-labelledby="flush-headingThree" data-bs-parent="#accordionFlushExample">
				      <div class="accordion-body">Placeholder content for this accordion, which is intended to demonstrate the <code>.accordion-flush</code> class. This is the third item's accordion body. Nothing more exciting happening here in terms of content, but just filling up the space to make it look, at least at first glance, a bit more representative of how this would look in a real-world application.</div>
				    </div>
				  </div>
				  <div class="accordion-item">
				    <h2 class="accordion-header" id="flush-headingThree">
				      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
				        Accordion Item #3
				      </button>
				    </h2>
				    <div id="flush-collapseThree" class="accordion-collapse collapse" aria-labelledby="flush-headingThree" data-bs-parent="#accordionFlushExample">
				      <div class="accordion-body">Placeholder content for this accordion, which is intended to demonstrate the <code>.accordion-flush</code> class. This is the third item's accordion body. Nothing more exciting happening here in terms of content, but just filling up the space to make it look, at least at first glance, a bit more representative of how this would look in a real-world application.</div>
				    </div>
				  </div>
				</div>
   			</div>
   		</div>
   	</div>
</section>

<section class="realizations" id="realizations">
	   	<h5 class="text-center fw-bold fs-5 text-uppercase mb-3">
	   		Nos realisations
	   	</h5>
	   	<div class="text-center fw-bold text-uppercase text-muted" style="font-size: .9rem;">Plus de 200 projets realises pour nos clients dans plus de 22 pays
	   	</div>
	   	<div class="realizations-slider swiper">
	   		<div class="swiper-wrapper">
	   			 
		   		<div class="swiper-slide">
	   				<div class="realization-wrap">
		              <div class="card realization-item border-0">
		              	<div class="card-body">
		              		<img src="{{asset('assets/img/pc.png')}}" class="realization-img img-fluid">
		              		<div class="text-danger fw-bold fs-4 text-capitalize">
 		              			 djdksd  sddsd
 		              		</div>
		              	</div>
		              </div>
		          	</div>
		   		</div>
		   		<div class="swiper-slide">
	   				<div class="realization-wrap">
		              <div class="card realization-item border-0">
		              	<div class="card-body">
		              		<img src="{{asset('assets/img/pc.png')}}" class="realization-img img-fluid">
		              	</div>
		              </div>
		          	</div>
		   		</div>
		   		<div class="swiper-slide">
	   				<div class="realization-wrap">
		              <div class="card realization-item border-0">
		              	<div class="card-body">
		              		<img src="{{asset('assets/img/pc.png')}}" class="realization-img img-fluid">
		              	</div>
		              </div>
		          	</div>
		   		</div>
	   		</div>
 	   	</div>
	   	<div class="swiper-pagination"></div>
	   	<div class="text-center mt-5">
	   		<a href="{{route('eurazcom.portfolio')}}" class="btn btn-outline-primary fw-bold text-uppercase" style="font-size: .9rem;">
				voir toutes nos realisations
			</a>
	   	</div>
</section>

<section id="service-projects" class="service-projects">
	<div class="container bg-danger rounded" id="word-section">
		<div class="col-lg-8 mx-auto">
			<div class="container">
				<h2 class="text-center text-uppercase fw-bold py-5">
					Avez-vous un projet ? Etes-vous a la recherche d'un prestataire ?
				</h2>
				<div>
					{{config('eurazcom.name')}} est l'agence qu'il vous faut ou que vous soyez dans le monde 
					nous vous accompagne dans votre projet digital. 
				</div>
				<div>En cas de besoin nous sommes a votre disposition 24H/24 et 7J/7 en contactant l'un de ces numeros</div>
				<div>+237699939633 / +237 680638086</div>
				<div>
					<a href="{{route('eurazcom.contact.create')}}" class="btn btn-primary btn-sm text-uppercase fw-bold">
						Contactez-nous
					</a>
				</div>
			</div>
		</div>
	</div>
</section>
@stop