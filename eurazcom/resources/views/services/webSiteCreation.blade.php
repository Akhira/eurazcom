@extends('users.layout.master',['title'=>'Agence de conception de sites & applications web'])

@section('services-style')
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/services.css')}}">
@stop

@section('main')
<section class="services-banner" id="services-banner" 
	style="background: url('{{asset('assets/img/hero-bg.jpg')}}') center no-repeat;">
	 <div class="col-12 col-lg-8 col-xl-8 mx-auto position-relative">
	 	<h3 class="text-center">Agencce de conception de sites & applications web</h3>
	 	<p class="text-center text-light">
	 		Vous avez un projet de creation ou de conception d'un site ou d'une application web ? 
			notre agence est le partenaire qu'il vous faut pour la realisation de votre projet, notre grande experience acquise au fil du temps nous a permis depuis notre lancement jusqu'aujourdhui de nous frotter a un certains nombre de systmes tout aussi complexe les uns que les autres, mais grace a notre equipe en charge de la conception des sites et appications web, nous avons sur relever chaque defis qui nous etait soumis par nos clients de part le monde, et ce pour leur grance satisfaction.
	 	</p>
	 </div>
</section>

<section id="presentation" class="presentation">
	<div class="col-10 col-md-11 col-xl-8 col-lg-8 mx-auto">
		<h1 class="text-center text-muted fs-6 fw-bold text-uppercase mb-3">C'est quoi un site web ? pourquoi en avoir?</h1>
		<p>
			Aujourd'hui, plus que jamais, il est primordial de posséder un site Internet. Que ce soit simplement pour que les gens puissent entrer en communication avec vous ou pour agrandir ou fideliser votre reseax clientèle, une page web est le meilleur outil pour atteindre vos objectifs. Contrairement aux autres médias, le Web est accessible de partout à travers le monde, en tout temps et par tout le monde.
          Nous sommes une agences internationale de développement de sites Web de commerce électronique , avec une approche intelligente et systématique pour répondre à vos besoins commerciaux. En gardant à l’esprit votre marché cible, vos produits, vos budgets et vos spécifications, de la conception à la maintenance, nous fournissons les meilleures solutions pour stimuler votre entreprise à un prix abordable.
		</p>
	</div>
</section>

<section id="services-about" class="services-about">
	<div class="col-10 col-md-11 col-xl-8 col-lg-8 mx-auto">
		<div class="row">
			<div class="col-lg-6 col-xl-6">
				<h6 class="text-uppercase text-muted fw-bold fs-6">pourquoi nous ?</h6>
			</div>
		</div>
	</div>
</section>
@stop