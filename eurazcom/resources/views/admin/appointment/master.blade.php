@extends('admin.layout.master',['title'=>'Admin Dashboad Appointments'])

@section('master')
<div class="content-wrapper">
	<div class="content-header">
	      <div class="container-fluid">
	        <div class="row mb-2">
	          <div class="col-sm-6">
	            <h1 class="m-0 text-dark">Rendez-vous client</h1>
	          </div><!-- /.col -->
	          <div class="col-sm-6">
	            <ol class="breadcrumb float-sm-right">
	              <li class="breadcrumb-item">
	              	  <a href="{{route('admin.appointments.former')}}" class="btn btn-primary btn-sm {{activeRoute('admin.appointments.former')}} font-weight-bold">Ancient RDV</a>
	              </li>
	              <li class="breadcrumb-item">
	              	 {{$apppointments->count()}} apppointments
	              </li>
	            </ol>
	          </div> 
	        </div> 
	      </div> 
	</div>
	<section class="content">
	  @yield('appointment')
	</section>
</div>
@stop