@extends('admin.appointment.master',['title'=>'Admin Dashboad Appointments'])

@section('appointment')
  <div class="container-fluid">
	<div class="row">
	  <div class="col-12">
	    <div class="card">
	      <div class="card-body">
	        <table id="example2" class="table table-bordered table-hover">
	          <thead>
	              <tr>
		                <th>Nom</th>
		                <th>Telephone</th>
	                <th>Adresse Email</th>
	                <th>Pays de residence</th>
	                <th>Type</th>
	                <th>Objet</th>
	                <th>Date</th>
	                <th>Heure</th>
	                <th>Personne a voir</th>
		                <th>Actions</th>
	              </tr>
	          </thead>
	          <tbody>
	          	@forelse($apppointments as $apppointment)
	              <tr>
		                <td>
		                	<img alt="Avatar" class="table-avatar" src="{{asset($apppointment->avatar)}}" alt="apppointment avatar">
		                </td>
		                <td>{{$apppointment->name}}</td>
	                <td>{{$apppointment->phone}}</td>
	                <td>{{$apppointment->email}}</td>
	                <td>{{$apppointment->country}}</td>
	                <td>{{$apppointment->type}}</td>
	                <td>{{$apppointment->object}}</td>
	                <td>{{$apppointment->date}}</td>
	                <td>{{$apppointment->hour}}</td>
	                <td>{{$apppointment->target}}</td>
		                <td>
	                	<div class="row">
	                		<div class="col-lg-3">
	                			<a href="{{route('admin.apppointments.show',$apppointment->id)}}"><i class="fas fa-eye"></i>View</a>
	                		</div>
	                	</div>		                	
	                </td>
	              </tr>
	           @empty
	           	<tr>
	           		<td colspan="10" class="text-center bg-light">
	           			<img src="{{asset('assets/img/no.jpeg')}}" alt="no image" width="120">
	           			<p>Aucun apppointment enregistre</p>
	           		</td>
	           	</tr>
	           @endforelse
	          </tbody>
	         </table>
	      </div>
	    </div>
	   </div>
	</div>
  </div>
  @stop