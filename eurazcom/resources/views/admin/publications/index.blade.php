@extends('admin.layout.master',['title'=>'Admin Dashboad Publications'])

@section('master')
<div class="content-wrapper">
	<div class="content-header">
	      <div class="container-fluid">
	        <div class="row mb-2">
	          <div class="col-sm-6">
	            <h1 class="m-0 text-dark">Publications</h1>
	          </div><!-- /.col -->
	          <div class="col-sm-6">
	            <ol class="breadcrumb float-sm-right">
	              <li class="breadcrumb-item">
	              	<a href="{{route('admin.publication.create')}}" class="btn btn-primary btn-sm font-weight-bold">
		              	<i class="fa fa-plus"></i> Nouvelle publication
	              	</a>
	              </li>
	            </ol>
	          </div> 
	        </div> 
	      </div> 
	</div>
	<section class="content">
	  <div class="container-fluid">
	    <div class="row">
	      <div class="col-12">
	        <div class="card">
	          <div class="card-body">
	            <table id="example2" class="table table-bordered table-hover">
	              <thead>
		              <tr>
 		                <th>Titre</th>
		                <th>Auteur</th>
		                <th>Date de publication</th>
 		                <th>Actions</th>
		              </tr>
	              </thead>
	              <tbody>
	              	@forelse($publications as $publication)
		              <tr>
  		                <td>{{$publication->title}}</td>
		                <td> dsd</td>
		                <td>{{$publication->publish == 1? $publication->date_publication : "Brouillon"}}</td>
 		                <td>
		                	<div class="row">
		                		<div class="col-lg-3">
		                			<a target="__blank" href="{{route('admin.publication.show',[$publication->id,$publication->slug])}}"><i class="fa fa-eye"></i></a>
		                		</div>
		                		<div class="col-lg-3">
		                			<a target="__blank" href="{{route('admin.publication.show',[$publication->id,$publication->slug])}}"><i class="fa fa-eye"></i></a>
		                		</div>
		                	</div>		                	
		                </td>
		              </tr>
	               @empty
	               	<tr>
	               		<td colspan="7" class="text-center">
	               			<img src="{{asset('assets/img/no.jpeg')}}" alt="no image" width="120">
	               			<p>Aucune publication</p>
	               		</td>
	               	</tr>
	               @endforelse
	              </tbody>
	             </table>
	          </div>
	        </div>
	       </div>
	    </div>
	  </div>
	</section>
</div>
@stop