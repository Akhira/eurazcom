<!DOCTYPE html>
<html>
<head>
	<title>{{$publication->title}}</title>
	<link href="{{asset('assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
	<link rel="stylesheet" href="{{asset('assets/plugins/fontawesome-free/css/all.min.css')}}">
</head>
<style type="text/css">
	.publication-img{
		height: 300px;
	}
	.publication-img img{
		width: 100%;
		height: 300px;
	}
</style>
<body>
	<div class="container-fluid mt-1">
		<div class="row">
			<div class="col-lg-3 p-5">
				<ul class="ml-4 mb-0 fa-ul text-muted">
					<li>Auteur #: </li>
					<li class="mb-5">Date publication #: {{$publication->publish == 1 ? $publication->date_publication : "Mode Brouillon"}}</li>
					<li class="mt-3">
						<a href="" class="btn btn-primary btn-sm fw-bold"><i class="fa fa-pencil-alt"> Editer la publication</i></a>
					</li>
					<li class="mt-3">
						<a href="" class="btn btn-primary btn-sm fw-bold"><i class="fa fa-pencil-alt"> Editer le contenu</i></a>
					</li>
					<li class="mt-3">
						<a href="" class="btn btn-danger btn-sm fw-bold"><i class="fa fa-trash-alt"> Supprimer</i></a>
					</li>
				</ul>
 			</div>
			<div class="col-lg-6 bg-dangers">
				<div class="publication-img">
					<img src="{{asset($publication->image)}}" class="img-fluid">
				</div>
				<div class="publication-title pt-3 fw-bold fs-5">
					{{$publication->title}}
				</div>
				<div class="row publication-title text-muted p-1">
					<div class="col-lg-6">Publie par : Emma Nya  le {{$publication->date_publication??''}} </div>
					<div class="col-lg-6">Date de derniere modification : {{substr($publication->updated_at??'',0,10)}} </div>
				</div>
				<div class="publication-title text-muted">
							<ul class="list-inline">
							<li class="list-inline-item"><i class="fas fa-comments"></i> 0</li>
							<li class="list-inline-item ms-5"><i class="fas fa-eye"></i> 0 </li>
							<li class="list-inline-item ms-5"><i class="far fa-thumbs-up "> 0 </i></li>
						</ul>
					</div>
				<div class="publication-content">
					{{$publication->content}}
				</div>
			</div>
			<div class="col-lg-3"></div>
		</div>
	</div>
</body>
</html>