@extends('admin.layout.master',['title'=>'Admin Dashboad Publication'])

@section('master')
<div class="content-wrapper">
	<div class="content-header">
	      <div class="container-fluid">
	        <div class="row mb-2">
	          <div class="col-sm-6">
	            <h1 class="m-0 text-dark">Add Publication</h1>
	          </div><!-- /.col -->
	          <div class="col-sm-6">
	            <ol class="breadcrumb float-sm-right">
	              <li class="breadcrumb-item">
	              	<a href="{{route('admin.publications')}}" class="btn btn-primary btn-sm font-weight-bold">
		              	<i class="fa fa-plus"></i> All Publications
	              	</a>
	              </li>
	            </ol>
	          </div> 
	        </div> 
	      </div> 
	</div>
    <section class="content">
      <div class="container-fluid">
        <div class="row">
           <div class="col-md-7 offset-3">
             <div class="card card-primary">
               <form role="form" method="POST" action="{{route('admin.publication.store')}}" enctype="Multipart/form-data">
               		@csrf
	                <div class="card-body">
	                	<div class="row">
		               		<div class="col-lg-12">
		               			<div class="form-group">
				                    <label for="title">Titre de la publication</label>
				                    <input type="text" class="form-control" id="title" placeholder="Enter title" name="title" value="{{old('title')}}">
				                    @error('title')<small class="text-danger">{{$message}}</small>@enderror
				                </div>
 				                <div class="form-group">
				                    <label for="client">Image</label>
				                    <input type="file" class="form-control" id="image" name="image" value="{{old('image')}}">
				                    @error('image')<small class="text-danger">{{$message}}</small>@enderror
				                </div>
 		               		</div>
			             </div>
			             <div class="row">
			             	<div class="col-lg-12">
					         	<div class="form-group">
				                    <label for="presentation">Presentation de la realization</label>
				                    <textarea name="presentation" placeholder="Presentation" class="form-control" rows="5">{{old('presentation')}}</textarea>
				                    @error('presentation')<small class="text-danger">{{$message}}</small>@enderror
				                </div>
				            </div>
				            <div class="col-lg-12">
				            	<div class="form-group">
				            		<label class="form-label">Tags</label>
				            	</div>
				            </div>
				         </div>
			         </div>
			          
	                 <div class="card-footer text-center">
	                  <button type="submit" class="btn btn-primary font-weight-bold">Ajouter au systeme</button>
	                </div>
              </form>
            </div>
           </div>
         </div>
       </div> 
    </section>
 </div>
@stop