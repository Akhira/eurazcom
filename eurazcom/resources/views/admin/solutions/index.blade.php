@extends('admin.layout.master',['title'=>'Admin Dashboad solutions'])

@section('master')
<div class="content-wrapper">
	<div class="content-header">
	      <div class="container-fluid">
	        <div class="row mb-2">
	          <div class="col-sm-6">
	            <h1 class="m-0 text-dark">Solutions</h1>
	          </div><!-- /.col -->
	          <div class="col-sm-6">
	            <ol class="breadcrumb float-sm-right">
	              <li class="breadcrumb-item">
	              	<a href="{{route('admin.solutions.create')}}" class="btn btn-primary btn-sm font-weight-bold">
		              	<i class="fa fa-plus"></i> Nouvelle solution
	              	</a>
	              </li>
	            </ol>
	          </div> 
	        </div> 
	      </div> 
	</div>
	<section class="content">
	  <div class="container-fluid">
	    <div class="row">
	      <div class="col-12">
	        <div class="card">
	          <div class="card-body">
	            <table id="example2" class="table table-bordered table-hover">
	              <thead>
		              <tr>
 		                <th>Name</th>
		                <th>Category</th>
 		                <th>Description</th>
		                <th>Actions</th>
		              </tr>
	              </thead>
	              <tbody>
	              	@forelse($solutions as $solution)
		              <tr>
 		                <td>{{$solution->title}}</td>
		                <td>{{$solution->category}}</td>
 		                <td>{{$solution->description}}</td>
		                <td>
		                	<div class="row">
		                		<div class="col-lg-3">
		                			<a href="{{route('admin.solutions.show',$solution->id)}}"><i class="fa fa-eye"></i></a>
		                		</div>
		                		<div class="col-lg-3">
		                			<a href="{{route('admin.solutions.edit',$solution->id)}}"><i class="fa fa-pen"></i></a>
		                		</div>
		                		<div class="col-lg-3">
		                			<a href="{{route('admin.solutions.images',$solution->id)}}"><i class="fa fa-image"></i></a>
		                		</div>
		                		<div class="col-lg-3">
		                			<a href="#"
		                				onclick="if (confirm('Voulez-vous vraiment supprimer cette solution ??')) {
		                					event.preventDefault();
		                					document.getElementById('delete-form-{{$solution->id}}').submit();
		                				}else{event.preventDefault();}">
		                				<i class="fa fa-trash"></i>
		                			</a>
		                		</div>
		                	</div>		                	
		                </td>
		              </tr>
		              <form class="d-none" id="delete-form-{{$solution->id}}" method="POST" action="{{route('admin.solutions.delete',$solution->id)}}">
		              	@csrf
		              	{{method_field("DELETE")}}
		              </form>
	               @empty
	               	<tr>
	               		<td colspan="5" class="text-center">
	               			<img src="{{asset('assets/img/no.jpeg')}}" alt="no image" width="120">
	               			<p>Aucune solution</p>
	               		</td>
	               	</tr>
	               @endforelse
	              </tbody>
	             </table>
	          </div>
	        </div>
	       </div>
	    </div>
	  </div>
	</section>
</div>
@stop