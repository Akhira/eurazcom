@extends('admin.layout.master',['title'=>'Admin Dashboad solutions'])

@section('master')
<div class="content-wrapper">
	<div class="content-header">
	      <div class="container-fluid">
	        <div class="row mb-2">
	          <div class="col-sm-6">
	            <h1 class="m-0 text-dark">Add Solutions</h1>
	          </div><!-- /.col -->
	          <div class="col-sm-6">
	            <ol class="breadcrumb float-sm-right">
	              <li class="breadcrumb-item">
	              	<a href="{{route('admin.solutions.index')}}" class="btn btn-primary btn-sm font-weight-bold">
		              	<i class="fa fa-plus"></i> All solutions
	              	</a>
	              </li>
	            </ol>
	          </div> 
	        </div> 
	      </div> 
	</div>
    <section class="content">
      <div class="container-fluid">
        <div class="row">
           <div class="col-md-8 offset-2">
             <div class="card card-primary">
               <form role="form" method="POST" action="{{route('admin.solutions.store')}}" enctype="Multipart/form-data">
               		@csrf
	                <div class="card-body">
	                	<div class="row">
		               		<div class="col-lg-6">
		               			<div class="form-group">
				                    <label for="name">Nom de la solution</label>
				                    <input type="text" class="form-control" id="name" placeholder="Enter name" name="name" value="{{old('name')}}">
				                    @error('name')<small class="text-danger">{{$message}}</small>@enderror
				                </div>
				                <div class="form-group">
				                    <label for="category">Categorie</label>
				                     <select class="form-control" name="category">
				                     	<option selected disabled>Selectionner une categorie</option>
				                     	<option value="Hopitaux">Gestion Hopitaux</option>
				                     	<option value="Etablissement">Gestion Etablissement</option>
				                     	<option value="Hotel">Gestion Hotel</option>
				                     	<option value="Entreprise">Gestion Entreprise</option>
				                     </select>
				                    @error('category')<small class="text-danger">{{$message}}</small>@enderror
				                </div>
		               		</div>
		                    <div class="col-lg-6"> 
			                  <div class="form-group">
			                    <label for="image">Image de premier plan</label>
			                    <input type="file" class="form-control" id="image" name="image" value="{{old('image')}}">
			                    @error('image')<small class="text-danger">{{$message}}</small>@enderror
			                  </div>
			                  <div class="form-group">
			                    <label for="video">Video de presentation</label>
			                    <input type="file" class="form-control" id="video" name="video" value="{{old('video')}}">
			                    @error('video')<small class="text-danger">{{$message}}</small>@enderror
			                  </div>
 		                    </div>
			             </div>
			             <div class="row">
			             	<div class="col-lg-12">
					         	<div class="form-group">
				                    <label for="image">Presentation de la solution</label>
				                    <textarea name="description" class="form-control" rows="5">{{old('description')}}</textarea>
				                    @error('description')<small class="text-danger">{{$message}}</small>@enderror
				                </div>
				            </div>
				         </div>
			         </div>
			          
	                 <div class="card-footer text-center">
	                  <button type="submit" class="btn btn-primary font-weight-bold">Ajouter au systeme</button>
	                </div>
              </form>
            </div>
           </div>
         </div>
       </div> 
    </section>
 </div>
@stop