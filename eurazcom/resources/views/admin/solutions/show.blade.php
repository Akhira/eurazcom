@extends('admin.layout.master',['title'=>$solution->title])

@section('master')
<div class="content-wrapper">
	<div class="content-header" >
	      <div class="container-fluid">
	        <div class="row mb-0">
	          <div class="col-lg-9">
	            <h1 class="m-0 text-dark" style="font-size: 25px;">
	              {{$solution->title}}
 	            </h1>
	          </div><!-- /.col -->
	          <div class="col-lg-3">
	            <div class="row">
            		<div class="col-lg-3">
            			<a href="{{route('admin.solutions.edit',$solution->id)}}" class="text-dark"><i class="fa fa-pen"></i></a>
            		</div>
            		<div class="col-lg-3">
            			<a href="{{route('admin.solutions.images',$solution->id)}}" class="text-dark"><i class="fa fa-image"></i></a>
            		</div>
            		<div class="col-lg-3">
            			<a href="" class="text-dark"
            				onclick="if (confirm('Voulez-vous supprimer cette solution ?')) {
            					event.preventDefault();
            					document.getElementById('form-delete-{{$solution->id}}').submit();
            				}else{event.preventDefault();}">
            				<i class="fa fa-trash"></i>
            			</a>
            		</div>
	            </div>
	          </div> 
	        </div> 
	      </div> 
	</div>
	<section class="content">
	  <div class="container-fluid bg-light">
	     <div class="row">
	     	<div class="col-lg-9">
	     		<div class="mt-0 mb-4 col-lg-11 mx-auto">
	     			<img src="{{asset($solution->image)}}" alt="solution image" class="img-fluid col-lg-11">
	     		</div>
	     		<div class="mt-1">
 	     			{{$solution->description}}
	     		</div>
	     		<div class="mt-3">
	     			<h6 class="font-weight-bold mb-2 text-decoration-underline">Images associees</h6>
	     			<div class="portfolio-details-slider swiper">
		     			<div class="row swiper-wrapper">
		     				<div class="col-lg-3 mb-2 swiper-slide">
		     					<a href="{{asset($solution->image)}}" data-gallery="portfolioGallery"  class="portfolio-lightbox">
		     						<img src="{{asset($solution->image)}}" class="img-fluid rounded">
		     					</a>
		     				</div>
		     				<div class="col-lg-3 mb-2 swiper-slide">
		     					<a href="{{asset($solution->image)}}" data-gallery="portfolioGallery"  class="portfolio-lightbox">
		     						<img src="{{asset($solution->image)}}" class="img-fluid rounded">
		     					</a>
		     				</div>
		     			</div>	
		     		</div>
	     		</div>
	     	</div>
	     	<div class="col-lg-3">
	     		<div class="p-1">Name : {{$solution->title}}</div>
 	     		<div class="p-1">Category : {{$solution->category}}</div>
 	     		<div class="p-1">URL : 
	     			@if(!empty($solution->url))
	     				<a href="{{$solution->url}}">{{substr($solution->url,11,20)}}</a> 
	     			@else 
	     				<i>Undefined</i>
	     			@endif
	     		</div>
	     		<div class="p-1">Video demo : <a href="">Regarder la video</a></div>
	     	</div>
	     </div>
	  </div>
	</section>
</div>

<form method="POST" action="{{route('admin.solutions.delete',$solution->id)}}" id="form-delete-{{$solution->id}}" class="d-none">
	@csrf
	{{method_field("DELETE")}}
</form>
@stop

