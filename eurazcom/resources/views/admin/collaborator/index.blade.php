@extends('admin.layout.master',['title'=>'Admin Dashboad'])

@section('master')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Collaborators</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item">
              	<a href="{{route('admin.collaborator.create')}}" class="font-weight-bold btn btn-primary btn-sm">
              		<i class="fa fa-plus"></i> Nouveau Collaborateur</a>
              </li>
             </ol>
          </div>
        </div>
      </div> 
    </section>

    <section class="content">
      <div class="card card-solid">
        <div class="card-body pb-0">
          <div class="row d-flex align-items-stretch">
          	 @foreach($collaborators as $collaborator) 
	            <div class="col-12 col-sm-6 col-md-4 d-flex align-items-stretch">
	              <div class="card bg-light">
	                <div class="card-header text-muted border-bottom-0">
	                  {{$collaborator->domaine}}
	                </div>
	                <div class="card-body pt-0">
	                  <div class="row">
	                    <div class="col-7">
	                      <h2 class="lead"><b>{{$collaborator->name.' '.$collaborator->lastnem}}</b></h2>
	                      <p class="text-muted text-sm"><b>About: </b>{{$collaborator->skills}}</p>
	                      <ul class="ml-4 mb-0 fa-ul text-muted">
	                        <li class="small"><span class="fa-li"><i class="fas fa-lg fa-building"></i></span> 
	                        	Address: {{$collaborator->address}}</li>
	                        <li class="small"><span class="fa-li"><i class="fas fa-lg fa-phone"></i></span> 
	                        	Phone #: {{$collaborator->phone}}</li>
	                        <li class="small"><span class="fa-li"><i class="fas fa-lg fa-globe"></i></span> 
	                        	Nationnality #: {{$collaborator->nationality}}
	                        </li>
	                        <li class="small"><span class="fa-li"><i class="fas fa-lg fa-home"></i></span> 
	                        	City #: {{$collaborator->city}}
	                        </li>
	                        <li class="small"><span class="fa-li"><i class="fas fa-lg fa-book"></i></span> 
	                        	Niveau #: {{$collaborator->degree}}
	                        </li>
	                        <li class="small"><span class="fa-li"><i class="fas fa-lg fa-user"></i></span> 
	                        	Status #: {{$collaborator->block==0?'Actif':"Suspendu"}}
	                        </li>
	                        <li class="small"><span class="fa-li"><i class="fas fa-lg fa-envelope"></i></span> 
	                        	 {{$collaborator->email}}
	                        </li>
	                        <li class="small"><span class="fa-li"><i class="fas fa-lg fa-user"></i></span> 
	                        	Diplome de #: {{$collaborator->establishment}}
	                        </li>
	                      </ul>
	                    </div>
	                    <div class="col-5 text-center">
 	                      <img src="{{asset($collaborator->avatar =='default.jpg'? 'assets/img/'.$collaborator->avatar 
 	                      														 : $collaborator->avatar)}}" 
 	                       alt="" class="img-circle img-fluid">
 	                       <span class="text-success font-weight-bold">Matricule: {{$collaborator->matricule}}</span>
	                    </div>
	                  </div>
	                </div>
	                <div class="card-footer">
	                  <div class="text-right">
	                    <a href="#" class="btn btn-sm bg-teal">
	                      <button class="btn btn-sm font-weight-bold" 
	                      onclick="event.preventDefault();
	                      document.getElementById('form-block-{{$collaborator->id}}').submit();">
	                  		{{$collaborator->block == 1?"Debloquer":"Suspendre"}}
	                  	 </button>
	                    </a>
	                    
	                  </div>
	                  <form method="post" action="{{route('admin.collaborator.block',$collaborator->id)}}"
	                  	id="form-block-{{$collaborator->id}}" class="d-none">
	                  		@csrf
	                  		{{method_field('PUT')}}
	                  	</form>
	                </div>
	              </div>
	            </div>
	         @endforeach 
        </div>
        <div class="card-footer">
          {{$collaborators->links()}}
        </div>
       </div>
     </section>
   </div>
@stop