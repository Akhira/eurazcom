@extends('admin.layout.master',['title'=>'Admin Dashboad Collaborator'])

@section('master')
<div class="content-wrapper">
	<div class="content-header">
	      <div class="container-fluid">
	        <div class="row mb-2">
	          <div class="col-sm-6">
	            <h1 class="m-0 text-dark">Add Collaborator</h1>
	          </div> 
	          <div class="col-sm-6">
	            <ol class="breadcrumb float-sm-right">
	              <li class="breadcrumb-item">
	              	<a href="{{route('admin.collaborators')}}" class="btn btn-primary btn-sm font-weight-bold">
		              	<i class="fas fa-folder"></i> All Collaborator
	              	</a>
	              </li>
	            </ol>
	          </div> 
	        </div> 
	      </div> 
	</div>
    <section class="content">
      <div class="container-fluid">
        <div class="row">
           <div class="col-md-8 offset-2">
             <div class="card card-primary">
               <form role="form" method="POST" action="{{route('admin.collaborator.store')}}" enctype="Multipart/form-data">
               		@csrf
               		<input type="hidden" name="matricule" value="{{matricule()}}">
               		<input type="hidden" name="verification_code" value="{{sha1(time())}}">
	                <div class="card-body">
	                	<div class="row">
		               		<div class="col-lg-6">
		               			<div class="form-group">
 				                    <input type="text" class="form-control" id="name" placeholder="Enter name" name="name" value="{{old('name')}}">
				                    @error('name')<small class="text-danger">{{$message}}</small>@enderror
				                </div>
 				                <div class="form-group">
 				                    <input type="text" class="form-control" name="email" value="{{old('email')}}" placeholder="Enter email address">
				                    @error('email')<small class="text-danger">{{$message}}</small>@enderror
				                </div>
				                <div class="form-group">
 				                    <input type="text" class="form-control" name="nationality" value="{{old('nationality')}}" placeholder="Nationnality">
				                    @error('nationality')<small class="text-danger">{{$message}}</small>@enderror
				                </div>
				                <div class="form-group">
 				                    <input type="text" class="form-control" name="address" value="{{old('address')}}" placeholder="Address">
				                    @error('address')<small class="text-danger">{{$message}}</small>@enderror
				                </div>
				                <div class="form-group">
 				                    <input type="text" class="form-control" name="degree" value="{{old('degree')}}" placeholder="Niveau etude">
				                    @error('degree')<small class="text-danger">{{$message}}</small>@enderror
				                </div>
				                <div class="form-group">
 				                    <input type="password" class="form-control" name="password" value="{{old('password')}}" placeholder="Password">
				                    @error('password')<small class="text-danger">{{$message}}</small>@enderror
				                </div>
		               		</div>
		               		<div class="col-lg-6">
		               			<div class="form-group">
 				                    <input type="text" class="form-control" placeholder="Lastname" name="lastname" value="{{old('lastname')}}">
				                    @error('lastname')<small class="text-danger">{{$message}}</small>@enderror
				                </div>
 				                <div class="form-group">
 				                    <input type="text" class="form-control" name="phone" value="{{old('phone')}}" placeholder="Enter phone">
				                    @error('phone')<small class="text-danger">{{$message}}</small>@enderror
				                </div>
				                <div class="form-group">
 				                    <input type="text" class="form-control" name="city" value="{{old('city')}}" placeholder="city">
				                    @error('city')<small class="text-danger">{{$message}}</small>@enderror
				                </div>
				                <div class="form-group">
 				                    <input type="text" class="form-control" name="domaine" value="{{old('domaine')}}" placeholder="domaine">
				                    @error('domaine')<small class="text-danger">{{$message}}</small>@enderror
				                </div>
				                <div class="form-group">
 				                    <input type="text" class="form-control" name="establishment" value="{{old('establishment')}}" placeholder="etablissement frequente">
				                    @error('establishment')<small class="text-danger">{{$message}}</small>@enderror
				                </div>
				                <div class="form-group">
 				                    <input type="password" class="form-control" name="c_password" value="{{old('c_password')}}" placeholder="Confirm password">
				                    @error('c_password')<small class="text-danger">{{$message}}</small>@enderror
				                </div>
		               		</div>
			             </div>
			             <div class="row">
			             	<div class="col-lg-12">
				             	<div class="form-group">
				             		<input type="text" name="skills" value="{{old('skills')}}" class="form-control" placeholder="Comptence ex:Dev python/dev java,...">
				             		<div style="font-size: .8rem;">Si Vous disposez de plusieurs competences, separez-les par un slash EX: competence1/competence2</div>
				             		@error('skills')<small class="text-danger">{{$message}}</small>@enderror
				             	</div>
				            </div>
			             </div>
			         </div>
			          
	                 <div class="card-footer text-center">
	                  <button type="submit" class="btn btn-primary font-weight-bold">Ajouter au systeme</button>
	                </div>
              </form>
            </div>
           </div>
         </div>
       </div> 
    </section>
</div>
@stop