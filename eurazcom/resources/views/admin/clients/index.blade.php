@extends('admin.layout.master',['title'=>'Admin Dashboad clients'])

@section('master')
<div class="content-wrapper">
	<div class="content-header">
	      <div class="container-fluid">
	        <div class="row mb-2">
	          <div class="col-sm-6">
	            <h1 class="m-0 text-dark">Nos Partenaires apporteurs d'affaires</h1>
	          </div> 
	          <div class="col-sm-6">
	            <ol class="breadcrumb float-sm-right">
	              <li class="breadcrumb-item">
	              	 {{$users->count()}} clients
	              </li>
	            </ol>
	          </div> 
	        </div> 
	      </div> 
	</div>
	  <div class="container-fluid">
	    <div class="row">
	      <div class="col-12">
	        <div class="card">
	          <div class="card-body">
	            <table id="example2" class="table table-bordered table-hover">
	              <thead>
		              <tr>
 		                <th>Photo</th>
 		                <th>Name</th>
		                <th>Lastname</th>
		                <th>Phone</th>
		                <th>Email</th>
		                <th>Country</th>
		                <th>city</th>
		                <th>Address</th>
		                <th>Status</th>
		                <th>Actions</th>
		              </tr>
	              </thead>
	              <tbody>
	              	@forelse($users as $user)
		              <tr>
 		                <td>
 		                	<img src="{{asset('assets/img/default.jpg')}}" width="30" height="30" class="img-circle img-fluid">
 		                </td>
 		                <td>{{$user->name}}</td>
		                <td>{{$user->lastname}}</td>
		                <td>{{$user->phone}}</td>
		                <td>{{$user->email}}</td>
		                <td>{{$user->country}}</td>
		                <td>{{$user->city}}</td>
		                <td>{{$user->address}}</td>
		                <td>{{$user->status}}</td>
 		                <td>
		                	<div class="row">
		                		<div class="col-lg-12 text-center">
		                			<a href="{{route('admin.client.show',$user->id)}}"><i class="fas fa-eye"></i></a>
		                		</div>
		                	</div>		                	
		                </td>
		              </tr>
	               @empty
	               	<tr>
	               		<td colspan="10" class="text-center bg-light">
	               			<img src="{{asset('assets/img/no.jpeg')}}" alt="no image" width="120">
	               			<p>Aucun client enregistre</p>
	               		</td>
	               	</tr>
	               @endforelse
	              </tbody>
	             </table>
	          </div>
	        </div>
	       </div>
	    </div>
	  </div>
	</section>
</div>
@stop