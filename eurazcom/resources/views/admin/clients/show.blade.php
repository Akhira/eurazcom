@extends('admin.layout.master',['title'=>$user->name." ".$user->lastname])

@section('master')
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Client Profil : <span class="text-info font-weight-bold">{{$user->status}}</span></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item">
                <a href="{{route('admin.clients')}}" class="btn btn-primary btn-sm font-weight-bold">
                  <i class="fa fa-arrow-left"></i> Back to clients list
                </a>
              </li>
             </ol>
          </div> 
        </div> 
      </div> 
    </div>
 
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-6">
            <div class="card card-primary card-outline">
              <div class="card-header">
                <h5 class="m-0">Representant infos</h5>
              </div>
              <div class="card-body">
                <div class="row">
                  <div class="col-lg-6">
                    <img src="{{asset('assets/img/default.jpg')}}" width="200" height="200" class="img-circle img-fluid">
                    <div class="bg-dark mt-1 col-lg-8 text-center"><strong>Representatnt avatar</strong></div>
                  </div>
                  <div class="col-lg-6">
                    <div class="p-1">Name: <strong class="text-success font-weight-bold">{{$user->name." ".$user->lastname}}</strong></div>
                    <div class="p-1">Email: <strong class="text-success font-weight-bold">{{$user->email}}</strong></div>
                    <div class="p-1">Phone: <strong class="text-success font-weight-bold">{{$user->phone}}</strong></div>
                    <div class="p-1">Location: <strong class="text-success font-weight-bold">{{$user->address}}</strong></div>
                    <div class="p-1">Residence: <strong class="text-success font-weight-bold">{{$user->city."-".$user->country}}</strong></div>
                    <div class="p-1">Entr Role: <strong class="text-success font-weight-bold">{{$user->users_role}}</strong></div>
                    <div class="p-1">Total project: <strong class="text-success font-weight-bold">{{"20"}}</strong></div>
                  </div>
                </div>
               </div>
            </div>

            <div class="card card-primary card-outline">
              <div class="card-header">
                <h5 class="m-0">Entreprise infos</h5>
              </div>
              <div class="card-body">
                <div class="row">
                  <div class="col-lg-6 text-center">
                     <img src="{{asset('assets/img/default.jpg')}}" width="200" height="200" class="img-circle img-fluid">
                     <div class="bg-dark mt-1 col-lg-8 mx-auto text-center"><strong>Entreprise Logo</strong></div>
                  </div>
                  <div class="col-lg-6">
                    <div class="p-1">Entr/Assoc/Org name: 
                      <strong class="text-success font-weight-bold">{{$user->organization_name}}</strong></div>
                    <div class="p-1">Entr Phone: 
                      <strong class="text-success font-weight-bold">{{$user->organization_phone}}</strong></div>
                    <div class="p-1">Entr Email: 
                      <strong class="text-success font-weight-bold">{{$user->organization_email}}</strong></div>
                    <div class="p-1">Entr Address: 
                      <strong class="text-success font-weight-bold">{{$user->organization_address}}</strong></div>
                  </div>
                </div>
               </div>
            </div>
           </div>

          <div class="col-lg-6">
            <div class="card card-primary card-outline">
              <div class="card-header">
                <h5 class="m-0">Projects Listing</h5>
              </div>
              <div class="card-body">
                <table id="example2" class="table table-bordered table-hover">
                  <thead>
                    <tr>
                      <th>Name</th>
                      <th>Type</th>
                      <th>Category</th>
                      <th>budget</th>
                      <th>status</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                  </tbody>
                </table>
               </div>
            </div>

            <div class="card card-primary card-outline">
              <div class="card-header">
                <h5 class="m-0">Featured</h5>
              </div>
              <div class="card-body">
                <h6 class="card-title">Special title treatment</h6>

                <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                <a href="#" class="btn btn-primary">Go somewhere</a>
              </div>
            </div>
          </div>
         </div>
       </div> 
    </div>
  </div>
@stop