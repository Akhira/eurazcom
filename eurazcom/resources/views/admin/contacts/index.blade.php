@extends('admin.layout.master',['title'=>'Admin Dashboad Messages'])

@section('master')
<div class="content-wrapper">
	<div class="content-header">
	      <div class="container-fluid">
	        <div class="row mb-2">
	          <div class="col-sm-6">
	            <h1 class="m-0 text-dark">Messages recus</h1>
	          </div><!-- /.col -->
	          <div class="col-sm-6">
	            <ol class="breadcrumb float-sm-right">
		              <li class="breadcrumb-item">
		              	 <button type="button" class="btn btn-danger btn-sm font-weight-bold" 
		              	 onclick="event.preventDefault();
		              	 document,getElementById('delete-all').submit();">
		              	 	<i class="fa fa-trash"></i> Supprimer tous
		              	 </button>
		              </li>
	            </ol>
	          </div> 
	        </div> 
	      </div> 
	</div>
	<section class="content">
	  <div class="container-fluid">
	    <div class="row">
	      <div class="col-12">
	        <div class="card">
	          <div class="card-body">
	            <table id="example2" class="table table-bordered table-hover">
	              <thead>
		              <tr>
  		                <th>Nom</th>
 		                <th>Adresse Email</th>
 		                <th>Subject</th>
		                <th>Message</th>
 		                <th>Action</th>
		              </tr>
	              </thead>
	              <tbody>
	              	@forelse($contacts as $contact)
		              <tr>
 		                <td>
 		                	<img alt="Avatar" class="table-avatar" src="{{asset($contact->avatar)}}" alt="contact avatar">
 		                </td>
 		                <td>{{$contact->name}}</td>
 		                <td>{{$contact->email}}</td>
 		                <td>{{$contact->subject}}</td>
		                <td>{{$contact->message}}</td>
   		                <td>
		                	<div class="row">
		                		<div class="col-lg-6">
		                			<a href="{{route('admin.contacts.show',$contact->id)}}"><i class="fas fa-folder"></i>View</a>
		                		</div>
		                		<div class="col-lg-6">
		                			<button type="button" class="btn btn-danger btn-sm" 
		                				onclick="event.preventDefault(); 
		                				document.getElementById('delete-uniq-{{$contact->id}}').submit();">
		                				<i class="fa fa-trash"></i> 
		                			</button>
 		                		</div>
		                	</div>		                	
		                </td>
		              </tr>
		              <form class="d-none" method="POST" action="{{route('contacts.destroy',$contact->id)}}" id="delete-uniq-{{$contact->id}}">
						@csrf
						{{method_field('DELETE')}}
					  </form>
	               @empty
	               	<tr>
	               		<td colspan="10" class="text-center bg-light">
	               			<img src="{{asset('assets/img/no.jpeg')}}" alt="no image" width="120">
	               			<p>Aucun contact enregistre</p>
	               		</td>
	               	</tr>
	               @endforelse
	              </tbody>
	             </table>
	          </div>
	        </div>
	       </div>
	    </div>
	  </div>
	</section>
</div>
<form class="d-none" method="POST" action="{{route('admin.contacts.destroyAll')}}" id="delete-all">
	@csrf
	{{method_field('DELETE')}}
</form>
@stop