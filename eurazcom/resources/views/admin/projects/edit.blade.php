@extends('admin.projects.quotations.master',['title'=>'Admin Dashboad Quotation'])

@section('quotations')
 <div class="container-fluid">
    <section class="content">
      <div class="container-fluid">
        <div class="row">
           <div class="col-md-6 offset-3">
             <div class="card card-primary">
               <form method="POST" action="{{route('admin.project.update',$project->code)}}">
               		@csrf
               		{{method_field("PATCH")}}
 	                <div class="card-body">
			             <div class="row mt-5">
			             	<div class="col-lg-12">
				             	<div class="form-group">
				             		<label class="form-label">Budget final</label>
				             		<input type="text" name="budget" value="{{$project->final_budget}}" class="form-control" placeholder="Budget final">
				             		@error('budget')<small class="text-danger">{{$message}}</small>@enderror
 				             	</div>
				            </div>
			             </div>
			         </div>
			          
	                 <div class="card-footer text-center">
	                  <button type="submit" class="btn btn-primary font-weight-bold">Modifier</button>
	                </div>
              </form>
            </div>
           </div>
         </div>
       </div> 
    </section>
 </div>
@stop