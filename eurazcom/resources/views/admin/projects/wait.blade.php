@extends('admin.projects.master',['title'=>'Admin Dashboad Projects'])

@section('projects')
 <div class="container-fluid">
	    <div class="row">
	      <div class="col-12">
	        <div class="card">
	          <div class="card-body">
	            <table id="example2" class="table table-bordered table-hover">
	              <thead>
		              <tr>
 		                <th>Code</th>
		                <th>Nom Projet</th>
		                <th>Secteur activite</th>
		                <th>Type</th>
 		                <th>Budget</th>
 		                <th>Status</th>
		                <th>Action</th>
		              </tr>
	              </thead>
	              <tbody>
	              	@forelse($projects as $project)
		              <tr>
 		                <td>{{$project->code}}</td>
		                <td>{{$project->title}}</td>
		                <td>{{$project->activity??'Undefined'}}</td>
		                <td>{{$project->type}}</td>
 		                <td>{{$project->final_budget}}</td>
		                <td>{{$project->status}}</td>
		                <td>
		                	<div class="row">
		                		<div class="col-lg-3">
		                			<a href="{{route('admin.projects.show',$project->id)}}" target="__blank"><i class="fa fa-eye"></i></a>
		                		</div>
		                	</div>		                	
		                </td>
		              </tr>
	               @empty
	               	<tr>
	               		<td colspan="11" class="text-center bg-light">
	               			<img src="{{asset('assets/img/no.jpeg')}}" alt="no image" width="120">
	               			<p>Aucun projet en cours actuellement</p>
	               		</td>
	               	</tr>
	               @endforelse
	              </tbody>
	             </table>
	          </div>
	        </div>
	       </div>
	    </div>
	  </div>
@stop