<!DOCTYPE html>
<html>
<head>
	<title>{{$project->title}} - {{config('eurazcom.name')}}</title>
	<link href="{{asset('assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
	 <link rel="stylesheet" href="{{asset('assets/plugins/fontawesome-free/css/all.min.css')}}">
</head>
<body class="bg-darks">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-3 ps-5 pt-5">
				<p>Project Name : <strong> {{$project->title}}</strong></p>
				<p>Project Sector : <strong> {{$project->activity}} </strong></p>
				<p>Type of project: <strong> {{$project->type}} </strong></p>
				<p>Budget: <strong> {{$project->final_budget}} </strong></p>
			</div>
			<div class="col-lg-6 shadow" style="height: 100vh; overflow-y: scroll;">
				contenus pdf
			</div>
			<div class="col-lg-3 p-5">
			 <div class="row mb-3">
			 	<p class="mb-3">Status : <span class="text-danger fw-bold">{{$project->status}} </span></p>
				@if($project->status === "In process")
					<a href="{{route('admin.project.edit',$project->code)}}"  class="btn btn-info btn-sm fs-5 mb-3  fw-bold">
						<i class="fas fa-pencil-alt"></i> Editer le budget
					</a>
				@endif
				@if($project->status == "Waiting for treatment")
				<button class="btn btn-info btn-sm fs-5 mb-3  fw-bold"
				onclick="event.preventDefault();document.getElementById('start-treat-{{$project->code}}').submit();">
					<i class="fa fa-clock"></i> Debuter l'etude
				</button>
				@endif
 				<button type="button" class="btn btn-info btn-sm fs-5  fw-bold"><i class="fa fa-download"></i> Telecharger</button> 
  			</div>
			</div>
		</div>
	</div>

	<form class="d-none" id='start-treat-{{$project->code}}' method="POST" action="{{route('admin.project.startraitment',$project->code)}}">
		@csrf
		{{method_field("PATCH")}}
	</form>
</body>
</html>