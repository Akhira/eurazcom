@extends('admin.layout.master')

@section('master')
 <div class="content-wrapper">
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Listing Projets</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              
              <li class="breadcrumb-item">
                <a href="{{route('admin.projects')}}" class="btn btn-success btn-sm font-weight-bold {{activeRoute('admin.projects')}}">
                  Projects en cours
                </a>
              </li>
              <li class="breadcrumb-item">
                <a href="{{route('admin.projects.waiting')}}" class="btn btn-success btn-sm font-weight-bold {{activeRoute('admin.projects.waiting')}}">
                  Projects en attentes
                </a>
              </li>
              <li class="breadcrumb-item">
                <a href="{{route('admin.projects.deliver')}}" class="btn btn-success btn-sm font-weight-bold {{activeRoute('admin.projects.deliver')}}">
                  Projects livres
                </a>
              </li>
              
            </ol>
          </div>
        </div>
      </div>
    </div>
    <section class="content">
      <div class="container-fluid">
        <div class="container">
          
        </div>
      </div>
        @yield('projects')
    </section>
@stop