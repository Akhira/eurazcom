@extends('admin.layout.master')

@section('master')
 <div class="content-wrapper">
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Demandes de devis</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item">
                <a href="{{route('admin.quotations')}}" class="btn btn-success btn-sm font-weight-bold
                {{activeRoute('admin.quotations')}} ">Toutes les demandes</a>
              </li>
              <li class="breadcrumb-item">
                <a href="{{route('admin.quotations.traited')}}" class="btn btn-success btn-sm font-weight-bold {{activeRoute('admin.quotations.traited')}}">
                  Demandes en attentes approbations
                </a>
              </li>
            </ol>
          </div>
        </div>
      </div>
    </div>
    <section class="content">
      <div class="container-fluid">
        <div class="container">
          
        </div>
      </div>
        @yield('quotations')
    </section>
@stop