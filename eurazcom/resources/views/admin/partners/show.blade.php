@extends('admin.layout.master',['title'=>$partner->name." ".$partner->lastname])

@section('master')
 <div class="content-wrapper">

    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Partner Profil code 
              <span class="font-weight-bold text-success" style="font-size: 1.5rem">{{$partner->code}}</span>
            </h1>
          </div> 
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              @if($partner->accepted == 0)
                <li class="breadcrumb-item">
                  <a href="{{route('admin.partner.accepted',$partner->code)}}" class="btn btn-success btn-sm font-weight-bold"
                  onclick="event.preventDefault(); document.getElementById('validate-{{$partner->code}}').submit();">
                    Valider la candidature
                  </a>
                </li>
              @endif
              <li class="breadcrumb-item">
                <a href="{{route('admin.partner.block',$partner->code)}}" class="btn btn-danger btn-sm font-weight-bold"
                  onclick="event.preventDefault(); document.getElementById('block-{{$partner->code}}').submit();">
                  @if($partner->block == 0)
                   Suspendre le partenaire
                  @else
                    Reabilite le partenaire
                  @endif
                </a>
              </li>
              <li class="breadcrumb-item">
                <a href="{{route('admin.partners')}}" class="btn btn-primary btn-sm font-weight-bold">
                  <i class="fa fa-arrow-left"></i> Back to partners list
                </a>
              </li>
             </ol>
          </div> 
        </div> 
      </div> 
    </div>
 
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-6">
            <div class="card card-primary card-outline">
               <div class="card-body">
                <div class="row">
                  <div class="col-lg-6">
                    <img src="{{asset('assets/img/default.jpg')}}" width="200" height="200" class="img-circle img-fluid">
                  </div>
                  <div class="col-lg-6">
                    <div class="p-1">Name: <strong class="text-success font-weight-bold">{{$partner->name." ".$partner->lastname}}</strong></div>
                    <div class="p-1">Email: <strong class="text-success font-weight-bold">{{$partner->email}}</strong></div>
                    <div class="p-1">Phone: <strong class="text-success font-weight-bold">{{$partner->phone}}</strong></div>
                    <div class="p-1">Location: <strong class="text-success font-weight-bold">{{$partner->address}}</strong></div>
                    <div class="p-1">Residence: 
                        <strong class="text-success font-weight-bold">{{$partner->city."-".$partner->country}}</strong>
                    </div>
                    <div class="p-1">Total project: <strong class="text-success font-weight-bold">{{"20"}}</strong></div>
                  </div>
                </div>
               </div>
            </div>
            
          </div>

          <div class="col-lg-6">
            <div class="card card-primary card-outline">
              <div class="card-body">
                <div class="row">
                  <div class="col-lg-6">
                    <div class="p-1">Categorie: <strong class="text-success font-weight-bold">{{$partner->type}}</strong></div>
                    <div class="p-1">CNI number: <strong class="text-success font-weight-bold">{{$partner->cni}}</strong></div>
                    <div class="p-1">Agence signature:
                      <strong class="text-success font-weight-bold">
                        {{$partner->acceptep==1?'Valider':"Non valider"}}
                      </strong>
                    </div>
                    <div class="p-1">Status:
                      <strong class="text-success font-weight-bold">
                        {{$partner->block==0 ? 'Actif' : "Suspendu"}}
                      </strong>
                    </div>
                  </div>
                  <div class="col-lg-6">
                    <div class="mb-2" style="max-width: 1200px; max-height: 100px">
                      <img src="{{asset($partner->cni_image1)}}" alt="cni 1">
                    </div>
                    <div style="max-width: 1200px; max-height: 100px">
                      <img src="{{asset($partner->cni_image1)}}" alt="cni 1">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-12">
           <div class="card card-primary card-outline">
              <div class="card-header">
                <h5 class="m-0">Projects Listing</h5>
              </div>
              <div class="card-body">
                <table id="example2" class="table table-bordered table-hover">
                  <thead>
                    <tr>
                      <th>Name</th>
                      <th>Category</th>
                      <th>Nom client</th>
                      <th>Localisation</th>
                      <th>Budget propose</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                     <tr>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                  </tbody>
                </table>
               </div>
            </div>
         </div>
       
      </div> 
    </div>
  </div>

  <form class="d-none" method="POST" action="{{route('admin.partner.block',$partner->code)}}" id="block-{{$partner->code}}">
    @csrf
    {{method_field('PATCH')}}
  </form>

  <form class="d-none" method="POST" action="{{route('admin.partner.accepted',$partner->code)}}" id="validate-{{$partner->code}}">
    @csrf
    {{method_field('PATCH')}}
  </form>
@stop