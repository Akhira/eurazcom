@extends('admin.partners.layout.main',['title'=>'Recherches results'])

@section('partners')
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-body">
            <table id="example2" class="table table-bordered table-hover">
              <thead>
	              <tr>
	                <th>Photo</th>
	                <th>Code</th>
	                <th>Nom</th>
	                <th>Nom de famille</th>
 	                <th>Pays </th>
	                <th>Ville </th>
	                <th>Adresse</th>
	                <th>Actions</th>
	              </tr>
              </thead>
              <tbody>
              	@forelse($partners as $partner)
	              <tr>
	                <td>
	                	<img src="{{asset('assets/img/default.jpg')}}" width="30" height="30" class="img-fluid rounded-circle">
	                </td>
	                <td class="text-success font-weight-bold">{{$partner->code}}</td>
	                <td>{{$partner->name}}</td>
	                <td>{{$partner->lastname}}</td>
 	                <td>{{$partner->country}}</td>
	                <td>{{$partner->city}}</td>
	                <td>{{$partner->address}}</td>
		                <td>
	                	<div class="row">
	                		<div class="col-lg-12 text-center">
	                			<a href="{{route('admin.partners.show',$partner->code)}}"><i class="fas fa-eye"></i></a>
	                		</div>
	                	</div>		                	
	                </td>
	              </tr>
               @empty
               	<tr>
               		<td colspan="9" class="text-center bg-light">
               			<img src="{{asset('assets/img/no.jpeg')}}" alt="no image" width="120">
               			<p>Aucun partner trouve</p>
               		</td>
               	</tr>
               @endforelse
              </tbody>
             </table>
          </div>
        </div>
       </div>
    </div>
  </div>
@stop