@extends('admin.layout.master')

@section('master')
<div class="content-wrapper">
	<div class="content-header">
	      <div class="container-fluid">
	        <div class="row mb-2">
	          <div class="col-sm-4">
	          	@if(!Route::is('admin.partners'))
		          	<a href="{{route('admin.partners')}}" class="btn btn-primary btn-sm font-weight-bold">
	                  <i class="fa fa-arrow-left"></i> Back to partners list
	                </a>
	            @else
		            <h1 class="m-0 text-dark">Nos Partenaires</h1>
                @endif
	          </div>
	          <div class="col-sm-4">
	          	<div class="col-sm-12">
					<form method="POST" action="{{route('admin.partners.search')}}" autocomplete="off">
						@csrf
						<div class="input-group">
							<input type="search" name="search" value="{{request()->search??''}}" placeholder="Nom ou code du partenaire" 
							class="form-control">
							<button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
						</div>
				 	</form>
				</div>
	          </div> 
	          <div class="col-sm-4">
	            <ol class="breadcrumb float-sm-right">
	              <li class="breadcrumb-item">
	              	 {{$partners->count()}} Partners
	              </li>
	            </ol>
	          </div> 
	        </div> 
	      </div> 
	</div>
	<section class="content">
	   @yield('partners')
	</section>
</div>
@stop