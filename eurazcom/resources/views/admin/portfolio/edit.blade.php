@extends('admin.layout.master',['title'=>$realization->title])

@section('master')
<div class="content-wrapper">
	<div class="content-header">
	      <div class="container-fluid">
	        <div class="row mb-2">
	          <div class="col-sm-6">
	            <h1 class="m-0 text-dark">Update Realization</h1>
	          </div><!-- /.col -->
	          <div class="col-sm-6">
	            <ol class="breadcrumb float-sm-right">
	              <li class="breadcrumb-item">
	              	<a href="{{route('admin.portfolio.index')}}" class="btn btn-primary btn-sm font-weight-bold">
		              	<i class="fa fa-plus"></i> All Realizations
	              	</a>
	              </li>
	            </ol>
	          </div> 
	        </div> 
	      </div> 
	</div>
    <section class="content">
      <div class="container-fluid">
        <div class="row">
           <div class="col-md-10 offset-1">
             <div class="card card-primary">
               <form role="form" method="POST" action="{{route('admin.portfolio.update',$realization->id)}}" enctype="Multipart/form-data">
               		@csrf
               		{{method_field('PUT')}}
	                <div class="card-body">
	                	<div class="row">
		               		<div class="col-lg-6">
		               			<div class="form-group">
				                    <label for="name">Nom de la realization</label>
				                    <input type="text" class="form-control" id="name" placeholder="Enter name" name="name" value="{{$realization->title}}">
				                    @error('name')<small class="text-danger">{{$message}}</small>@enderror
				                </div>
				                <div class="form-group">
				                    <label for="category">Categorie</label>
				                     <select class="form-control" name="category">
 				                     	<option value="Web Site">Site web</option>
				                     	<option value="Web Application" 
				                     		{{$realization->category==='Web Application'?'selected':''}}>Application web
				                        </option>
				                     	<option value="Design" 
				                     		{{$realization->category==='Design'?'selected':''}}>
				                     		Branding & Logo design
				                     	</option>
				                     	<option value="Communication" 
				                     		{{$realization->category==='Communication'?'selected':''}}>Communication
				                     	</option>
				                     	<option value="Marketing" 
				                     		{{$realization->category==='Marketing'?'selected':''}}>
				                     		Marketing
				                     	</option>
				                     	<option value="Publicite" {{$realization->category==='Publicite'?'selected':''}}>Publicite</option>
				                     	<option value="Progiciel" {{$realization->category==='Progiciel'?'selected':''}}>Progiciel</option>
				                     </select>
				                    @error('category')<small class="text-danger">{{$message}}</small>@enderror
				                </div>
				                <div class="form-group">
				                    <label for="client">Client</label>
				                    <input type="text" class="form-control" id="client" name="client" value="{{$realization->client}}" placeholder="Entrer le Client">
				                    @error('client')<small class="text-danger">{{$message}}</small>@enderror
				                </div>
				                <div class="form-group">
				                    <label for="client">Country</label>
				                    <input type="text" class="form-control" id="client" name="country" value="{{$realization->country}}" placeholder="Entrer le country">
				                    @error('country')<small class="text-danger">{{$message}}</small>@enderror
				                </div>
		               		</div>
		                    <div class="col-lg-6"> 
			                  <div class="form-group">
			                    <label for="date">Date de realisation</label>
			                    <input type="date" class="form-control" id="date" name="date" value="{{$realization->date}}">
			                    @error('date')<small class="text-danger">{{$message}}</small>@enderror
			                  </div>
			                  <div class="form-group">
			                    <label for="date">Url de realisation</label>
			                    <input type="text" class="form-control" id="url" name="url" value="{{$realization->url}}">
			                    @error('url')<small class="text-danger">{{$message}}</small>@enderror
			                  </div>
			                  <div class="form-group">
			                    <label for="image">Image de premier plan</label>
			                    <input type="file" class="form-control" id="image" name="image" value="{{$realization->image}}">
			                    @error('image')<small class="text-danger">{{$message}}</small>@enderror
			                  </div>
			                  <div class="form-group">
			                    <label for="video">Ajouter d'autres images</label>
			                    <input type="file" class="form-control" id="video" name="video" value="{{$realization->video}}">
			                    @error('video')<small class="text-danger">{{$message}}</small>@enderror
			                  </div>
 		                    </div>
			             </div>
			             <div class="row">
			             	<div class="col-lg-12">
					         	<div class="form-group">
				                    <label for="image">Presentation de la realization</label>
				                    <textarea name="description" class="form-control" rows="5">{{$realization->description}}</textarea>
				                    @error('description')<small class="text-danger">{{$message}}</small>@enderror
				                </div>
				            </div>
				         </div>
			         </div>
			          
	                 <div class="card-footer text-center">
	                  <button type="submit" class="btn btn-primary font-weight-bold">Ajouter au systeme</button>
	                </div>
              </form>
            </div>
           </div>
         </div>
       </div> 
    </section>
 </div>
@stop