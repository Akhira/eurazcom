@extends('admin.layout.master',['title'=>$realization->title])

@section('master')
<div class="content-wrapper">
	<div class="content-header" >
	      <div class="container-fluid">
	        <div class="row mb-0">
	          <div class="col-lg-9">
	            <h1 class="m-0 text-dark" style="font-size: 25px;">
	            	Projet {{$realization->title}} pour le compte de 
	            	<span class="font-weight-bold">{{$realization->client}}</span>
	            </h1>
	          </div><!-- /.col -->
	          <div class="col-lg-3">
	            <div class="row">
            		<div class="col-lg-3">
            			<a href="{{route('admin.portfolio.edit',$realization->id)}}" class="text-dark"><i class="fa fa-pen"></i></a>
            		</div>
            		<div class="col-lg-3">
            			<a href="{{route('admin.portfolio.images',$realization->id)}}" class="text-dark"><i class="fa fa-image"></i></a>
            		</div>
            		<div class="col-lg-3">
            			<a href="" class="text-dark"
            				onclick="if (confirm('Voulez-vous supprimer cette realization ?')) {
            					event.preventDefault();
            					document.getElementById('form-delete-{{$realization->id}}').submit();
            				}else{event.preventDefault();}">
            				<i class="fa fa-trash"></i>
            			</a>
            		</div>
	            </div>
	          </div> 
	        </div> 
	      </div> 
	</div>
	<section class="content">
	  <div class="container-fluid bg-light">
	     <div class="row">
	     	<div class="col-lg-9">
	     		<div class="mt-0 mb-4 col-lg-11 mx-auto">
	     			<img src="{{asset($realization->image)}}" alt="realization image" class="img-fluid col-lg-11">
	     		</div>
	     		<div class="mt-1">
 	     			{{$realization->description}}
	     		</div>
	     		<div class="mt-3">
	     			<h6 class="font-weight-bold mb-2 text-decoration-underline">Images associees</h6>
	     			<div class="portfolio-details-slider swiper">
		     			<div class="row swiper-wrapper">
		     				<div class="col-lg-3 mb-2 swiper-slide">
		     					<a href="{{asset($realization->image)}}" data-gallery="portfolioGallery"  class="portfolio-lightbox">
		     						<img src="{{asset($realization->image)}}" class="img-fluid rounded">
		     					</a>
		     				</div>
		     				<div class="col-lg-3 mb-2 swiper-slide">
		     					<a href="{{asset($realization->image)}}" data-gallery="portfolioGallery"  class="portfolio-lightbox">
		     						<img src="{{asset($realization->image)}}" class="img-fluid rounded">
		     					</a>
		     				</div>
		     			</div>	
		     		</div>
	     		</div>
	     	</div>
	     	<div class="col-lg-3">
	     		<div class="p-1">Name : {{$realization->title}}</div>
	     		<div class="p-1">Client : {{$realization->client??'Undefined'}}</div>
	     		<div class="p-1">Country : {{$realization->country??'Undefined'}}</div>
	     		<div class="p-1">Category : {{$realization->category}}</div>
	     		<div class="p-1">Date : {{$realization->date}}</div>
	     		<div class="p-1">URL : 
	     			@if(!empty($realization->url))
	     				<a href="{{$realization->url}}">{{substr($realization->url,11,20)}}</a> 
	     			@else 
	     				<i>Undefined</i>
	     			@endif
	     		</div>
	     		<div class="p-1">Video demo : <a href="">Regarder la video</a></div>
	     	</div>
	     </div>
	  </div>
	</section>
</div>

<form method="POST" action="{{route('admin.portfolio.delete',$realization->id)}}" id="form-delete-{{$realization->id}}" class="d-none">
	@csrf
	{{method_field("DELETE")}}
</form>
@stop

