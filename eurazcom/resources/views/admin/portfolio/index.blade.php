@extends('admin.layout.master',['title'=>'Admin Dashboad realization'])

@section('master')
<div class="content-wrapper">
	<div class="content-header">
	      <div class="container-fluid">
	        <div class="row mb-2">
	          <div class="col-sm-6">
	            <h1 class="m-0 text-dark">Realizations</h1>
	          </div><!-- /.col -->
	          <div class="col-sm-6">
	            <ol class="breadcrumb float-sm-right">
	              <li class="breadcrumb-item">
	              	<a href="{{route('admin.portfolio.create')}}" class="btn btn-primary btn-sm font-weight-bold">
		              	<i class="fa fa-plus"></i> Ajouter
	              	</a>
	              </li>
	            </ol>
	          </div> 
	        </div> 
	      </div> 
	</div>
	<section class="content">
	  <div class="container-fluid">
	    <div class="row">
	      <div class="col-12">
	        <div class="card">
	          <div class="card-body">
	            <table id="example2" class="table table-bordered table-hover">
	              <thead>
		              <tr>
 		                <th>Name</th>
		                <th>Category</th>
		                <th>Client</th>
		                <th>country</th>
		                <th>date</th>
		                <th>Url</th>
		                <th>Actions</th>
		              </tr>
	              </thead>
	              <tbody>
	              	@forelse($realizations as $realization)
		              <tr>
 		                <td>{{$realization->title}}</td>
		                <td>{{$realization->category}}</td>
		                <td>{{$realization->client}}</td>
		                <td>{{$realization->country}}</td>
		                <td>{{$realization->date}}</td>
		                <td>
		                 @if(!empty($realization->url))
                			<a href="{{$realization->url}}" target="__blank">{{substr($realization->url,11,20)}}</a>
                		 @else
                		 <i>Undefined</i>
                		 @endif
		                </td>
		                <td>
		                	<div class="row">
		                		<div class="col-lg-3">
		                			<a href="{{route('admin.portfolio.show',$realization->id)}}"><i class="fa fa-eye"></i></a>
		                		</div>
		                	</div>		                	
		                </td>
		              </tr>
	               @empty
	               	<tr>
	               		<td colspan="7" class="text-center">
	               			<img src="{{asset('assets/img/no.jpeg')}}" alt="no image" width="120">
	               			<p>Aucune realization</p>
	               		</td>
	               	</tr>
	               @endforelse
	              </tbody>
	             </table>
	          </div>
	        </div>
	       </div>
	    </div>
	  </div>
	</section>
</div>
@stop