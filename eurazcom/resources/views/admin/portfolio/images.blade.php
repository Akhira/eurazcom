@extends('admin.layout.master',['title'=>$realization->title])

@section('master')
<div class="content-wrapper">
	<div class="content-header">
	      <div class="container-fluid">
	        <div class="row mb-2">
	          <div class="col-sm-6">
	            <h1 class="m-0 text-dark">Ajouter de images a la Realization</h1>
	          </div><!-- /.col -->
	          <div class="col-sm-6">
	            <ol class="breadcrumb float-sm-right">
	              <li class="breadcrumb-item">
	              	<a href="{{route('admin.portfolio.index')}}" class="btn btn-primary btn-sm font-weight-bold">
		              	<i class="fa fa-plus"></i> All Realizations
	              	</a>
	              </li>
	            </ol>
	          </div> 
	        </div> 
	      </div> 
	</div>
    <section class="content">
      <div class="container-fluid">
        <div class="row">
           <div class="col-md-10 offset-1">
             <div class="card card-primary">
               <form method="POST" action="{{route('admin.portfolio.images.store',$realization->id)}}"enctype="Multipart/form-data">
               		@csrf
	                <div class="card-body">
			            <input type="hidden" name="realization" value="{{$realization->id}}">
			             <div class="row">
			             	<div class="col-lg-12">
					         	<div class="form-group">
				                    <label for="image" class="form-label">Ajouter des images a la realisation</label>
				                    <input type='file' name="images[]" class="form-control" value="{{old('images[]')}}" multiple>
				                    @error('images')<small class="text-danger">{{$message}}</small>@enderror
				                </div>
				            </div>
				         </div>
			         </div>
			          
	                 <div class="card-footer text-center">
	                  <button type="submit" class="btn btn-primary font-weight-bold">Ajouter a la realisation</button>
	                </div>
              </form>
            </div>
           </div>
         </div>
       </div> 
    </section>
 </div>
@stop