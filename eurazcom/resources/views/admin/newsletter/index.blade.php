@extends('admin.layout.master',['title'=>'Admin Dashboad'])

@section('master')
<div class="content-wrapper">
	<div class="content-header">
	      <div class="container-fluid">
	        <div class="row mb-2">
	          <div class="col-sm-6">
	            <h1 class="m-0 text-dark">Newsletter</h1>
	          </div><!-- /.col -->
	          <div class="col-sm-6">
	            <ol class="breadcrumb float-sm-right">
	              <li class="breadcrumb-item font-weight-bold">
	              	{{$newsletters->count()}} Enregistrements
	              </li>

	              <li class="breadcrumb-item font-weight-bold">
	              	<a href="" class="btn btn-primary btn-sm font-weight-bold">Envoyer un Mail groupe</a>
	              </li>
	            </ol>
	          </div> 
	        </div> 
	      </div> 
	</div>
	<section class="content">
	  <div class="container-fluid">
	    <div class="row">
	      <div class="col-12">
	        <div class="card">
	          <div class="card-body">
	            <table id="example2" class="table table-bordered table-hover">
	              <thead>
		              <tr>
 		                <th>Email Address</th>
		                <th>Actions</th>
		              </tr>
	              </thead>
	              <tbody>
	              	@forelse($newsletters as $newsletter)
		              <tr>
 		                <td>{{$newsletter->email}}</td>
		                <td>
                 			<a href="">Send Mail</a>
		                </td>
		              </tr>
 
	               @empty
	               	<tr>
	               		<td colspan="2" class="text-center">
	               			<img src="{{asset('assets/img/no.jpeg')}}" alt="no image" width="120">
	               			<p>Aucune newsletter</p>
	               		</td>
	               	</tr>
	               @endforelse
	              </tbody>
	             </table>
	          </div>
	        </div>
	       </div>
	    </div>
	  </div>
	</section>
</div>
@stop