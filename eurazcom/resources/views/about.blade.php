@extends('users.layout.master',['title'=>'Agence de transformation digitale'])

@section('about-style')
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/about.css')}}">
@stop

@section('main')
	{{-- banner section --}}
   <section id="banner" class="banner mb-3">
	   	<div class="container position-relative">
	   		<h3 class="text-center py-5">Qui sommes-nous ?</h3>
	   		<p class="text-justify col-12 col-lg-7 mx-auto">Eurazcom est une agence de transformation digitale dotee d'une experience et d'une expertise reconnue dans le plus de 40 pays a travers le monde. Notre expertise couvre les domaines de la creation des sites et applications web professionnels, des sites de e-commerce, des marketplaces, l'elaboration des strategies de communication et de marketing, la conception et la diffusion publicitaire pour ne prendre que ceux la.</p>
	   	</div>
   </section>

   {{-- presentation section --}}
   <section id="presentation" class="presentation">
	   	<div class="container-fluid">
	   		<h6 class="fw-bold text-center text-uppercase text-muted mb-4 fs-6">
	   			Qui sommes-nous et pourquoi nous ?
	   		</h6>
	   		<h1 class="col-12 col-md-12 col-lg-6 col-xl-6  mb-3 mx-auto fw-bold text-center text-uppercase fs-5">Eurazcom est l'agence creee pour vous faciliter la vie grace au numerique et le digital 
	   		</h1>
	   		<div class="col-12 col-md-12 col-lg-6 text-center col-xl-8 mx-auto text-justify  text-break">
	   			Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
	   		</div>
	   	</div>
	   	<div class="bg-dark mt-3 p-3">
	   		<div class="container">
	   			<div class="row">
	   				<div class="col-xl-7">
	   					<h1 class="text-light fw-bold fs-4 pt-3 text-uppercase">
	   					Notre document de presentation</h1>
	   					<p class="mt-4 text-light fw-bold col-xl-9 pb-3" style="border-bottom: 5px solid #FFF;">
	   					Un document concu pour faciliter la comprehension de nos differents services et vous detailler de nos references en images et en terme de projets.
	   					</p>
	   				</div>
	   				<div class="col-xl-4 d-flex justify-content-center align-items-center">
	   					<a href="" class="btn btn-primary btn-sm text-uppercase p-2 fw-bold" style="font-size: .90rem;">
	   					telecharger notre document
	   					</a>
	   				</div>
	   			</div>
	   		</div>
		</div>
   </section>

   {{-- mission section --}}
   <section id="mission" class="mission">
	   	<div class="container">
	   		<div class="row">
	   			<div class="col-xl-6 mt-3">
	   				<h5 class="fw-bold fs-6 text-muted text-uppercase">
	   					Une agence, une expertise
	   				</h5>
	   				<h2 class="fw-bold">
	   					Une agence avec une expertise au service du digital
	   				</h2>
	   				<div class="accordion accordion-flush" id="accordionFlushExample">
					  <div class="accordion-item">
					    <h2 class="accordion-header" id="flush-headingOne">
					      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
					        Accordion Item #1
					      </button>
					    </h2>
					    <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
					      <div class="accordion-body">Placeholder content for this accordion, which is intended to demonstrate the <code>.accordion-flush</code> class. This is the first item's accordion body.</div>
					    </div>
					  </div>
					  <div class="accordion-item">
					    <h2 class="accordion-header" id="flush-headingOne">
					      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
					        Accordion Item #1
					      </button>
					    </h2>
					    <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
					      <div class="accordion-body">Placeholder content for this accordion, which is intended to demonstrate the <code>.accordion-flush</code> class. This is the first item's accordion body.</div>
					    </div>
					  </div>
					  <div class="accordion-item">
					    <h2 class="accordion-header" id="flush-headingTwo">
					      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
					        Accordion Item #2
					      </button>
					    </h2>
					    <div id="flush-collapseTwo" class="accordion-collapse collapse" aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushExample">
					      <div class="accordion-body">Placeholder content for this accordion, which is intended to demonstrate the <code>.accordion-flush</code> class. This is the second item's accordion body. Let's imagine this being filled with some actual content.</div>
					    </div>
					  </div>
					  <div class="accordion-item">
					    <h2 class="accordion-header" id="flush-headingThree">
					      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
					        Accordion Item #3
					      </button>
					    </h2>
					    <div id="flush-collapseThree" class="accordion-collapse collapse" aria-labelledby="flush-headingThree" data-bs-parent="#accordionFlushExample">
					      <div class="accordion-body">Placeholder content for this accordion, which is intended to demonstrate the <code>.accordion-flush</code> class. This is the third item's accordion body. Nothing more exciting happening here in terms of content, but just filling up the space to make it look, at least at first glance, a bit more representative of how this would look in a real-world application.</div>
					    </div>
					  </div>
					  <div class="accordion-item">
					    <h2 class="accordion-header" id="flush-headingThree">
					      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
					        Accordion Item #3
					      </button>
					    </h2>
					    <div id="flush-collapseThree" class="accordion-collapse collapse" aria-labelledby="flush-headingThree" data-bs-parent="#accordionFlushExample">
					      <div class="accordion-body">Placeholder content for this accordion, which is intended to demonstrate the <code>.accordion-flush</code> class. This is the third item's accordion body. Nothing more exciting happening here in terms of content, but just filling up the space to make it look, at least at first glance, a bit more representative of how this would look in a real-world application.</div>
					    </div>
					  </div>
					</div>
	   			</div>
	   			<div class="col-xl-6 d-flex justify-content-center align-items-center bg-danger text-end mission-img">
	   				<div class="text-start">
	   					<h1>pret a adopter le digital pour votre reussite ?</h1>
	   					<p class="fw-bold fs-5 ps-4 text-light">Nous sommes a votre disposition 24h/24 en cas de besoin
	   					<a href="" class="btn btn-warning fw-bold mt-2">Contactez-nous</a>
	   					</p>
	   				</div>
	    		</div>
	   		</div>
	   	</div>
   </section>

   {{-- mission section --}}
   <section id="mission" class="mission-scd">
	   	<div class="container">
	   		<div class="row">
	   		   <div class="col-xl-6 d-flex justify-content-center align-items-center bg-danger text-end mission-scd-img">
	   				<div class="text-start">
	   					<h1>pret a adopter le digital pour votre reussite ?</h1>
	   					<p class="fw-bold fs-5 ps-4 text-light">Nous sommes a votre disposition 24h/24 en cas de besoin
	   					<a href="" class="btn btn-warning fw-bold mt-2">Contactez-nous</a>
	   					</p>
	   				</div>
	    		</div>
	    		<div class="col-xl-6 mt-3">
	   				<h5 class="fw-bold fs-6 text-muted text-uppercase">
	   					Une agence, une expertise
	   				</h5>
	   				<h2 class="fw-bold">
	   					Une agence avec une expertise au service du digital
	   				</h2>
	   				<div class="accordion accordion-flush" id="accordionFlushExample">
					  <div class="accordion-item">
					    <h2 class="accordion-header" id="flush-headingOne">
					      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
					        Accordion Item #1
					      </button>
					    </h2>
					    <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
					      <div class="accordion-body">Placeholder content for this accordion, which is intended to demonstrate the <code>.accordion-flush</code> class. This is the first item's accordion body.</div>
					    </div>
					  </div>
					  <div class="accordion-item">
					    <h2 class="accordion-header" id="flush-headingOne">
					      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
					        Accordion Item #1
					      </button>
					    </h2>
					    <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
					      <div class="accordion-body">Placeholder content for this accordion, which is intended to demonstrate the <code>.accordion-flush</code> class. This is the first item's accordion body.</div>
					    </div>
					  </div>
					  <div class="accordion-item">
					    <h2 class="accordion-header" id="flush-headingTwo">
					      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
					        Accordion Item #2
					      </button>
					    </h2>
					    <div id="flush-collapseTwo" class="accordion-collapse collapse" aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushExample">
					      <div class="accordion-body">Placeholder content for this accordion, which is intended to demonstrate the <code>.accordion-flush</code> class. This is the second item's accordion body. Let's imagine this being filled with some actual content.</div>
					    </div>
					  </div>
					  <div class="accordion-item">
					    <h2 class="accordion-header" id="flush-headingThree">
					      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
					        Accordion Item #3
					      </button>
					    </h2>
					    <div id="flush-collapseThree" class="accordion-collapse collapse" aria-labelledby="flush-headingThree" data-bs-parent="#accordionFlushExample">
					      <div class="accordion-body">Placeholder content for this accordion, which is intended to demonstrate the <code>.accordion-flush</code> class. This is the third item's accordion body. Nothing more exciting happening here in terms of content, but just filling up the space to make it look, at least at first glance, a bit more representative of how this would look in a real-world application.</div>
					    </div>
					  </div>
					  <div class="accordion-item">
					    <h2 class="accordion-header" id="flush-headingThree">
					      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
					        Accordion Item #3
					      </button>
					    </h2>
					    <div id="flush-collapseThree" class="accordion-collapse collapse" aria-labelledby="flush-headingThree" data-bs-parent="#accordionFlushExample">
					      <div class="accordion-body">Placeholder content for this accordion, which is intended to demonstrate the <code>.accordion-flush</code> class. This is the third item's accordion body. Nothing more exciting happening here in terms of content, but just filling up the space to make it look, at least at first glance, a bit more representative of how this would look in a real-world application.</div>
					    </div>
					  </div>
					</div>
	   			</div>
	   		</div>
	   	</div>
   </section>

   {{-- ask-quotation section --}}
   <section id="ask-quotation" class="ask-quotation">
	   	<div class="bg-dark mt-0 p-3">
			<div class="container">
				<div class="row">
					<div class="co-12 col-md-10 col-lg-7 col-xl-7">
						<h1 class="text-light fw-bold fs-4 pt-3 text-uppercase">
						Avez-vous un projet?</h1>
						<p class="mt-4 text-light fw-bold col-xl-9 pb-3" style="border-bottom: 5px solid #FFF;">
						Un document concu pour faciliter la comprehension de nos differents services et vous detailler de nos references en images et en terme de projets.
						</p>
					</div>
					<div class="col-xl-4 d-flex justify-content-center align-items-center">
						<a href="" class="btn btn-primary btn-sm text-uppercase p-2 fw-bold" style="font-size: .90rem;">
						telecharger notre document
						</a>
					</div>
				</div>
			</div>
		</div>
   </section>

   {{-- value section --}}
   <section id="values" class="values">
	   	<div class="container">
	   		<h4 class="fw-bold fs-4 col-12 col-md-8 col-lg-6 text-center col-xl-6 mx-auto mb-3">
	   			Les valeurs qui sont les notres
	   		</h4>
	   		<div class="col-12 col-md-11 col-lg-10 col-xl-7 mx-auto mt-2">
	   			Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
	   		</div>
	   		<div class="co-12 col-nd-12 col-lg-10 mx-auto col-xl-12 mt-5">
	   			<div class="row">
		   			<div class="col-0 col-xl-6">
		   				<div class="values-image-first">
		   				</div>
		   				<div class="container  mt-2">
		   					<div class="row">
			   					<div class="col-xl-6" id="values-image-scd"></div>
			   					<div class="col-xl-6" id="values-image-thrt"></div>
		   					</div>
		   				</div>
		   			</div>
		   			<div class="col-12 col-xl-6">
		   				 <div class="containers">
		   				 	<div class="row values-box">
		   				 		<div class="col-12 col-xl-12">
		   				 			<div class="row">
		   				 				<div class="col-12 col-xl-3">
		   				 					<div class="percent fw-bold fs-4"> 100 %</div>
		   				 				</div>	
		   				 				<div class="col-12 col-xl-9">
		   				 					<h4 class="fw-bold">Integrite</h4>
		   				 					<div>
		   				 						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
												tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
		   				 					</div>
		   				 				</div>	
		   				 			</div>
		   				 		</div>
		   				 	</div>
		   				 </div>
		   			</div>
		   		</div>

	   		</div>
	   	</div>
   </section>

   {{-- why us section --}}
   <section id="why-us" class="why-us">
	   	<div class="container">
	   		
	   		<div class="row">
	   			<div class="col-xl-7">
	   				<div class="why-us-img-top">
	   					<div class="why-us-img-middle"></div>
	   				</div>
	   				
	   				<div class="why-us-img-bottom"></div>
	   			</div>
	   			<div class="col-xl-5">
	   				<h5 class="fs-6 fw-bold text-muted text-uppercase">Pourquoi nous choisir ?</h5>
			   		<h2 class="fw-bold fs-3 mb-5 mt-3">Travaillez .....</h2>
			   		<div class="why-us-boxes">
			   			<div class="why-us-box mb-5">
			   				<div class="row">
			   					<div class="col-xl-1 box-icon badge bg-primary">
			   						<i class="fa fa-check"></i>
			   					</div>
			   					<div class=" col-xl-10 box-title">
			   						<h5>Nos Propositions</h5>
			   					</div>
			   				</div>
			   				<div class="box-text pt-2">
			   					shdjshdjs dshdjhsjdhsds dshdjshdjs
			   				</div>
			   			</div>
			   			<div class="why-us-box mb-5">
			   				<div class="row">
			   					<div class="col-xl-1 box-icon badge bg-primary">
			   						<i class="fa fa-check"></i>
			   					</div>
			   					<div class=" col-xl-10 box-title">
			   						<h5>Une offre unique</h5>
			   					</div>
			   				</div>
			   				<div class="box-text pt-2">
			   					shdjshdjs dshdjhsjdhsds dshdjshdjs
			   				</div>
			   			</div>
			   			<div class="why-us-box mb-3">
			   				<div class="row">
			   					<div class="col-xl-1 box-icon badge bg-primary">
			   						<i class="fa fa-check"></i>
			   					</div>
			   					<div class=" col-xl-10 box-title">
			   						<h5>Ce dont nous vous proposons</h5>
			   					</div>
			   				</div>
			   				<div class="box-text pt-2">
			   					shdjshdjs dshdjhsjdhsds dshdjshdjs
			   				</div>
			   			</div>
			   		</div>
	   			</div>
	   		</div>
	   	</div>
   </section>

   {{-- project asked section --}}
   <section  id="project" class="project">
   		<div class="container">
   			<h1 class="text-center fw-bold fs-2 col-xl-8 offset-2">
   				Vous avez un projet ? Alors confiez le a la meilleur agence de transformation digitale
   			</h1>
   			<h6 class="text-center mt-4 text-muted fw-bold fs-6">
   				Pour une assistance rapide , contactez l'un de ces numeros
   			</h6>
   			<div class="fw-bold fs-3 text-center mt-3">
   				+237 699939633/+237 680638086
   			</div>
   			<div class="text-center mt-4">
   				<a href="{{route('eurazcom.contact.create')}}" class="btn btn-primary fw-bold">Nous contacter</a>
   			</div>
   		</div>
   </section>

   {{-- crew members section --}}
   <section id="crew" class="crew">
	   	<div class="container">
		   	<h5 class="text-muted fw-bold text-uppercase fs-6">Notre equipe</h5>
		   	<h3 class="fw-bold text-capitalize fs-3">nos collaborateurs mis a votre disposition</h3>
		   	<div class="row">
		   		<div class="col-xl-10 text-justify">
		   			<div class="col-xl-10 fw-bold text-muted">
		   				dshdksjdkjsd sdjskjdksjdks djskdjksjdksd sjdksjdksd sjdkjskjds djskdjksjds djsdjkjskdjs dskdkjsjds dskdjksjd sjdskjdksd sdjskjdksjds djskjdksd sdjskjdksd skdjsk
		   			</div>
		   		</div>
		   		<div class="col-xl-2 text-end">
		   			<a href="" class="btn btn-outline-primary btn-lg fw-bold fw-bold text-uppercase  " style="font-size: .8rem;">
		   				Nous Rejoindre
		   			</a>
		   		</div>
		   	</div>
		   	<div class="row mt-4">
		   		<div class="col-xl-3">
		   			<div class="card border-0">
		   				<div class="card-body text-center">
		   					<img src="{{asset('assets/img/pers.jpeg')}}">
		   				</div>
		   				<div class="card-foter">
		   					<div class="text-center text-uppercase fw-bold" style="margin-top: -10px;">Emma Nya</div>
		   					<div class="text-muted fw-bold text-center" style="font-size: .85rem;">Ingenieure en software ingenieuring
		   					</div>
		   					<div class="" style="font-size: .9rem;">
		   						sjajkjs dshdjshd shdjshds dshdjshd sjhdjshds dhsjdhjsdh
		   					</div>
		   				</div>
		   			</div>
		   		</div>
		   		<div class="col-xl-3">
		   			<div class="card border-0">
		   				<div class="card-body text-center">
		   					<img src="{{asset('assets/img/pers.jpeg')}}" class="img-fluid">
		   				</div>
		   				<div class="card-foter">
		   					<div class="text-center text-uppercase fw-bold" style="margin-top: -10px;">Emma Nya</div>
		   					<div class="text-muted fw-bold text-center" style="font-size: .85rem;">Ingenieure en software ingenieuring
		   					</div>
		   					<div class="" style="font-size: .9rem;">
		   						sjajkjs dshdjshd shdjshds dshdjshd sjhdjshds dhsjdhjsdh
		   					</div>
		   				</div>
		   			</div>
		   		</div>
		   		<div class="col-xl-3">
		   			<div class="card border-0">
		   				<div class="card-body text-center">
		   					<img src="{{asset('assets/img/pers.jpeg')}}">
		   				</div>
		   				<div class="card-foter">
		   					<div class="text-center text-uppercase fw-bold" style="margin-top: -10px;">Emma Nya</div>
		   					<div class="text-muted fw-bold text-center" style="font-size: .85rem;">Ingenieure en software ingenieuring
		   					</div>
		   					<div class="" style="font-size: .9rem;">
		   						sjajkjs dshdjshd shdjshds dshdjshd sjhdjshds dhsjdhjsdh
		   					</div>
		   				</div>
		   			</div>
		   		</div>
		   		<div class="col-xl-3">
		   			<div class="card border-0">
		   				<div class="card-body text-center">
		   					<img src="{{asset('assets/img/pers.jpeg')}}">
		   				</div>
		   				<div class="card-foter">
		   					<div class="text-center text-uppercase fw-bold" style="margin-top: -10px;">Emma Nya</div>
		   					<div class="text-muted fw-bold text-center" style="font-size: .85rem;">Ingenieure en software ingenieuring
		   					</div>
		   					<div class="" style="font-size: .9rem;">
		   						sjajkjs dshdjshd shdjshds dshdjshd sjhdjshds dhsjdhjsdh
		   					</div>
		   				</div>
		   			</div>
		   		</div>
		   		 
		   	</div>
		</div>
   </section>

   {{-- realizations section --}}
   @include('partials.portfolio')

   {{-- stats section --}}
   <section id="stats" class="stats">
	   	<div class="container">
	   		<div class="row">
		   		<div class="col-xl-6">
		   			<div class="container"> 
			   			<div class="row">
			   				<div class="card col-xl-5 mb-5 shadow stats-box">
			   					<div class="card">
			   						<div class="card-body">
			   							<h1>200+</h1>
			   						</div>
			   						<div class="card-footer">
			   							<div class="col-xl-9 offset-2 fw-bold">
			   								Projets realises pour nos clients
			   							</div>
			   						</div>
			   					</div>
			   				</div>
			   				<div class="card col-xl-5 mb-5 shadow stats-box">
			   					<div class="card">
			   						<div class="card-body">
			   							<h1>99%</h1>
			   						</div>
			   						<div class="card-footer">
			   							<div class="col-xl-12 offset-2 fw-bold">
			   								De taux de statisfaction clientelle
			   							</div>
			   						</div>
			   					</div>
			   				</div>
			   			</div>
			   			<div class="row">
			   				<div class="card col-xl-5 mb-5 shadow stats-box">
			   					<div class="card">
			   						<div class="card-body">
			   							<h1>150+</h1>
			   						</div>
			   						<div class="card-footer">
			   							<div class="col-xl-12 offset-2 fw-bold">
			   								Clients de par le monde <br>(Afrique, Europe, ...)
			   							</div>
			   						</div>
			   					</div>
			   				</div>
			   				<div class="card col-xl-5 mb-5 shadow stats-box">
			   					<div class="card">
			   						<div class="card-body">
			   							<h1>15+</h1>
			   						</div>
			   						<div class="card-footer">
			   							<div class="col-xl-12 offset-2 fw-bold">
			   								Ingenieurs surmotives pour l'atteinte de vos objectifs
			   							</div>
			   						</div>
			   					</div>
			   				</div>
			   			</div>
		 	   		</div>
		   		</div>
		   		<div class="col-xl-6">
		   			<div class="card">	
		   				<div class="card-body">
		   					<img src="{{asset('assets/img/stats.png')}}" class="img-fluid" style="width: 100%; margin-top: -50px;">
		   				</div>
		   			</div>
		   		</div>
		   	</div>
	   	</div>
   </section>

   {{-- testimonials section --}}
   <section id="testimonial" class="testimonial">
	   	<div class="container">
	   		<h2 class="text-center fs-6 text-muted fw-bold text-uppercase">Ce qu'ils pensent de nous </h2>
	   		<div class="testimonial-slider swiper">
	   			<div class="swiper-wrapper">
	   				<div class="swiper-slide">
			            <div class="testimonial-wrap">
			              <div class="testimonial-item">
			              	<img src="{{asset('assets/img/testimonials/testimonials-1.jpg')}}" class="testimonial-img" alt="">
			              	<h3>Saul Goodman</h3>
			                <h4>Ceo &amp; Founder</h4>
			                <p>
			                  <i class="bx bxs-quote-alt-left quote-icon-left"></i>
			                  Proin iaculis purus consequat sem cure digni ssim donec porttitora entum suscipit rhoncus. Accusantium quam, ultricies eget id, aliquam eget nibh et. Maecen aliquam, risus at semper.
			                  <i class="bx bxs-quote-alt-right quote-icon-right"></i>
			                </p>
			              </div>
				        </div>
			        </div>
			        <div class="swiper-slide">
			            <div class="testimonial-wrap">
			              <div class="testimonial-item">
			              	<img src="{{asset('assets/img/testimonials/testimonials-1.jpg')}}" class="testimonial-img" alt="">
			              	<h3>Saul Goodman</h3>
			                <h4>Ceo &amp; Founder</h4>
			                <p>
			                  <i class="bx bxs-quote-alt-left quote-icon-left"></i>
			                  Proin iaculis purus consequat sem cure digni ssim donec porttitora entum suscipit rhoncus. Accusantium quam, ultricies eget id, aliquam eget nibh et. Maecen aliquam, risus at semper.
			                  <i class="bx bxs-quote-alt-right quote-icon-right"></i>
			                </p>
			              </div>
				        </div>
			        </div>
			        <div class="swiper-slide">
			            <div class="testimonial-wrap">
			              <div class="testimonial-item">
			              	<img src="{{asset('assets/img/testimonials/testimonials-1.jpg')}}" class="testimonial-img" alt="">
			              	<h3>Saul Goodman</h3>
			                <h4>Ceo &amp; Founder</h4>
			                <p>
			                  <i class="bx bxs-quote-alt-left quote-icon-left"></i>
			                  Proin iaculis purus consequat sem cure digni ssim donec porttitora entum suscipit rhoncus. Accusantium quam, ultricies eget id, aliquam eget nibh et. Maecen aliquam, risus at semper.
			                  <i class="bx bxs-quote-alt-right quote-icon-right"></i>
			                </p>
			              </div>
				        </div>
			        </div>
			        <div class="swiper-slide">
			            <div class="testimonial-wrap">
			              <div class="testimonial-item">
			              	<img src="{{asset('assets/img/testimonials/testimonials-1.jpg')}}" class="testimonial-img" alt="">
			              	<h3>Saul Goodman</h3>
			                <h4>Ceo &amp; Founder</h4>
			                <p>
			                  <i class="bx bxs-quote-alt-left quote-icon-left"></i>
			                  Proin iaculis purus consequat sem cure digni ssim donec porttitora entum suscipit rhoncus. Accusantium quam, ultricies eget id, aliquam eget nibh et. Maecen aliquam, risus at semper.
			                  <i class="bx bxs-quote-alt-right quote-icon-right"></i>
			                </p>
			              </div>
				        </div>
			        </div>
	   			</div>
	   			<div class="swiper-pagination"></div>
	   		</div>
	   	</div>
   </section>
@stop