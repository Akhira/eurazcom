<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\users\solutionController as Solutions;
use App\Http\Controllers\users\realizationController as Realizations;
use App\Http\Controllers\users\eurazcomController as Eurazcom;
use App\Http\Controllers\users\contactController as Contacts;
use App\Http\Controllers\users\AppointmentController as Appointments;
use App\Http\Controllers\users\AcademyController as Academy;
use App\Http\Controllers\users\ProjectController as Projects;
use App\Http\Controllers\users\PartnerController as Partner;
use App\Http\Controllers\users\ArticleController as Articles;
use App\Http\Controllers\users\ServiceController as Services;
use App\Http\Controllers\users\ProfilController;

// admin controller
use App\Http\Controllers\admins\AdminController;
use App\Http\Controllers\admins\PartnerController;
use App\Http\Controllers\admins\AppointmentController;
use App\Http\Controllers\admins\PublicationsController;
use App\Http\Controllers\admins\CollaboratorController;
use App\Http\Controllers\admins\ProjectController;
use App\Http\Controllers\admins\RealizationsController;
  
// home
Route::get('/',[Eurazcom::class,'welcome'])->name('welcome');
Route::prefix('eurazcom')->name('eurazcom.')->group(function(){

		// services
		Route::get('/agence conception site et application web',[Services::class,'webSiteCreation'])->name('service-1');
		Route::get('/conception de logiciel specialises',[Services::class,'software'])->name('service-2');
		Route::get('/ERP integration',[Services::class,'erpIntegration'])->name('service-3');
		Route::get('/agence graphique et communication visuelle',[Services::class,'brandinDesign'])->name('service-4');
		Route::get('/agence publicitaire',[Services::class,'adverts'])->name('service-5');
		Route::get('/agence de communication et marketing',[Services::class,'marketingAndCom'])->name('service-6');
		Route::get('/consulting numerique',[Services::class,'consulting'])->name('service-7');
		Route::get('/formations',[Services::class,'trainings'])->name('service-8');
			
		// Newsletter Route
		Route::get('about',[Eurazcom::class,'about'])->name('about');
		Route::post('mewsletter',[Eurazcom::class,'newsletter'])->name('newsletter');

		// testimonials
		Route::get('testimonials',[Eurazcom::class,'testimonials'])->name('testimonials');
		Route::post('testimonials',[Eurazcom::class,'doneTestimonials'])->name('testimonial.store');

		// realization Routes
		Route::get('portfolio/all/',[Realizations::class,'getAllRealizations'])->name('portfolio');
		Route::get('portfolio/{id}/details/',[Realizations::class,'getRealizationById'])->name('portfolio.show');
		
		// partner Route
		Route::get('/partner-program',[Partner::class,'welcome'])->name('partner.home');
		Route::get('/partner/became',[Partner::class,'create'])->name('partner.create');
		Route::post('/partner/became',[Partner::class,'store'])->name('partner.store');
		Route::get('/partner/authentication',[Partner::class,'connexion'])->name('partner.connexion');
		Route::post('/partner/authentication',[Partner::class,'partnerAuthentication'])->name('partner.authentication');
		Route::get('/partner/project',[Partner::class,'project'])->name('partner.project');
		Route::post('/partner/project',[Partner::class,'storeProject'])->name('partner.project-store');
		Route::get('/partner/forgot-code',[Partner::class,'forgotCode'])->name('partner.forgotCode');
		Route::post('/partner/forgot-code',[Partner::class,'forgotCodeStore'])->name('partner.forgotCodeStore');
 
		// academy Routes
		Route::get("/academy-center",[Academy::class,'academyHome'])->name('academy');

		// project Route
		Route::get("/project",[Projects::class,'home'])->name('project.home');
		Route::get("/project/create",[Projects::class,'create'])->name('project.create');
		Route::post("/project/store",[Projects::class,'store'])->name('project.store');

		// Appointments Routes
		Route::get("appointments",[Appointments::class,'create'])->name('appointment.create');
		Route::post("appointments",[Appointments::class,'store'])->name('appointment.store');

		// Contact Routes
		Route::get("contacts",[Contacts::class,'create'])->name('contact.create');
		Route::post("contacts",[Contacts::class,'sendEmail'])->name('contact.send');

		// blog Route
		Route::get('/publications',[Articles::class,'getAllPublications'])->name('publications');
		Route::get('/publication/{publicationId}/show/{slug}/',[Articles::class,'getPublicationById'])->name('publication.show');
		Route::post('/comment/{publicationId}/doComment',[Articles::class,'commentPublication'])->name('publication.comment');
		Route::get('/publication/comment/{commentId}/edit',[Articles::class,'editComment'])->name('publication.comment.edit');
		Route::put('/comment/{commentId}/update',[Articles::class,'updateComment'])->name('publication.comment.update');
		Route::patch('/comment/{publicationId}/like',[Articles::class,'likePublication'])->name('publication.like');
		Route::post('/publications/results',[Articles::class,'searchPublications'])->name('publication.search');
		Route::get('/publications/filter',[Articles::class,'filterPublications'])->name('publications.filter');
		Route::get('/publications/categorie',[Articles::class,'categoriesFilter'])->name('publications.categoriesFilter');
		// Route::get('/publications/publish',[Articles::class,'publish'])->name('publication.publish');

		// solutions Routes
		Route::get('solutions',[Solutions::class,'getAllSolutions'])->name('solutions');
		Route::get('solutions/{id}/details',[Solutions::class,'getSolutionById'])->name('solution.show');

		// Route for only user auth
		Route::middleware(['auth:web'])->group(function(){
		Route::get('/{name}',[ProfilController::class,'welcome'])->name('profil');
		Route::get('/about/me',[ProfilController::class,'about'])->name('profil.about');
		Route::get('/profil/edit',[ProfilController::class,'edit'])->name('profil.edit');
		Route::patch('/profil/update/user',[ProfilController::class,'updateUserInfos'])->name('profil.updateUserInfos');
		Route::patch('/profil/update/avatar',[ProfilController::class,'updateUserAvatar'])->name('profil.updateUserAvatar');
 		Route::get('/profil/edit/status',[ProfilController::class,'status'])->name('profil.status');
 		Route::patch('/profil/status/updateStatus',[ProfilController::class,'updateStatus'])->name('profil.updateStatus');
 		Route::get('/profil/close/account',[ProfilController::class,'closeAccount'])->name('profil.closeAccount');
 		Route::patch('/profil/close/account',[ProfilController::class,'storeCloseAccount'])->name('profil.storeCloseAccount');
 		Route::get('/profil/password/change',[ProfilController::class,'changerPassword'])->name('profil.password');
 		Route::patch('/profil/password/change',[ProfilController::class,'updatePassword'])->name('profil.updatePassword');

		Route::get('/quotations/all',[Projects::class,'getAllProjects'])->name('profil.quotations');
 		Route::get('/quotations/current',[Projects::class,'currentProjects'])->name('quotations.current');
		Route::get('/quotations/traitement',[Projects::class,'traitement'])->name('quotations.treat');
		Route::get('/quotations/follow',[Projects::class,'follow'])->name('quotations.follow');
		Route::get('/quotations/waiting-validation',[Projects::class,'waitForValidation'])->name('quotations.waitValidation');
		Route::get("/project/{code}/view",[Projects::class,'getProjectByCode'])->name('quotation.show');

 		Route::get('/projects/all',[Projects::class,'getAllProjects'])->name('profil.projects');
		Route::get('/projects/current',[Projects::class,'currentProjects'])->name('projects.current');
		Route::get('/projects/traitement',[Projects::class,'traitement'])->name('projects.treat');
		Route::get('/projects/follow',[Projects::class,'follow'])->name('projects.follow');
 		Route::patch("/project/{code}/mark",[Projects::class,'markProject'])->name('project.mark');
 		Route::get("/project/{code}/view",[Projects::class,'getProjectByCode'])->name('project.show');
 		Route::patch("/project/{code}/approve",[Projects::class,'transform'])->name('project.approve');
	 	});

});

/*******************************************/
/*        	ADMIN ROUTES ONLY	 
/******************************************/ 
Route::prefix('admin')->name('admin.')->group(function(){

	// Route::middleware(['guest:admin'])->group(function(){
	// 	Route::get('login',[AdminController::class,'login'])->name('login');

	// });

	Route::middleware(['guest:admin'])->group(function(){
 		Route::get('welcome',[AdminController::class,'welcome'])->name('welcome');

 		// Newsletter Routes
 		Route::get('newsletter',[AdminController::class,'newsletter'])->name('newsletter');
 		// clients Routes
 		Route::get('clients',[AdminController::class,'getAllClients'])->name('clients');
 		Route::get('client/{id}/details',[AdminController::class,'getAllClientById'])->name('client.show');

 		// Admins Routes
 		Route::get('collaborators',[CollaboratorController::class,'getAllCollaborators'])->name('collaborators');
 		Route::get('collaborator/create',[CollaboratorController::class,'create'])->name('collaborator.create');
 		Route::post('collaborator/post',[CollaboratorController::class,'store'])->name('collaborator.store');
 		Route::put('collaborator/{id}/block',[CollaboratorController::class,'block'])->name('collaborator.block');

 		// Publications
 		Route::get('publications',[PublicationsController::class,'index'])->name('publications');
 		Route::get('publication/{id}/{slug}/view',[PublicationsController::class,'show'])->name('publication.show');
 		Route::get('publication/create',[PublicationsController::class,'create'])->name('publication.create');
 		Route::post('publication/store',[PublicationsController::class,'store'])->name('publication.store');

 		// contact
 		Route::get('contacts',[AdminController::class,'contacts'])->name('contacts');
 		Route::delete('contact/{id}/details',[AdminController::class,'destroy'])->name('contact.destroy');
 		Route::delete('contacts',[AdminController::class,'destroyAll'])->name('contacts.destroyAll');

 		// Appointment Route
 		Route::get('appointments',[AppointmentController::class,'index'])->name('appointments');
 		Route::get('appointments/former',[AppointmentController::class,'index'])->name('appointments.former');
 		Route::get('appointment/{id}/details',[AppointmentController::class,'show'])->name('appointments.show_source()');

 		// Partner Route
 		Route::get('partners',[PartnerController::class,'index'])->name('partners');
 		Route::get('partners/{code}/details',[PartnerController::class,'show'])->name('partners.show');
 		Route::post('partners/search',[PartnerController::class,'ResultSearch'])->name('partners.search');
 		Route::patch('partner/{code}/block',[PartnerController::class,'block'])->name('partner.block');
 		Route::patch('partner/{code}/accepted',[PartnerController::class,'agree'])->name('partner.accepted');

 		// project Routes
 		Route::get('quotations',[ProjectController::class,'getAllQuotations'])->name('quotations');
 		Route::get('projects/Waiting-for-validation',[ProjectController::class,'getAllQuotations'])->name('quotations.traited');
 		Route::get('project/{id}/view',[ProjectController::class,'show'])->name('project.show');
 		Route::get('project/{code}/edit',[ProjectController::class,'edit'])->name('project.edit');
 		Route::patch('project/{code}/update',[ProjectController::class,'update'])->name('project.update');
 		Route::patch('project/{code}/startraitment',[ProjectController::class,'starTraitment'])->name('project.startraitment');

 		Route::get('projects',[ProjectController::class,'getAllProjects'])->name('projects');
 		Route::get('projects/waiting',[ProjectController::class,'index'])->name('projects.waiting');
 		Route::get('projects/deliver',[ProjectController::class,'index'])->name('projects.deliver');

 		// solution 
 		Route::get('/solutions',[Solutions::class,'getAllSolutions'])->name('solutions.index');
 		Route::get('/solutions/create',[Solutions::class,'create'])->name('solutions.create');
 		Route::post('/solutions/create',[Solutions::class,'store'])->name('solutions.store');
 		Route::get('/solutions/{id}/show',[Solutions::class,'getSolutionById'])->name('solutions.show');
 		Route::get('/solutions/{id}/edit',[Solutions::class,'edit'])->name('solutions.edit');
 		Route::put('/solutions/{id}/update',[Solutions::class,'update'])->name('solutions.update');
 		Route::get('/solutions/{id}/images',[Solutions::class,'imagesForm'])->name('solutions.images');
 		Route::post('/solutions/{id}/images',[Solutions::class,'addImages'])->name('solutions.images.store');
 		Route::delete('/solutions/{id}/delete',[Solutions::class,'destroy'])->name('solutions.delete');

 		// portfolio 
 		Route::get('/portfolio',[RealizationsController::class,'getAllRealizations'])->name('portfolio.index');
 		Route::get('/portfolio/create',[RealizationsController::class,'create'])->name('portfolio.create');
 		Route::post('/portfolio/store',[RealizationsController::class,'store'])->name('portfolio.store');
 		Route::get('/portfolio/{id}/show',[RealizationsController::class,'getRealizationById'])->name('portfolio.show');
 		Route::get('/portfolio/{id}/edit',[RealizationsController::class,'edit'])->name('portfolio.edit');
 		Route::put('/portfolio/{id}/update',[RealizationsController::class,'update'])->name('portfolio.update');
 		Route::get('/portfolio/{id}/images',[RealizationsController::class,'imagesForm'])->name('portfolio.images');
 		Route::post('/portfolio/{id}/images',[RealizationsController::class,'storeImages'])->name('portfolio.images.store');
 		Route::delete('/portfolio/{id}/delete',[RealizationsController::class,'destroy'])->name('portfolio.delete');
	});

});

 

Auth::routes();
Route::get('/verify', [App\Http\Controllers\Auth\RegisterController::class, 'verifyUser'])->name('verify');
Route::get('/confirmation', [App\Http\Controllers\Auth\RegisterController::class, 'confirmation'])->name('confirmation');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
