// organisation 
$(document).ready(function(){
    $('#company, #association, #organization').click(function(){
      var val = $(this).attr("id");
      var target = $('.' + val);
      $(".form-organization").not(target).hide();
       $(target).show();
    });
});

$(document).ready(function(){
    $('#particular').click(function(){
      $(".form-organization").hide();
    });
});


// budget yes
$(document).ready(function(){
    $('#budget_yes').click(function(){
      var val = $(this).attr("id");
      var target = $('.' + val);
      $(".budget-yes-form").not(target).hide();
       $(target).show();
    });
});

$(document).ready(function(){
    $('#budget_no').click(function(){
      $(".budget-yes-form").hide();
    });
});

// budget no
$(document).ready(function(){
    $('#budget_no').click(function(){
      var val = $(this).attr("id");
      var target = $('.' + val);
      $(".budget-no-form").not(target).hide();
       $(target).show();
    });
});

$(document).ready(function(){
    $('#budget_yes').click(function(){
      $(".budget-no-form").hide();
    });
});