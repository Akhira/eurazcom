<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->increments('id');
            $table->string('matricule')->unique();
            $table->string('name');
            $table->string('lastname');
            $table->string('nationality');
            $table->string('city');
            $table->string('address');
            $table->string('phone')->unique();
            $table->string('email')->unique();
            $table->string('skills');
            $table->string('degree');
            $table->string('establishment');
            $table->string('domaine');
            $table->string('avatar')->default("default.jpg");
            $table->boolean('block')->default(false);
            $table->string('verification_code');
            $table->boolean('is_verified')->default(false);
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins');
    }
}
