<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('lastname');
            $table->string('phone')->unique();
            $table->string('email')->unique();
            $table->string('country');
            $table->string('address')->nullable();
            $table->string('city');
            $table->enum('status',['Particular','Company','Organization','Association']);
            $table->string('organization_name')->nullable();
            $table->string('users_role')->nullable();
            $table->string('organization_email')->nullable();
            $table->string('organization_sector')->nullable();
            $table->string('organization_address')->nullable();
            $table->string('organization_phone')->nullable();
            $table->string('organization_logo')->nullable();
            $table->string('avatar')->default("default.jpg");
            $table->string('verification_code');
            $table->string('reason')->nullable();
            $table->boolean('close')->default(false);
            $table->boolean('is_verified')->default(false);
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }


}
