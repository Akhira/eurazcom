<?php 
use App\Models\Article;
use App\Models\Categorie;
use App\Models\Document;
use App\Models\Realization;
use App\Models\Testimonial;


// portfolio
if (!function_exists('testimonials')) {
	function testimonials(){
		return Testimonial::all();
	}
}

// portfolio
if (!function_exists('realizations')) {
	function realizations(){
		return Realization::all();
	}
}

// web site documents
if (!function_exists('documents')) {
	function documents(){
		return Document::get();
	}
}
// modify date format
if (!function_exists('carbone_date')) {
	function carbone_date(string $date){
		return Carbon\Carbon::parse($date)->diffForHumans();
	}
}

// count articles
if (!function_exists('categories')) {
	function categories(){
		$categories = Categorie::orderBy('name',"ASC")->get();
		return $categories;  
	}
}

// count articles
if (!function_exists('articles')) {
	function articles(){
		$articles = Article::where('publish',1)->count();
		return $articles;  
	}
}
//filter system
if (!function_exists('filter')) {
	function filter(string $str){
		return request()->filter == $str ? 'selected':'';  
	}
}

// trucate words
if (!function_exists('truncatewords')) {
	function truncatewords(string $str, int $value){
		$trucate = strlen($str) > $value ? substr($str, 0,$value).'...' : $str;
		return $trucate;
	}
}
// control size of text
if (!function_exists('stringSize')) {
	function stringSize(string $text,string $text2="", int $lenght){
		return  strlen($text.' '.$text2)>$lenght ? strlen($text)>strlen($text2) ? $text2 : $text :$text.' '.$text2;
	}
}
// verify type of project
if (!function_exists('verifyType')) {
	function verifyType(string $type){
		return $type === "APPDEV" ? "SFT" : $type === "BLD" ? "BLD" : $type === "MCD" ? "MCD" : "CDP";
	}
}
// genrate matricule use
if (!function_exists('matricule')) {
	function matricule(){
     	$codeGenrated =substr(date('Y'), 2,3).''.generatorCode(5);
     	return substr_replace($codeGenrated,"E",2,0);
	}
}

if (!function_exists('generatorCode')) {
	function generatorCode(string $lenght){
		$string = "";
        // $key_such = "0A0B1C2D3E4F5G6H7I8J9K0L1M2N3O4P5Q6R7S8T9U0V1W2X3Y4Z5";
        $key_such = "0123456789";
        srand((double)microtime()*1000000);
        for ($i=0; $i <$lenght ; $i++) { 
             $string .=$key_such[rand()%strlen($key_such)];
        }
        return $string;
	}
}
// get MAC address of user
if (!function_exists('userMacAddress')) {
	function userMacAddress()
	{
		return !empty($_SERVER['HTTP_CLIENT_IP']) 
	  						? $_SERVER['HTTP_CLIENT_IP'] 
	  						: !empty($_SERVER['HTTP_X_FORWARDED_FOR']) 
	  						? $_SERVER['HTTP_X_FORWARDED_FOR']
	  						: $_SERVER['REMOTE_ADDR'];
	}
}
// return user auth
if (!function_exists('userAuth')) {
	function userAuth()
	{
		return Auth::user();
	}
}

// verify if user auth
if (!function_exists('ifUserAuth')) {
	function ifUserAuth(){
        if (Auth::user()) {
            return true;
        }
        return false;
	}
}

// geolocation user country
if (!function_exists("getUserCountry")) {
	function getUserCountry(){

		foreach (allCountries() as $key => $country) {
			 if ($key === geolocate()->iso_code) {
			 	 return $country;
			 }
		}

	}
}

// actived current route
if (!function_exists('activeRoute')) {
	function activeRoute(string $route){
		return Route::is($route) ? 'active' : '';
	}
}

// test multiples routes
// if (!function_exists('multiplesRoute')) {
// 	function multiplesRoute(array $routes = [], string $currentRoute){
// 		if (in_array($currentRoute, $routes)) {
// 			return 'active;
// 		}
// 	}
// }
 
// dynamique title
if (!function_exists('dynamicTitle')) {
	function dynamicTitle(string $title){
		$base  = config("app.name");
		return isset($title) ? $title.' - '.$base : $base;
	}
}

// get ip address of the machine who connected to a website
if (!function_exists("getMachineIpAddress")) {
	  function getMachineIpAddress()
	  {
	  	return !empty($_SERVER['HTTP_CLIENT_IP']) 
	  						? $_SERVER['HTTP_CLIENT_IP'] 
	  						: !empty($_SERVER['HTTP_X_FORWARDED_FOR']) 
	  						? $_SERVER['HTTP_X_FORWARDED_FOR']
	  						: $_SERVER['REMOTE_ADDR'];
	  }
}

// geolocate user by ip address
if (!function_exists("geolocate")) {
	function geolocate(){
		// '129.0.76.157'
	   return geoip()->getLocation(getMachineIpAddress());
	   // return geoip()->getLocation('129.0.76.157');
	}
}

// all country
if (! function_exists('allCountries')) {
   function allCountries() {
	    return [
	      "AF" => "Afghanistan",
	        "AX" => "Aland Islands",
	        "AL" => "Albania",
	        "DZ" => "Algeria",
	        "AS" => "American Samoa",
	        "AD" => "Andorra",
	        "AO" => "Angola",
	        "AI" => "Anguilla",
	        "AQ" => "Antarctica",
	        "AG" => "Antigua and Barbuda",
	        "AR" => "Argentina",
	        "AM" => "Armenia",
	        "AW" => "Aruba",
	        "AU" => "Australia",
	        "AT" => "Austria",
	        "AZ" => "Azerbaijan",
	        "BS" => "Bahamas",
	        "BH" => "Bahrain",
	        "BD" => "Bangladesh",
	        "BB" => "Barbados",
	        "BY" => "Belarus",
	        "BE" => "Belgium",
	        "BZ" => "Belize",
	        "BJ" => "Benin",
	        "BM" => "Bermuda",
	        "BT" => "Bhutan",
	        "BO" => "Bolivia",
	        "BQ" => "Bonaire, Sint Eustatius and Saba",
	        "BA" => "Bosnia and Herzegovina",
	        "BW" => "Botswana",
	        "BV" => "Bouvet Island",
	        "BR" => "Brazil",
	        "IO" => "British Indian Ocean Territory",
	        "BN" => "Brunei Darussalam",
	        "BG" => "Bulgaria",
	        "BF" => "Burkina Faso",
	        "BI" => "Burundi",
	        "KH" => "Cambodia",
	        "CM" => "Cameroon",
	        "CA" => "Canada",
	        "CV" => "Cape Verde",
	        "KY" => "Cayman Islands",
	        "CF" => "Central African Republic",
	        "TD" => "Chad",
	        "CL" => "Chile",
	        "CN" => "China",
	        "CX" => "Christmas Island",
	        "CC" => "Cocos (Keeling) Islands",
	        "CO" => "Colombia",
	        "KM" => "Comoros",
	        "CG" => "Congo",
	        "CD" => "Congo, the Democratic Republic of the",
	        "CK" => "Cook Islands",
	        "CR" => "Costa Rica",
	        "CI" => "Cote D'Ivoire",
	        "HR" => "Croatia",
	        "CU" => "Cuba",
	        "CW" => "Curacao",
	        "CY" => "Cyprus",
	        "CZ" => "Czech Republic",
	        "DK" => "Denmark",
	        "DJ" => "Djibouti",
	        "DM" => "Dominica",
	        "DO" => "Dominican Republic",
	        "EC" => "Ecuador",
	        "EG" => "Egypt",
	        "SV" => "El Salvador",
	        "GQ" => "Equatorial Guinea",
	        "ER" => "Eritrea",
	        "EE" => "Estonia",
	        "ET" => "Ethiopia",
	        "FK" => "Falkland Islands (Malvinas)",
	        "FO" => "Faroe Islands",
	        "FJ" => "Fiji",
	        "FI" => "Finland",
	        "FR" => "France",
	        "GF" => "French Guiana",
	        "PF" => "French Polynesia",
	        "TF" => "French Southern Territories",
	        "GA" => "Gabon",
	        "GM" => "Gambia",
	        "GE" => "Georgia",
	        "DE" => "Germany",
	        "GH" => "Ghana",
	        "GI" => "Gibraltar",
	        "GR" => "Greece",
	        "GL" => "Greenland",
	        "GD" => "Grenada",
	        "GP" => "Guadeloupe",
	        "GU" => "Guam",
	        "GT" => "Guatemala",
	        "GG" => "Guernsey",
	        "GN" => "Guinea",
	        "GW" => "Guinea-Bissau",
	        "GY" => "Guyana",
	        "HT" => "Haiti",
	        "HM" => "Heard Island and Mcdonald Islands",
	        "VA" => "Holy See (Vatican City State)",
	        "HN" => "Honduras",
	        "HK" => "Hong Kong",
	        "HU" => "Hungary",
	        "IS" => "Iceland",
	        "IN" => "India",
	        "ID" => "Indonesia",
	        "IR" => "Iran, Islamic Republic of",
	        "IQ" => "Iraq",
	        "IE" => "Ireland",
	        "IM" => "Isle of Man",
	        "IL" => "Israel",
	        "IT" => "Italy",
	        "JM" => "Jamaica",
	        "JP" => "Japan",
	        "JE" => "Jersey",
	        "JO" => "Jordan",
	        "KZ" => "Kazakhstan",
	        "KE" => "Kenya",
	        "KI" => "Kiribati",
	        "KP" => "Korea, Democratic People's Republic of",
	        "KR" => "Korea, Republic of",
	        "XK" => "Kosovo",
	        "KW" => "Kuwait",
	        "KG" => "Kyrgyzstan",
	        "LA" => "Lao People's Democratic Republic",
	        "LV" => "Latvia",
	        "LB" => "Lebanon",
	        "LS" => "Lesotho",
	        "LR" => "Liberia",
	        "LY" => "Libyan Arab Jamahiriya",
	        "LI" => "Liechtenstein",
	        "LT" => "Lithuania",
	        "LU" => "Luxembourg",
	        "MO" => "Macao",
	        "MK" => "Macedonia, the Former Yugoslav Republic of",
	        "MG" => "Madagascar",
	        "MW" => "Malawi",
	        "MY" => "Malaysia",
	        "MV" => "Maldives",
	        "ML" => "Mali",
	        "MT" => "Malta",
	        "MH" => "Marshall Islands",
	        "MQ" => "Martinique",
	        "MR" => "Mauritania",
	        "MU" => "Mauritius",
	        "YT" => "Mayotte",
	        "MX" => "Mexico",
	        "FM" => "Micronesia, Federated States of",
	        "MD" => "Moldova, Republic of",
	        "MC" => "Monaco",
	        "MN" => "Mongolia",
	        "ME" => "Montenegro",
	        "MS" => "Montserrat",
	        "MA" => "Morocco",
	        "MZ" => "Mozambique",
	        "MM" => "Myanmar",
	        "NA" => "Namibia",
	        "NR" => "Nauru",
	        "NP" => "Nepal",
	        "NL" => "Netherlands",
	        "AN" => "Netherlands Antilles",
	        "NC" => "New Caledonia",
	        "NZ" => "New Zealand",
	        "NI" => "Nicaragua",
	        "NE" => "Niger",
	        "NG" => "Nigeria",
	        "NU" => "Niue",
	        "NF" => "Norfolk Island",
	        "MP" => "Northern Mariana Islands",
	        "NO" => "Norway",
	        "OM" => "Oman",
	        "PK" => "Pakistan",
	        "PW" => "Palau",
	        "PS" => "Palestinian Territory, Occupied",
	        "PA" => "Panama",
	        "PG" => "Papua New Guinea",
	        "PY" => "Paraguay",
	        "PE" => "Peru",
	        "PH" => "Philippines",
	        "PN" => "Pitcairn",
	        "PL" => "Poland",
	        "PT" => "Portugal",
	        "PR" => "Puerto Rico",
	        "QA" => "Qatar",
	        "RE" => "Reunion",
	        "RO" => "Romania",
	        "RU" => "Russian Federation",
	        "RW" => "Rwanda",
	        "BL" => "Saint Barthelemy",
	        "SH" => "Saint Helena",
	        "KN" => "Saint Kitts and Nevis",
	        "LC" => "Saint Lucia",
	        "MF" => "Saint Martin",
	        "PM" => "Saint Pierre and Miquelon",
	        "VC" => "Saint Vincent and the Grenadines",
	        "WS" => "Samoa",
	        "SM" => "San Marino",
	        "ST" => "Sao Tome and Principe",
	        "SA" => "Saudi Arabia",
	        "SN" => "Senegal",
	        "RS" => "Serbia",
	        "CS" => "Serbia and Montenegro",
	        "SC" => "Seychelles",
	        "SL" => "Sierra Leone",
	        "SG" => "Singapore",
	        "SX" => "Sint Maarten",
	        "SK" => "Slovakia",
	        "SI" => "Slovenia",
	        "SB" => "Solomon Islands",
	        "SO" => "Somalia",
	        "ZA" => "South Africa",
	        "GS" => "South Georgia and the South Sandwich Islands",
	        "SS" => "South Sudan",
	        "ES" => "Spain",
	        "LK" => "Sri Lanka",
	        "SD" => "Sudan",
	        "SR" => "Suriname",
	        "SJ" => "Svalbard and Jan Mayen",
	        "SZ" => "Swaziland",
	        "SE" => "Sweden",
	        "CH" => "Switzerland",
	        "SY" => "Syrian Arab Republic",
	        "TW" => "Taiwan, Province of China",
	        "TJ" => "Tajikistan",
	        "TZ" => "Tanzania, United Republic of",
	        "TH" => "Thailand",
	        "TL" => "Timor-Leste",
	        "TG" => "Togo",
	        "TK" => "Tokelau",
	        "TO" => "Tonga",
	        "TT" => "Trinidad and Tobago",
	        "TN" => "Tunisia",
	        "TR" => "Turkey",
	        "TM" => "Turkmenistan",
	        "TC" => "Turks and Caicos Islands",
	        "TV" => "Tuvalu",
	        "UG" => "Uganda",
	        "UA" => "Ukraine",
	        "AE" => "United Arab Emirates",
	        "GB" => "United Kingdom",
	        "US" => "United States",
	        "UM" => "United States Minor Outlying Islands",
	        "UY" => "Uruguay",
	        "UZ" => "Uzbekistan",
	        "VU" => "Vanuatu",
	        "VE" => "Venezuela",
	        "VN" => "Viet Nam",
	        "VG" => "Virgin Islands, British",
	        "VI" => "Virgin Islands, U.s.",
	        "WF" => "Wallis and Futuna",
	        "EH" => "Western Sahara",
	        "YE" => "Yemen",
	        "ZM" => "Zambia",
	        "ZW" => "Zimbabwe",
	    ];

	}
}