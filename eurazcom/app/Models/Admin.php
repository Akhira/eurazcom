<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    use HasFactory;

    protected $table = 'admins';

    protected $fillable = [
        'matricule',
        'name',
        'lastname',
        'nationality',
        'city',
        'address',
        'phone',
        'email',
        'skills',
        'degree',
        'establishment',
        'domaine',
        'verification_code',
        'block',
        'password'
    ];


     public function articles(){
    	return $this->hasMany(Article::class);
    }
}
