<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    use HasFactory;

    protected $table = 'articles';

    protected $fillable = [
    	'title',
    	'image',
    	'presentation',
    	'admin_id',
        'categorie_id'
    ];

    public $timestamps = false;

    public function admin(){
    	return $this->belongsTo(Admin::class);
    }

    public function comments(){
    	return $this->hasMany(Comment::class);
    }

    public function likes(){
        return $this->hasMany(Like::class);
    }

    public function categorie(){
        return $this->belongsTo(Categorie::class);
    }
}
