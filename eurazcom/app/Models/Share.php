<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Share extends Model
{
    use HasFactory;

    protected $table = 'shares';

    protected $fillabe = [
    	'address',
     	'articles_id'
    ];
 
    public function article()
    {
    	return $this->belongsTo(Article::class);
    }
}
