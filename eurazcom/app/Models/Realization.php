<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Realization extends Model
{
    use HasFactory;

     protected $table ='realizations';

    protected $fillable = [
        'title',
		'description',
		'image',
		'video',
		'client',
		'country',
		'category',
		'date'
    ];
}
