<?php

namespace App\Http\Controllers\users;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\ProjectRequest;
use Illuminate\Auth\AuthenticationException;
use App\Models\Project;
use App\Models\People;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

class ProjectController extends Controller
{
    public function home($value='')
    {
        return view('users.projects.home');   
    }
  
	public function getAllProjects()
	{
         $projects = [];
        if (Route::is('eurazcom.profil.quotations')) {
            $projects = Project::where('users_id',userAuth()->id)->where('project',0)->get();
         }else{
           $projects = Project::where('users_id',userAuth()->id)->where('project',1)
                                                                ->where('status','Accept')
                                                                ->get(); 
        }
  		return view("users.profil.projects.index",compact('projects'));
	}

    public function currentProjects()
    {
        $projects = [];
        if (Route::is('eurazcom.quotations.current')) {
            $projects = Project::where('users_id',userAuth()->id)->where('project',0)->where('status','Waiting for treatment')->get();
        }else{
           $projects = Project::where('users_id',userAuth()->id)->where('project',1)
                                                                ->where('deliver',0)
                                                                ->where('start',1)
                                                                ->where('status','Accept')
                                                                ->get();
        }
        return view("users.profil.projects.current",compact('projects'));
    }

    public function waitForValidation()
    {
        $projects = Project::where('users_id',userAuth()->id)->where('project',0)
                                                             ->where('status','Waiting for validation')
                                                             ->get();
        return view("users.profil.projects.waiting",compact('projects'));
    }

    public function traitement()
    {
        $projects = [];
        if (Route::is('eurazcom.quotations.traitement')) {
            $projects = Project::where('users_id',userAuth()->id)->where('project',0)
                                                                 ->where('status','In process')
                                                                 ->get();
        }else{
           $projects = Project::where('users_id',userAuth()->id)->where('project',1)
                                                           ->where('deliver',1)
                                                           ->where('status','Accept')
                                                           ->get();
        }
        return view("users.profil.projects.traitement",compact('projects'));
    }

    public function follow()
    {
        $projects = [];
        if (Route::is('eurazcom.quotations.follow')) {
            $projects = Project::where('users_id',userAuth()->id)->where('project',0)
                                                                 ->where('mark',1)
                                                                 ->get();
        }else{
           $projects = Project::where('users_id',userAuth()->id)->where('project',1)
                                                                ->where('mark',1)
                                                                ->where('status','Accept')
                                                                ->get();
        }
        return view("users.profil.projects.follow",compact('projects'));
    }

   

	// get project by code
	public function getProjectByCode($code)
{
	$project = self::findByCode($code);
    return view("users.profil.projects.show",compact('project'));
}
    //create
    public function create()
    {
    	return view("users.projects.create");
    }

    // store
    public function store(Request $request)
    {    
        // dd($request->all());
     	 $project = new Project;
    	 $project->code = substr_replace(generatorCode(6),verifyType($request->type),3,0);
         $project->title = $request->title;
         $project->slug = str_replace(" ", "-", strtolower($request->title));
         $project->activity = $request->sector;
         $project->type = $request->type;
     	 if (ifUserAuth()) {
    	 	$project->users_id = userAuth()->id;
    	 }else{
            $findPerson = People::where('email',$request->email)->first();
            if ($findPerson->count() > 0) {
                $project->peoples_id = $findPerson->id;
            }else{
        	 	$person = new People;
        	 	$person->name = $request->name;
        	 	$person->lastname = $request->lastname;
        	 	$person->phone = $request->phone;
        	 	$person->email = $request->email;
        	 	$person->country = $request->country;
                $person->city = $request->city;
        	 	$person->address = $request->address;
        	 	
        	 	$person->status = $request->status;
        	 	if ($request->status != "Particular") {
        	 	    $this->validate($request,['orgname'=>'required','role'=>'required']);
        	 	}
        	 	$person->organization_name = $request->orgname;
        	 	$person->role = $request->role;
        	 	$person->save();
         	 	$project->peoples_id = $findPerson->id;
             }
    	 }
	 	 $project->path = $request->file("chargeBook")->store("projects");     	 
    	 $project->save();
 
    	 // flashy
    	 return redirect()->route('welcome');
    }

    // edit project by code
	public function editProject($code)
	{
		 // $project = {};
		try {
 			$project = self::findByCode($code);
            return view("users.projects.edit",compact('project'));
   		} catch (AuthenticationException $e) {
			return view("errors.autError");
		}
		
	}

    public function update(ProjectRequest $request, $code)
    {
    	$project = self::findByCode($code);
    	$code = self::updateCode($request,$code);
    	$title = $request->title;
    	$activity = $request->activity;
     	DB::update("update projects set code=?, title=?,activity=?,start=?,duration=?,description=?,budget=? where code=?",
    	[$code,$title,$activity,$start,$duration,$description,$budget,$project->code]);
    	// update person datas
    	$name = $request->name;
    	$lastname = $request->lastname;
    	$phone = $request->phone;
    	$email = $request->email;
    	$country = $request->country;
    	$address = $request->address;
    	$status = $request->status;
    	$organization_name = "";
    	$role = "";
    	if ($request->status != "Particular") {
    		$this->validate($request,['orgname'=>'required','role'=>'required']);
    		$organization_name = $request->orgname;
	    	$role = $request->role;
    	} 
    	
    	DB::update("update projects set name=?,lastname=?,phone=?,email=?,country=?,address=?,status=?,
    		organization_name=?,role=? where code=?",
    	[$name,$lastname,$phone,$email,$country,$address,$status,$organization_name,$role, $project->peoples_id]);
    	// flashy
    	return redirect()->route("eurazcom.project.show",$project->code);
    }

    public function pathUpdate(Request $request,$code)
    {
    	 $project = self::findByCode($code);
    	 if (!empty($request->path)) {
    	 	DB::update("update projects set path=? where code=?",[$request->path,$project->code]);
    	 	// flashy
    	 	return redirect()->route("eurazcom.project.pdfViewer",$project->code);
    	 }
    }

    public function pdfViewer($code)
    {
    	 $project = self::findByCode($code);
    	return view("users.project.pdfViewer",compact('project'));
    }

    private function updateCode(Request $request,$code):string
    {
    	 $project = self::findByCode($code);
    	 if ($request->type != $project->type) {
    	 	return str_replace(substr($project->code,3,3), verifyType($$request->type), $project->code);
    	 }else{
    	 	return $project->code;
    	 }
    }

    // mark project
    public function markProject($code)
    {
  	 	$project = self::findByCode($code);
	 	if(!empty($project->users_id) && $project->users_id == userAuth()->id){
            $project->mark == 1 ? $project->mark = 0 : $project->mark = 1;
            $project->save();
	 	}
	 	// flashy
	 	return redirect()->back();
    }

    // reconize
    public function reconizeProjectByMine(Request $request, $code)
    {
    	$this->validate($request,['email'=>'required|email']);
    	$project = self::findByCode($code);
    	if (empty($project->users_id)) {
    		$person = People::where('email',$request->email)->count();
    		if ($person > 0) {
    			DB::update("update projects set users_id=? where code =?",[userAuth()->id,$project->code]);
    		}
    	}
    	// flashy
    	return redirect()->route('eurazcom.project.show',$project->code);
    }

    // transform demand to project
    public function transform(Request $request, $code)
    {
    	 $this->validate($request,['email'=>'required|email']);
         $project = self::findByCode($code);
         if (empty($project->users_id)) {
              return view("users.profil.project.reconizeInfos");
         }else{
            if (userAuth()->email === $request->email) {
                 $project->project = 1;
                 $project->status = "Accept";
                $project->save();
             }
            return redirect()->back();
         }
    }

    // search project
    public function searchProject(Request $request){
    	$this->validate($request,['query'=>'required']);
    	$q = request()->input('query');
        $project = project::where("code","like","%$q%")->orWhere("title","like","%$q%")->get();
        return view('users.projects.search', compact('project'));
    }
 
    // function find project by her code
    private function findByCode($code){
    	return Project::where('code',$code)->first();;
    }
}
