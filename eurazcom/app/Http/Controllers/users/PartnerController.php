<?php

namespace App\Http\Controllers\users;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\partnersRequest;
use App\Http\Requests\BusnessRequest;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use App\Models\Partner;
use App\Models\PartnerProject;
use App\Models\Request as Requests;

class PartnerController extends Controller
{
  //welcome
    public function welcome()
    {
    	return view('partners.welcome');
    }
    //welcome
    public function create()
    {
    	return view('partners.create');
    }

    // store
    public function store(Request $request)
    {
    	$partner = new Partner;
		$partner->code = matricule();
        $partner->type = $request->type;
        $partner->name = $request->name;
		$partner->lastname = $request->lastname;
		$partner->phone = $request->phone;
		$partner->email = $request->email;
		$partner->country = $request->country;
		$partner->address = $request->address;
		$partner->city = $request->city;
		$partner->cni = $request->cni_number;
	 	$partner->cni_image1 = $request->file('cni_img')->store('partners');
	 	$partner->cni_image2 = $request->file('cni_img2')->store('partners');
	 	$partner->avatar = $request->file('avatar')->store('partners');
	 	$partner->save();
	 	if ($partner != null) {
            // flashy
	 		return redirect()->route('eurazcom.partner.addConfirmation');
	 	}
     	// flashy
     	return redirect()->back();
    }

    // confirmation
     public function addConfirmation()
     {
     	 return view('partners.guest.confirmation');
     }

     // connexion 
     public function connexion()
     {
     	 return view('partners.connexion');
     }

     // send project Form
     public function project()
     {     
         $partner  = Partner::find(Session::get('code'));
         // dd($partner->code);
          return view('partners.project',compact('partner'));
     }

     // partner authentication
     public function partnerAuthentication(Request $request)
     {
     	 $this->validate($request,[
     	 	'code'=>'required',
  	 	],[
 	 		'code.exists'=>'This code is not exists o partners table'
 	 	]);
         $msg = 'Vous ne correspondez a aucun partenaire de notre agence : Entrer un code partenaire valide';
     	$partner = Partner::find($request->code);
        return $partner ? redirect()->route('eurazcom.partner.project')->with('code',$partner->code)
                        : redirect()->back()->with('warning',$msg);
        

    }

    public function storeProject(Request $request)
    {
        PartnerProject::create($request->only('clientName','clientPOBOX','clientCity','clientEmail','clientPhone','clientCountry','projectTitle','budget','categorie','currency'));
        //flashy
        return redirect()->back();
    }

    // forgot code Form
     public function forgotCode()
     {     
        return view('partners.forgotCode');
     }

     // forgot code logic function
     public function forgotCodeStore(Request $request)
     {    $this->validate($request,[
            'name' => 'required',
            'lastname' => 'required',
            'email' => 'required|email',
            'cni' => 'required',
          ]);
         Requests::create($request->only('name','lastname','email','cni'));
         return redirect()->route('welcome');
     }
  
     // find partner by her code
     private function findByCode($code){

     	return partner::where('code',$code)->first();
     }
}