<?php

namespace App\Http\Controllers\users;

use App\Http\Controllers\Controller;
use App\Http\Requests\SolutionRequest;
use Illuminate\Http\Request;
use App\Models\Solution;
use App\Models\ImagesBanK;
use Illuminate\Support\Facades\Route;

class solutionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllSolutions()
    {
        $solutions = Solution::orderBy('id','DESC')->simplepaginate(16);

        if (Route::is('admin.solutions.index')) {
           return view('admin.solutions.index',compact('solutions'));
        }
        return view('users.solutions.index',compact('solutions'));
    }

    // create form
    public function create()
    {
        return view('admin.solutions.create');
    }

    // store
    public function store(SolutionRequest $request)
    {
        $solution = new Solution;
        $solution->title = $request->name;
        $solution->slug = str_replace(" ", "-", $request->name);
        $solution->description = $request->description;
        $solution->category = $request->category;
        if ($request->hasFile('image') && $request->hasFile('image')) {
           $solution->image = $request->file('image')->store('solutions');
           $solution->video = $request->file('video')->store('solutions');
        }
        $solution->save();
        if ($solution != Null) {
           return redirect()->route('admin.solutions.index');
        }
        // flashy
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getSolutionById($id)
    {
        $solution = self::findSolutionById($id);
        // dd($solution);
        if (Route::is('admin.solutions.show',$solution->id)) {
           return view('admin.solutions.show',compact('solution'));
        }
        return view('users.solutions.detail',compact('solution'));
    }


    // edit
    public function edit($id)
    {
        $solution = self::findSolutionById($id);
        // return $solution;
        return view('admin.solutions.edit',compact('solution'));
    }

    // update
    public function update(Request $request, $id)
    {   $this->validate($request,[
            'name'=>'required',
            'description'=>'required',
            'category'=>'required',
        ]);
        // dd($request->all());
        $solution =  self::findSolutionById($id);
        $solution->title = $request->name;
        $solution->description = $request->description;
        $solution->category = $request->category;
        if ($request->hasFile('image')) {
            $solution->image = $request->file('image')->store('solutions');
        }
        if ($request->hasFile('video')) {
           $solution->video = $request->file('video')->store('solutions');
        }
        $solution->save();
        if ($solution != Null) {
            // flashy
           return redirect()->route('admin.solutions.show',$solution->id);
        }
        // flashy
        return redirect()->back();
    }

    // add images
    public function imagesForm($id)
    {
         $solution = self::findSolutionById($id);
         return view('admin.solutions.images',compact('solution'));
         
    }
    // add images
    public function addImages(Request $request,$id)
    {
        dd("ok");
        $this->validate($request,['images'=>'required | mimes:jpg']);
        $solution =  self::findSolutionById($id);
        $images = new ImagesBanK;
        foreach ($request->images as $image) {
            $images->path = $image;
            $images->solutions_id = $solution->id;
            $images->save();
        }

        if ($images != Null) {
             //flashy
            return redirect()->route('admin.solutions.show',$solution->id);
         }
         //flashy
         return redirect()->back();
    }

    // delete
    public function destroy($id)
    {
         Solution::destroy($id);
         // flashy
         return redirect()->route('admin.solutions.index');
    }

    private function findSolutionById($id){

        return Solution::find($id);
    }
}
