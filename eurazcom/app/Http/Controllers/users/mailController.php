<?php

namespace App\Http\Controllers\users;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use  App\Mail\EmailActivation;

class mailController extends Controller
{
    public static function sendMail($name,$email,$verification_code){
     	$data = [
     		'name'=>$name,
     		'verification_code'=>$verification_code,
     	];
     	Mail::to($email)->send(new EmailActivation($data));
     }
}
