<?php

namespace App\Http\Controllers\users;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Event;
use App\Models\EventReservation;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class EventController extends Controller
{
    //show all events
    public function getAllEvents()
    {
    	$events = Event::orderBy("date","ASC")->get();
    	return view("users.events.index",compact("events"));
    }

    // show event
    public function showEvent($id)
    {
    	$event = self::findEventById($id);
    	return view("users.events.show",compact("event"));
    }

    // show form reservation
    public function reservationForm($eventIdid)
    {
    	$event = self::findEventById($eventId);
    	return view("users.events.reservation",compact("event"));
    }

    // Make reservation
    public function makeReservation(EventReservationRequest $request)
    {
    	 // $event = self::findEventById($eventId);
    	 $reservation = EventReservation::create($request->only('code','events_id','name','email','phone','country','city','condition'));
    	 Mail::to($reservation->email)->send(new EventMessage($reservation));
    	 // flashy
    	 return redirect()->back();

    }
    // verify reservation
    public function verifyReservation($code)
    {
    	$reservation = EventReservation::where('code',$code)->first();
    	return view("users.events.verify",compact("reservation"));
    }

    // Cancel reservation
    public function cancelReservation($reservationId)
    {
    	$eventResv = EventReservation::find($reservationId);
    	DB::update("update event_reservations set cancel=? where id=?",[1,$eventResv->id]);
    	// flashy
    	return redirect()->back();
    }


    private function findEventById($eventId):Event{
    	return Event::find($eventId);
    }
}
