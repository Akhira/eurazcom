<?php

namespace App\Http\Controllers\users;

use App\Http\Controllers\Controller;
use App\Mail\contactMessage;
use App\Http\Requests\contactRequest;
use App\Models\users\Contact;
use Illuminate\Support\Facades\Mail;

class contactController extends Controller
{
    //create form
    public function create()
    {
    	return view('users.contact.create');
    }

    // store
	public function sendEmail(contactRequest $request){
		  $contact = Contact::create($request->only('name','email','subject','message'));
  		Mail::to($contact->email)->send(new contactMessage($contact));
  		// flashy
  		return redirect()->back();
	}

	public function showmessagetemplate()
	{
		return new contactMessage;
	}
}
