<?php

namespace App\Http\Controllers\users;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Realization;
use App\Models\ImagesBank;
use App\Http\Requests\PortfolioRequest;

class realizationController extends Controller
{
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllRealizations()
    {
        $realizations = Realization::distinct()->orderBy('id','DESC')->simplepaginate(16);
        return view('users.portfolio.index',compact('realizations'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getRealizationById($id)
    {
        $realization = Realization::find($id);
        return view('users.portfolio.detail',compact('realization'));
    }
 
}
