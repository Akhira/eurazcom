<?php

namespace App\Http\Controllers\users;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Academy;
use App\Models\Testimonial;
use App\Models\Event;

class AcademyController extends Controller
{
    //home page
    public function academyHome()
    {	
    	 $trainings = Academy::all();
    	 $testimonials = Testimonial::orderBy('id','DESC')->get();
    	 $events = Event::where("status","<>","past")->orderBy("date",'ASC')->get();
    	 return view("academy.home",compact('trainings','testimonials','events'));
    }

    public function getTrainingById($id)
    {
    	$training = Academy::find($id);
    	return view("academy.training",compact('training'));
    }

    // testimonials
    public function testimonial($value='')
    {
    	return view("academy.testimonial");
    }

    public function testimonialStore(Request $request)
    {
    	$this->validate($request,['name'=>'required','avatar','comment']);
    	Testimonial::create($request->only('name','avatar','comment'));
    	// flashy
    	return redirect()->route('academy');
    }
}
