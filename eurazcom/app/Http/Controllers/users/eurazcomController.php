<?php

namespace App\Http\Controllers\users;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\Solution;
use App\Models\Realization;
use App\Models\Newsletter;
use App\Models\Testimonial;
 

class eurazcomController extends Controller
{
    //home
    public function welcome()
    {
    	return  view('welcome');
    }

    //about page
    public function about()
    {
    	return  view('about');
    }


    /*****************************************/
    /*			ACADEMY CONTROLLER 
    /****************************************/ 
    public function academyHome()
    {
        return view("academy.home");    }
     
     /*****************************************/
    /*           SOLUTIONS CONTROLLER 
    /****************************************/ 
    public function getAllSolutions()
    {
        $solutions = Solution::orderBy('id','DESC')->groupBy('category')->simplepaginate(16);
        return  view('users.solutions.index',compact('solutions'));
    }

    public function getSolutionById($id)
    {
        $solution = Solution::find($id);
        return  view('users.solutions.show',compact('solution'));
    }
 

    /*****************************************/
    /*           NEWSLETTER CONTROLLER 
    /****************************************/
    public function newsletter(Request $request)
    {
        $this->validate($request,['email'=>'required|email|unique:newsletters']);
        Newsletter::create($request->only('email'));
        // flashy
        return redirect()->back();
    }


    // testimonials
    public function testimonials()
    {
        return view('users.testimonials.testimonial');
    }

    public function doneTestimonials(Request $request)
    {
         $this->validate($request,[
            'name'=>'required',
            'profession'=>'required',
            'image'=>'required',
            'content'=>'required|max:200',
         ]);  

         $testimonial = new Testimonial;
         $testimonial->name = $request->name;  
         $testimonial->profession = $request->profession;
         if ($request->hasFile('image')) {
         $testimonial->image = $request->file('image')->store('testimonials');  
         }  
         $testimonial->content = $request->content;
         $testimonial->save();
         if ($testimonial != null) {
             return redirect()->route('welcome');
         }  
         
    }
}
