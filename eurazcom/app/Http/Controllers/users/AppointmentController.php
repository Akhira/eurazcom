<?php

namespace App\Http\Controllers\users;

use App\Http\Controllers\Controller;
// use Illuminate\Http\Request;
use App\Models\Appointment;
use App\Http\Requests\appointmentRequest;

class AppointmentController extends Controller
{
    //create
    public function create(){
    	return view('users.appointment.create');
    }

    // store
    public function store(appointmentRequest $request){

    	$appointment = appointment::create($request->only('name','phone','email','date','hours','target','type','object'));
    	if ($appointment) {
    		//flashy
    		return redirect()->back();
    	}else{
    		//flashy
    		return redirect()->back;
    	}
    }
}
