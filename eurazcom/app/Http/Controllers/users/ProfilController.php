<?php

namespace App\Http\Controllers\users;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Project;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class ProfilController extends Controller
{
    //welcome
    public function welcome()
    {
        // dd(userAuth()->id);
    	return view('users.profil.welcome');
    }

    //about
    public function about()
    {
    	return view('users.profil.about');
    }

    public function edit($value='')
    {
    	return view('users.profil.edit');
    }

    // update infos
    public function updateUserInfos(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',
            'email'=>'required|email',
            'city'=>'required',
            'address'=>'required',
            'lastname'=>'required',
            'phone'=>'required',
            'country'=>'required',
        ]);
        $name = $request->name;
        $email = $request->email;
        $city = $request->city;
        $address = $request->address;
        $lastname = $request->lastname;
        $phone = $request->phone;
        $country = $request->country;
        DB::update('update users set name=?,email=?,city=?,address=?,lastname=?,phone=?,country=? where id=?',
            [$name,$email,$city,$address,$lastname,$phone,$country,userAuth()->id]);
        return redirect()->back();
    }

     // update avatar
    public function updateUserAvatar(Request $request)
    {
        $this->validate($request,[
            'avatar'=>'required',
        ]);
        $user = self::findUser(UserAuth()->id);
        $user->avatar = $request->file('avatar')->store('users');
        $user->save();
        return redirect()->back();
    }

    public function status($value='')
    {
        // dd(userAuth()->status);
        return view('users.profil.status');
    }

    // update entreprise infos
    public function updateStatus(Request $request)
    {
        $user = self::findUser(userAuth()->id);
        $status = $request->status;
        // return $user->status;
        if ($status === 'Particular' && $user->status != "Particular") {
            DB::update('update users set status=?, organization_name=?,users_role=?,organization_email=?,organization_sector=?,organization_address=?,organization_phone=?,organization_logo=? where id=?',
            [$status, "","","","","","","",userAuth()->id]);
        }else{
            if ($status != 'Particular') {
                   $this->validate($request,[
                    'orgname'=>'required',
                    'orgemail'=>'required|email',
                    'orgsector'=>'required',
                    'orgrole'=>'required',
                    'orgphone'=>'required',
                    'address'=>'required',
                 ]);  
            }
            if (empty($user->organization_logo)) {
               $this->validate($request,['logo'=>'required']); 
            }
            $orgname = $request->orgname;
            $orgemail = $request->orgemail;
            $orgsector = $request->orgsector;
            $orgrole = $request->orgrole;
            $orgphone = $request->orgphone;
            $address = $request->address;
            DB::update('update users set status=?, organization_name=?,users_role=?,organization_email=?,organization_sector=?,organization_address=?,organization_phone=? where id=?',
                [$status, $orgname,$orgrole,$orgemail,$orgsector,$address,$orgphone,userAuth()->id]);

            if ($request->hasFile('logo')) {
                $logo = $request->file('logo')->store("users");
                DB::update('update users set organization_logo=? where id=?',
                [$logo,userAuth()->id]);
            }
        }
        return redirect()->back();
    }

    // close account
    public function closeAccount()
    {
       return view('users.profil.closeAccount');
    }

    public function storeCloseAccount(Request $request)
    {
       $user = self::findUser(userAuth()->id);
       $reason = $request->reason;
       DB::update('update users set reason=?,close=? where id=?',[$reason,1,userAuth()->id]);
    }

    public function changerPassword($value='')
    {
        return view('users.profil.password');
    }

    public function updatePassword(Request $request)
    {
         $this->validate($request,[
            'password'=>'required',
            'new_password'=>'required',
            'c_new_password'=>'required|same:new_password',

         ]);
         $user = self::findUser(userAuth()->id);
         $password = Hash::make($request->password);
         if ($password != $user->password) {
            return redirect()->back()->with("warning","Your current password isn't correct");
         }else{
            DB::update("update users set password=? where id=?",[Hash::make($request->new_password), $user->id]);
            return redirect()->back();
         }
    }


    private function findUser($id)
    {
         return User::find($id);
;
    }
}
