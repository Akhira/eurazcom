<?php

namespace App\Http\Controllers\users;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    
    public function webSiteCreation($value='')
    {
    	return view('services.webSiteCreation');
    }

    public function software($value='')
    {
    	return view('services.software');
    }

    public function erpIntegration($value='')
    {
    	return view('services.erp');
    }
    public function brandinDesign($value='')
    {
    	return view('services.graphic');
    }

    public function adverts($value='')
    {
    	return view('services.adverts');
    }
    public function marketingAndCom($value='')
    {
    	return view('services.marketing');
    }

    public function consulting($value='')
    {
    	return view('services.consulting');
    }

    public function trainings($value='')
    {
    	return view('services.trainings');
    }
}
