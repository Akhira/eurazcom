<?php

namespace App\Http\Controllers\users;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Article;
use App\Models\Comment;
use App\Models\Like;
use App\Models\View;
use App\Models\Share;
use App\Http\Requests\CommentRequest;


class ArticleController extends Controller
{   

    /**
    * Afficher l'ensemble des articles deja publies.
    *
    * @return \Illuminate\Http\Response
    */
    public function getAllPublications()
    {
        $articles = Article::query()->with(['admin'=>function($query){
                $query->select("name","lastname");
        }])->orderBy('id','DESC')->where('publish',1)->simplepaginate(20);
    	return  view('users.publications.index',compact('articles'));
    }

    /**
    * Obtenir un article unique.
    *
    * @param  int $id
    * @return \Illuminate\Http\Response
    */
    public function getPublicationById(int $id){
        $article = Article::query()->with(["comments"=>function($query){
            $query->select('*');
        }])->find($id);
        $articleCategories = Article::with(['admin'])->where('categorie_id', $article->categorie_id)->limit(5)->get();
        self::viewArticle($id);
        return  view('users.publications.show',compact('article','articleCategories'));
    }

    /**
    * Filtrer les articles en fonction des parametres(Popularite,ancienete,like,partager).
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function filterPublications(Request $request)
    {
        $articles = [];
        $query = $request->input('filter');
        switch ($query) {
            case 'date':
                $articles = Article::query()->with(['admin'=>function($query){
                    $query->select("name","lastname");
                }])->orderBy('date_publication','ASC')->where('publish',1)->simplepaginate(20);
                break;
            case 'most like':
                $articles = Article::query()->with(['admin'=>function($query){
                    $query->select("name","lastname");
                }])->orderBy('likes','DESC')->where('likes','<>',0)->where('publish',1)->simplepaginate(20);
                break;
            case 'popular':
                $articles = Article::query()->with(['admin'=>function($query){
                    $query->select("name","lastname");
                }])->orderBy('views','DESC')->where('views','<>',0)->where('publish',1)->simplepaginate(20);
                break;
            case 'most share':
                $articles = Article::query()->with(['admin'=>function($query){
                    $query->select("name","lastname");
                }])->orderBy('shares','DESC')->where('shares','<>',0)->where('publish',1)->simplepaginate(20);
                break;

            default:
               return redirect()->route('eurazcom.publications');
                break;
        }
        return  view('users.publications.filter',compact('articles'));
    }

    /**
    * Filtrer les articles par leur categorie.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function categoriesFilter(Request $request)
    {
         $articles = Article::query()->with(['admin'=>function($query){
                $query->select("name","lastname");
         }])->orderBy('id','DESC')->where('categorie_id',$request->filter)->where('publish',1)->simplepaginate(20);
         return  view('users.publications.filter',compact('articles'));
    }

    /**
    * search Articles.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function searchPublications(Request $request)
    {
        $query = $request->input('search');
        $articles = Article::with(['admin'])->where('title','like','%$query%')->simplepaginate(10);
        return  view('users.publications.search',compact('articles'));
    }


    /**
    * Comment article.
    *
    * @param  int  $articleId
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function commentPublication(CommentRequest $request,int $articleId)
    {
        $comment = new Comment;
          $article = Article::find($articleId);
          $comment->name = $request->name;
          $comment->country = getUserCountry();
          $comment->article_id = $article->id;
          $comment->content = $request->content;
          $comment->address = userMacAddress();
          if ($request->hasFile('image')) {
              $comment->image = $request->file('image')->store('users');
          }
          $comment->save();
         // flashy
        return redirect()->back();
        
    }

    public function likePublication(int $articleId)
    {
          $like  = Like::where('articles_id',$articleId)->where('address',userMacAddress())->count();
          $article  = Article::find($articleId);
          if ($like > 0) {
              $findLike = Like::where('address',userMacAddress())->first();
              Like::destroy($findLike->id);
              $article->likes -=1;
          }else{
            $newLike = new Like;
            $newLike->articles_id = $articleId;
            $newLike->address  = userMacAddress();
            $newLike->save();

            if ($newLike != null) {
               $article->likes +=1;
            }
          }
          $article->save();
          return redirect()->back();
    }

    public function shares(Request $request, int $articleId){
        //a revoir
        Share::create($request->only('article'));
        $article = Article::find($articleId);
        $article->shares += 1;
        $article->save();
        return redirect()->back();
    }

    private function viewArticle(int $articleId){
        $views  = View::where('articles_id',$articleId)->where('address',userMacAddress())->count();
        if ($views == 0) {
            $view = new View;
            $view->articles_id = $articleId;
            $view->address = userMacAddress();
            $view->save();
            if ($view != null) {
                $article = Article::find($articleId);
                $article->views += 1;
                $article->save();
            }
        }
    }

}
