<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use App\Models\Project;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\users\mailController;
use App\Http\Requests\UserRequest;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  Request  $request
     * @return \App\Models\User
     */
    protected function register(Request $request)
    {
        // dd($request->all());
        $status = $request->status;
        if ($status !="Particular") {
            $this->validate($request,[
                'orgname'=>'required',
                'role'=>'required'
            ]);
        }
        $user = new User();
        $user->name = $request->name;
        $user->lastname = $request->lastname;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->city = $request->city;
        $user->country = $request->country;
        $user->status = $request->status;
        $user->status = $status;
        $user->organization_name = $request->orgname;
        $user->users_role = $request->role;
        $user->password = Hash::make($request->password);
        $user->verification_code = sha1(time());
        $user->save();
        // infos sur le projet
         $project = new Project;
         $project->code = substr_replace(generatorCode(6),verifyType($request->type),3,0);
         $project->title = $request->projectName;
         $project->slug = str_replace(" ", "-", strtolower($request->projectName));
         $project->activity = $request->sector;
         $project->type = $request->type;
         $project->path = $request->file('chargeBook')->getClientOriginalName()->store('projects');
         $currentUser = User::where('email',$request->email)->first();
         $project->users_id = $currentUser->id;
         $project->save();
        if ($user != null && $project != null) {
             mailController::sendMail($user->name, $user->email,$user->verification_code);
            return redirect()->route("confirmation")
                             ->with('success','Consustez votre boite mails ett activez votre compte en cliquant sur le lien.');
        }

        return redirect()->back()->with('warning','Erreur est survenue lors de l\'enregistrement');
    }

    public function verifyUser(Request $request){

        $verification_code = $request->get('code');
        $user = User::where(['verification_code'=>$verification_code])->first();
        if ($user !=null) {
            $user->is_verified = 1;
            $user->save();
            return redirect()->route('login')->with('success','Please connect your self');
        }
        return redirect()->route('login')->with('success','Please connect your self');
    }

    public function confirmation()
    {
       return view('auth.confirmation');
    }
}
