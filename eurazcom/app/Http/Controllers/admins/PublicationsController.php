<?php

namespace App\Http\Controllers\admins;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Article;

class PublicationsController extends Controller
{
    //index
    public function index()
    {
    	$publications = Article::where('publish',0)->orderBy('date_publication','DESC')->simplePaginate(10);
    	return view("admin.publications.index",compact('publications'));
    }

    // show unique
    public function show($id)
    {
    	$publication = self::findPublicationBy($id);
    	return view("admin.publications.show",compact('publication'));
    }

    // create
    public function create(){

    	return view("admin.publications.create");
    }

    // store
    public function store(Request $request)
    {
    	 $this->validate($request,[
    	 	'title'=>'required',
    	 	'presentation'=>'required',
    	 	'image'=>'required',
    	 ]);

    	 $publication = new Article;
         $title = $request->title;
    	 $publication->title = $title;
         $publication->content = "djskjdksj";
         $publication->admins_id = 1;
         $publication->slug = str_replace(" ", "-", strtolower($title));
    	 $publication->presentation = $request->presentation;
    	 if ($request->hasFile('image')) {
    	 	$publication->image = $request->file('image')->store('blog');
    	 }
    	 $publication->save();
    	 if ($publication != Null) {
    	 	 return redirect()->route('admin.publications');
    	 }
    	 return redirect()->back();
    }

    // edit unique
    public function edit($id)
    {
    	$publications = self::findPublicationBy($id);
    	return view("admin.publications.edit",compact('publications'));
    }

    // store
    public function update(Request $request, $id)
    {
    	 $this->validate($request,[
    	 	'title'=>'required',
    	 	'presentation'=>'required',
    	 ]);

    	 $publication = self::findPublicationBy($id);
    	 $publication->title = $request->title;
    	 $publication->presentation = $request->presentation;
    	 if ($request->hasFile('image')) {
    	 	$publication->image = $request->file('image')->store('blog');
    	 }
    	 $publication->save();
    	 if ($publication != Null) {
    	 	 return redirect()->route('admin.publications.show',$publication->id);
    	 }
    	 return redirect()->back();
    }



    public function destroy($id)
    {
    	Article::destroy($id);
    	return redirect()->route('admin.publications.index');
    }

    // content

    public function content()
    {
    	$publication = self::findPublicationBy($id);
    	return redirect()->route('admin.publications.content',compact('publication'));
    }

    public function storeContent(Request $request,$id)
    {
    	$publication = self::findPublicationBy($id);
    	$publications->content  =  $request->content;
    	$publications->updated_at  =  \DateTime(now());
    	$publication->save();
    	return redirect()->route('admin.publications.show',$publication->id);
    }


    private function findPublicationBy($id){
    	return Article::find($id);
    }
}
