<?php

namespace App\Http\Controllers\admins;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin;
use App\Http\Requests\CollaboratorRequest;

class CollaboratorController extends Controller
{
    //index
    public function getAllCollaborators($value='')
    {
    	$collaborators = Admin::simplepaginate(9);
    	return view('admin.collaborator.index',compact('collaborators'));
    }

    public function create($value='')
    {
    	return view('admin.collaborator.create');
    }

    public function store(CollaboratorRequest $request)
    {
     	 $collaborator = Admin::create($request->only('name','matricule','skills','lastname','email','phone','nationality','city','address','domaine','degree','establishment','password','verification_code'));

    	 if ($collaborator != Null) {
    	 	return redirect()->route('admin.collaborators');
    	 }
    	 return redirect()->back();
    }

    public function block($id)
    {	 
    	$collaborator = Admin::find($id);
    	$collaborator->block == 1?$collaborator->block  = 0:$collaborator->block  = 1;
    	$collaborator->save();
    	return redirect()->back();
    }
}
