<?php

namespace App\Http\Controllers\admins;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Realization;
use App\Models\ImagesBank;
use Illuminate\Support\Facades\Route;
use App\Http\Requests\PortfolioRequest;

class RealizationsController extends Controller
{
 /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllRealizations()
    {
	    $realizations = Realization::distinct()->orderBy('id','DESC')->simplepaginate(16);
        return view('admin.portfolio.index',compact('realizations'));
  	}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getRealizationById($id)
    {
        $realization = self::findRealizationById($id);
        return view('admin.portfolio.show',compact('realization'));
    }

    // create form
    public function create()
    {
        return view('admin.portfolio.create');
    }

    // store
    public function store(PortfolioRequest $request)
    {
        $portfolio = new Realization;
        $name = $request->name;
        $portfolio->title =  $name;
        $portfolio->slug = str_replace(" ", "-", strtolower($name));
        $portfolio->description = $request->description;
        $portfolio->client = $request->client;
        $portfolio->country = $request->country;
        $portfolio->date = $request->date;
        $portfolio->url = "http://www.".$request->url;
        $portfolio->category = $request->category;
        if ($request->hasFile('image') && $request->hasFile('image')) {
           $portfolio->image = $request->file('image')->store('realizations');
           $portfolio->video = $request->file('video')->store('realizations');
        }
        $portfolio->save();
        if ($portfolio != Null) {
           return redirect()->route('admin.portfolio.index');
        }
        // flashy
        return redirect()->back();
    }

     // edit
    public function edit($id)
    {
        $realization = self::findRealizationById($id);
        // return $solution;
        return view('admin.portfolio.edit',compact('realization'));
    }

    // update
    public function update(Request $request,$id)
    {
        $this->validate($request,[
            'name'=>'required',
            'description'=>'required',
            'category'=>'required',
            'client'=>'required ',
            'country'=>'required ',
            'date'=>'required | date |before_or_equal:today ',
        ]);
        $portfolio = self::findRealizationById($id);
        $portfolio->title = $request->name;
        $portfolio->description = $request->description;
        $portfolio->client = $request->client;
        $portfolio->country = $request->country;
        $portfolio->date = $request->date;
        $portfolio->url = $request->url;
        $portfolio->category = $request->category;
        if ($request->hasFile('image')) {
           $portfolio->image = $request->file('image')->store('realizations');
        }
        if ($request->hasFile('image')) {
           $portfolio->video = $request->file('video')->store('realizations');
        }
        $portfolio->save();
        if ($portfolio != Null) {
           return redirect()->route('admin.portfolio.show',$portfolio->id);
        }
        // flashy
        return redirect()->back();
    }


    // add images
    public function imagesForm($id)
    {
         $realization = self::findRealizationById($id);
         return view('admin.portfolio.images',compact('realization'));
         
    }

    // store images
    public function storeImages(Request $request,$id)
    {
        // dd($request->images);
         // $this->validate($request,['images'=>'required']);
         $images = new ImagesBank;
         $realization = self::findRealizationById($id);
        
            foreach ($request->images as $value) {
             }
         // if ($images != Null) {
         //     //flashy
         //    return redirect()->route('admin.portfolio.show',$realization->id);
         // }
         //flashy
         // return redirect()->back();
    }

     // delete
    public function destroy($id)
    {   
         Realization::destroy($id);
         // flashy
         return redirect()->route('admin.portfolio.index');
    }



    private function findRealizationById($id){

        return Realization::find($id);
    }
}
