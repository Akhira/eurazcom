<?php

namespace App\Http\Controllers\admins;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Contact;
use App\Models\User;
use App\Models\Newsletter;

class AdminController extends Controller
{
    //home
    public function welcome()
    {
    	return view('admin.welcome');
    }

    public function login($value='')
    {
        return view('admin.login');
    }


    /**********************************/ 
    //	Contact System
    /*********************************/ 
    // contacts
    public function contacts($value='')
    {
    	$contacts = Contact::orderBy('id','DESC')->get();
    	return view("admin.contacts.index",compact('contacts'));
    }
    public function show($id)
    {
        $contact = Contact::find($id);
        return view("admin.contacts.show",compact('contact'));
    }

    public function destroy($id)
    {
    	Contact::destroy($id);
    	return redirect()->back();
    }
    public function destroyAll()
    {
    	$contacts = Contact::all();
    	foreach ($contacts as $contact) {
    		Contact::destroy($contact->id); 
    	}
    	return redirect()->back();
    }

    /**********************************/ 
    //	Newsletter System
    /*********************************/ 
    public function newsletter()
    {
    	$newsletters = Newsletter::simplepaginate(20);
    	return view("admin.newsletter.index",compact('newsletters'));
    }




    /**********************************/ 
    //  Clients System
    /*********************************/ 
    public function getAllClients()
    {
        $users = User::simplepaginate(20);
        return view("admin.clients.index",compact('users'));
    }

    public function getAllClientById($id)
    {
        $user = User::find($id);
        // return view("admin.clients.show");
        return view("admin.clients.show",compact('user'));
    }



}
