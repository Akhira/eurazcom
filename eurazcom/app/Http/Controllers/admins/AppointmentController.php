<?php

namespace App\Http\Controllers\admins;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Appointment;
use Illuminate\Support\Facades\Route;

class AppointmentController extends Controller
{
    //index
    public function index($value='')
    {
    	$apppointments = Appointment::orderBy('date','ASC')->get();
        if (Route::is('admin.appointments.former')) {
            $apppointments = Appointment::orderBy('date','ASC')->get();
            return view("admin.appointment.former",compact('apppointments'));
        }
    	return view("admin.appointment.index",compact('apppointments'));
    }
 
    //show
    public function show($id)
    {
        $apppointment = Appointment::find($id);
        return view("admin.appointment.show",compact('apppointment'));
    }


    // auto destroy appointment when the date past
    public function autoDestroyAppoiment()
    {
    	$appointments = Appointment::all();
    	foreach ($appointments as $appointment) {
    		if ($currDate > $appointment->date) {
    		 	Appointment::destroy($appointment->id);
    		 } 
    	}
    	return redirect()->back();
    }
}
