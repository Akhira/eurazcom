<?php

namespace App\Http\Controllers\admins;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Partner;

class PartnerController extends Controller
{
    //index
    public function index($value='')
    {
    	$partners = Partner::simplepaginate(30);
    	return view("admin.partners.index",compact('partners'));
    }

    //show
    public function show($code)
    {
    	$partner = self::getPartnerByCode($code);
    	return view("admin.partners.show",compact('partner'));
    }


    //edit
    public function edit($code)
    {
    	$partner = self::getPartnerByCode($code);
    	return view("admin.partners.edit",compact('partner'));
    }

    // block
    public function block($code)
    {   
    	$partner = self::getPartnerByCode($code);
    	$partner->block == 1 ? $partner->block = 0 : $partner->block = 1;
    	$partner->save();
    	return redirect()->back();
    }

    // agree
    public function agree($code)
    {
     	$partner = self::getPartnerByCode($code);
    	$partner->accepted = 1;
    	$partner->save();
    	return redirect()->back();
    }

    // destroy partner
    public function destroy($code)
    {
    	 Partner::destroy($code);
    	 return redirect()->route('admin.partners.index');
    }

    // search partner
    public function ResultSearch()
    {      
        $query = request()->input('search');
        // return $query;
         $partners = Partner::where("name","like","$query%")->orWhere("lastname","like","$query%")
                                                            ->orWhere("code","like","$query%")
                                                            ->get();
        return view('admin.partners.ResultSearch',compact('partners'));
    }


    private function getPartnerByCode($code){
    	return Partner::where('code',$code)->first();
    }


}
