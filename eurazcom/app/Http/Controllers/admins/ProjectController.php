<?php

namespace App\Http\Controllers\admins;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Project;
use Illuminate\Support\Facades\Route;

class ProjectController extends Controller
{
    //get all quotations
    public function getAllQuotations()
    {
            $projects = Project::query()->with(['user'=>function($query){
                $query->select("name");
            }])->where('project',0)->get();
            return view('admin.projects.quotations.index',compact('projects'));
          
    }

    //get all projects
    public function getAllProjects()
    {
            $projects = Project::query()->with(['user'=>function($query){
                $query->select("name");
            }])->where('project',1)->get();
            return view('admin.projects.quotations.index',compact('projects'));
          
    }

    //show
    public function show($code)
    {   
     	$findProject = self::getByCode($code);
        $project = empty($findProject->users_id) ? self::getByCode($code) :  self::getByCode($code);
      	return view('admin.projects.show',compact('project'));
    }

    // edit
    public function edit($code)
    {
        $findProject = self::getByCode($code);
        $project = empty($findProject->users_id) ? self::getByCode($code) :  self::getByCode($code);
        return view('admin.projects.edit',compact('project'));
    }

    // update
    public function update(Request $request, $code)
    {
      	$this->validate($request,[
     		'budget'=>'required',
    	 ]);
    	 $project = self::getByCode($code);
      	 $project->final_budget = $request->budget;
    	 $project->status = "Waiting for validation";
    	 $project->save();
    	 return redirect()->route('admin.project.show',$project->code);
    }

    // traitement 
    public function starTraitment($code)
    {
     	$project = self::getByCode($code);
        if ($project->project == 0) {
            $project->status = "In process";
            $project->save();
        }
	 	return redirect()->back();
    }

    // download doc
    public function downloadDoc($code)
    {
    	$project = self::getByCode($code);
    }

    // livre
    public function deliver($code)
    {
        $project = self::getByCode($code);
        if ($project->start == 1) {
            $project->deliver = 1;
            $project->save();
            return redirect()->back();
        }
        return redirect()->back();
        
    }

    // start
    public function start($code)
    {
        $project = self::getByCode($code);
        if ($project->status === "Accept") {
            $project->start = 1;
            $project->save();
            return redirect()->back();
        }
        return redirect()->back();
    }

    private function getByCode($code){
    	return Project::where('code',$code)->first();
    }
}
