<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class portfolioRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required | max:50',
            'description' => 'required | max:1000',
            'image' => 'required | mimes:jpg,JPG',
            'images' => 'required | mimes:jpg,JPG',
            'video' => 'required | mimes:mp4,MP4,avi,AVI',
            'client' => 'required',
            'country' => 'required',
            'category' => 'required',
            'date' => 'required | date | after_or_equal:today',
        ];
    }
}
