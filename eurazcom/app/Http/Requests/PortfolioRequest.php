<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PortfolioRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required',
            'description'=>'required',
            'category'=>'required',
            'image'=>'required ',
            'client'=>'required ',
            'country'=>'required ',
            'date'=>'required | date |before_or_equal:today ',
            'video'=>'required',
        ];
    }
}
