<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // infos sur le projet
            'type'=>'required',
            'projectName'=>'required',
            'sector'=>'required',
            // 'duration'=>'required',
            // 'description'=>'required',
            // 'budget'=>'required',
            // 'amount'=>'required',
            'chargeBook'=>'required|mimes:pdf',
            // 'startDate'=>'required|date|afeter:today',

            'name'=>'required',
            'lastname'=>'required',
            'email'=>'required|email|unique:users',
            'phone'=>'required|unique:users',
            'city'=>'required',
            'password'=>'required|min:8|confirmed',
            'c_password'=>'required|same:password',
        ];
    }
}
