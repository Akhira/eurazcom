<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class partnersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required',
            'lastname'=>'required',
            'phone'=>'required | unique:partners',
            'email'=>'required | email | unique:partners',
            'address'=>'required',
            'city'=>'required',
            // 'type'=>'required',
            'cni'=>'required',
            'cni_image1'=>'required',
            'cni_image2'=>'required',
        ];
    }
}
