<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CollaboratorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required',
            'lastname'=>'required',
            'email'=>'required|email|unique:admins',
            'phone'=>'required|unique:admins',
            'address'=>'required',
            'nationality'=>'required',
            'city'=>'required',
            'degree'=>'required',
            'domaine'=>'required',
            'skills'=>'required',
            'establishment'=>'required',
            'password'=>'required|min:8',
            'c_password'=>'required|same:password',
        ];
    }
}
