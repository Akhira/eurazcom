<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BusnessRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' =>'reequired',
            'email' =>'reequired|email',
            'phone' =>'reequired',
            'city' =>'reequired',
            'country' =>'reequired',
            'service' =>'reequired',
            'budget' =>'reequired',
            'address' =>'reequired',
        ];
    }
}
