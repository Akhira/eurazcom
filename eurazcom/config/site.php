<?php 

	return [
		'name'=>env('APP_NAME'),
		'email'=>env('APP_EMAIL'),
		'city'=>env('APP_CITY'),
		'country'=>env('APP_COUNTRY'),
		'pobox'=>env('APP_POBOX'),
		'phone' =>'+237 620 881 918',
		'address' =>'BP cite',

		// socials medias links
		'fb'=>'http://facebook.com',
		'twt'=>'',
		'inst'=>'',
		'yt'=>'',
		'ld'=>'',
	];	